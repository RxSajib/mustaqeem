package com.group.parak.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.group.parak.databinding.PrivatechatsenderlayoutBinding;

public class PrivateChatSenderViewHolder extends RecyclerView.ViewHolder {

    public PrivatechatsenderlayoutBinding binding;


    public PrivateChatSenderViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = PrivatechatsenderlayoutBinding.bind(itemView);
    }
}
