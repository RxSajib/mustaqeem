package com.group.parak.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.databinding.MyhirelayoutBinding;

public class MyHireDashboardViewHolder extends RecyclerView.ViewHolder {

    public MyhirelayoutBinding binding;

    public MyHireDashboardViewHolder(@NonNull MyhirelayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
