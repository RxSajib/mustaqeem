package com.group.parak.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.databinding.PrivatechatreceiverlayoutBinding;

public class PrivateChatReceiverViewHolder extends RecyclerView.ViewHolder {

    public PrivatechatreceiverlayoutBinding binding;

    public PrivateChatReceiverViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = PrivatechatreceiverlayoutBinding.bind(itemView);
    }
}
