package com.group.parak.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.group.parak.R;
import com.group.parak.databinding.JobbannerBinding;

import org.jetbrains.annotations.NotNull;

public class JobViewHolder extends RecyclerView.ViewHolder {

    public JobbannerBinding binding;

    public JobViewHolder(@NonNull @NotNull JobbannerBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
