package com.group.parak.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.R;
import com.group.parak.databinding.TeamSingleLayoutBinding;
import com.makeramen.roundedimageview.RoundedImageView;

import org.jetbrains.annotations.NotNull;

import de.hdodenhof.circleimageview.CircleImageView;

public class TeamViewHolder extends RecyclerView.ViewHolder {

    public TeamSingleLayoutBinding binding;

    public TeamViewHolder(@NonNull @NotNull TeamSingleLayoutBinding binding) {
        super(binding.getRoot());

        this.binding = binding;

       /* ProfileImage = itemView.findViewById(R.id.TeamImageID);
        Name = itemView.findViewById(R.id.TeamPersonNameID);
        Deginaction = itemView.findViewById(R.id.TeamDegID);*/
    }
}
