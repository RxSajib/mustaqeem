package com.group.parak.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.databinding.NotificationlayoutBinding;

public class NotificationViewHolder extends RecyclerView.ViewHolder {

    public NotificationlayoutBinding binding;

    public NotificationViewHolder(@NonNull NotificationlayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
