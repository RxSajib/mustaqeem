package com.group.parak.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.group.parak.R;
import com.group.parak.databinding.HairBannerBinding;

import org.jetbrains.annotations.NotNull;

public class HeirViewHolder extends RecyclerView.ViewHolder {

    public HairBannerBinding binding;

    public HeirViewHolder(@NonNull @NotNull HairBannerBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
