package com.group.parak.ViewHolder;

import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.R;
import com.group.parak.databinding.GlobalchatviewBinding;

import org.jetbrains.annotations.NotNull;

public class MessageViewHolder extends RecyclerView.ViewHolder {

    public GlobalchatviewBinding binding;

    public MessageViewHolder(@NonNull @NotNull GlobalchatviewBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
