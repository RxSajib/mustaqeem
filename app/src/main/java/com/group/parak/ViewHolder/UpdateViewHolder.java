package com.group.parak.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.textview.MaterialTextView;
import com.group.parak.R;
import com.group.parak.databinding.NewsBannerBinding;

import org.jetbrains.annotations.NotNull;

public class UpdateViewHolder extends RecyclerView.ViewHolder {

    public NewsBannerBinding binding;

    public UpdateViewHolder(@NonNull @NotNull NewsBannerBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
