package com.group.parak.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.R;

import org.jetbrains.annotations.NotNull;

public class PendingViewHolder extends RecyclerView.ViewHolder {

    public TextView Industry, TimeDate, Salary, Location, Experience;

    public PendingViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        Industry = itemView.findViewById(R.id.Industry);
        TimeDate = itemView.findViewById(R.id.TimeAndDate);
        Salary = itemView.findViewById(R.id.Salary);
        Location = itemView.findViewById(R.id.Location);
        Experience = itemView.findViewById(R.id.Experiences);
    }
}
