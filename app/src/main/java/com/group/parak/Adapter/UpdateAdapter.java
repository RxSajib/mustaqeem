package com.group.parak.Adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.Data.DataManager;
import com.group.parak.Model.UpdateModel;
import com.group.parak.R;
import com.group.parak.ViewHolder.UpdateViewHolder;
import com.group.parak.databinding.NewsBannerBinding;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class UpdateAdapter extends RecyclerView.Adapter<UpdateViewHolder> {

    private List<UpdateModel> updateModelList;
    private LayoutInflater layoutInflater;

    public void setUpdateModelList(List<UpdateModel> updateModelList) {
        this.updateModelList = updateModelList;
    }

    @NonNull
    @NotNull
    @Override
    public UpdateViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        NewsBannerBinding binding = NewsBannerBinding.inflate(layoutInflater, parent, false);
        return new UpdateViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull UpdateViewHolder holder, int position) {

            if(updateModelList.get(position).getPostType().equals(DataManager.Image)){
                Picasso.get().load(updateModelList.get(position).getPosterPath())
                        .into(holder.binding.Image);

                Log.d("TAG", updateModelList.get(position).getPosterPath());
            }
            holder.binding.AuthorBy.setText(updateModelList.get(position).getAuthorBy());
            holder.binding.Details.setText(updateModelList.get(position).getDetails());
            holder.binding.Title.setText(updateModelList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        if(updateModelList == null){
            return 0;
        }else {
            return updateModelList.size();

        }
    }


}
