package com.group.parak.Adapter;
import android.annotation.SuppressLint;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.group.parak.Data.DataManager;
import com.group.parak.Data.TimesSinceAgo;
import com.group.parak.Model.JobModel;
import com.group.parak.ViewHolder.JobViewHolder;
import com.group.parak.databinding.JobbannerBinding;
import org.jetbrains.annotations.NotNull;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class JobAdapter extends RecyclerView.Adapter<JobViewHolder> {

    private List<JobModel> jobModelList;
    public void setJobModelList(List<JobModel> jobModelList) {
        this.jobModelList = jobModelList;
    }
    private SetOnclick SetOnclick;
    private LayoutInflater layoutInflater;

    @NonNull
    @NotNull
    @Override
    public JobViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        JobbannerBinding binding = JobbannerBinding.inflate(layoutInflater, parent, false);
        return new JobViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull JobViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.binding.CurrencyName.setText(jobModelList.get(position).getCurrency());
        holder.binding.IndustryText.setText(jobModelList.get(position).getIndustry());
        holder.binding.PriceID.setText(jobModelList.get(position).getStartSalary()+" - "+jobModelList.get(position).getEndSalary()+DataManager.Par_month);
        holder.binding.LocationTextID.setText(jobModelList.get(position).getLocation());
        holder.binding.JobType.setText(jobModelList.get(position).getJobType());
        holder.binding.ContactText.setText(jobModelList.get(position).getContract());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOnclick.Onclick(jobModelList.get(position).getTimestamp());
            }
        });
        TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
        String LastTime = timesSinceAgo.getTimeAgo(jobModelList.get(position).getTimestamp(), holder.itemView.getContext());
        holder.binding.TimeID.setText(LastTime);
    }

    @Override
    public int getItemCount() {
        if(jobModelList == null){
            return 0;
        }else {
            return jobModelList.size();
        }
    }

    public interface SetOnclick{
        void Onclick(long key);
    }

    public void SetOnClickLisiner(SetOnclick SetOnclick){
        this.SetOnclick = SetOnclick;
    }

}
