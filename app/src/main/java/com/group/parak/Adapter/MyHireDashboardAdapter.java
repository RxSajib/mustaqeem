package com.group.parak.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.Data.TimesSinceAgo;
import com.group.parak.Model.HeirModel;
import com.group.parak.ViewHolder.MyHireDashboardViewHolder;
import com.group.parak.databinding.MyhirelayoutBinding;

import java.util.List;

public class MyHireDashboardAdapter extends RecyclerView.Adapter<MyHireDashboardViewHolder> {

    private List<HeirModel> list;

    public void setList(List<HeirModel> list) {
        this.list = list;
    }
    private LayoutInflater layoutInflater;
    private SetOnclick SetOnclick;


    @NonNull
    @Override
    public MyHireDashboardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        MyhirelayoutBinding binding = MyhirelayoutBinding.inflate(layoutInflater, parent, false);
        return new MyHireDashboardViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHireDashboardViewHolder holder, int position) {
        holder.binding.Work.setText(list.get(position).getWork());
        holder.binding.Location.setText(list.get(position).getLocation());
        holder.binding.Email.setText(list.get(position).getEmail());

        TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
        String Time = timesSinceAgo.getTimeAgo(list.get(position).getTimestamp(), holder.itemView.getContext());
        holder.binding.TimeDay.setText(Time);

        holder.itemView.setOnLongClickListener(view -> {
            SetOnclick.Click(list.get(position).getTimestamp(), holder.getAdapterPosition());
            return true;
        });
    }

    @Override
    public int getItemCount() {
        if(list == null){
            return 0;
        }else {
            return list.size();
        }
    }


    public interface SetOnclick{
        void Click(long Key, int adpterposition);
    }

    public void SetOnclickLisiner(SetOnclick SetOnclick){
        this.SetOnclick  = SetOnclick;
    }
}
