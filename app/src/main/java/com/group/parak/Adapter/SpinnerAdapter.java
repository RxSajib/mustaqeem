package com.group.parak.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.group.parak.Model.JobCurrencySpinnerModel;
import com.group.parak.R;

import java.util.List;

public class SpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<JobCurrencySpinnerModel> list;

    public SpinnerAdapter(Context context, List<JobCurrencySpinnerModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        if(list == null){
            return 0;
        }else {
            return list.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View SpinnerView = LayoutInflater.from(context).inflate(R.layout.currencyspinnerlayout, parent, false);

        ImageView countryicon = SpinnerView.findViewById(R.id.Icon);
        TextView currencytext = SpinnerView.findViewById(R.id.CurrencyName);

        countryicon.setImageResource(list.get(position).getIcon());
        currencytext.setText(list.get(position).getCurrencyName());
        if(list.get(position).getCurrencyName().equals("Select your currency")){
            countryicon.setVisibility(View.GONE);
        }else {
            countryicon.setVisibility(View.VISIBLE);
        }

        return SpinnerView;
    }
}
