package com.group.parak.Adapter;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.Data.DataManager;
import com.group.parak.Data.TimesSinceAgo;
import com.group.parak.Model.HeirModel;
import com.group.parak.R;
import com.group.parak.ViewHolder.HeirViewHolder;
import com.group.parak.databinding.HairBannerBinding;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class HeirAdapter extends RecyclerView.Adapter<HeirViewHolder> {

    private List<HeirModel> heirModelList;
    private OnClick OnClick;
    private LayoutInflater layoutInflater;

    public void setHeirModelList(List<HeirModel> heirModelList) {
        this.heirModelList = heirModelList;
    }

    @NonNull
    @NotNull
    @Override
    public HeirViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {

        layoutInflater = LayoutInflater.from(parent.getContext());
        HairBannerBinding binding = HairBannerBinding.inflate(layoutInflater, parent, false);
        return new HeirViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull HeirViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.binding.NameID.setText(heirModelList.get(position).getName());
        holder.binding.Work.setText(heirModelList.get(position).getWork());

        TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
        String LastTime = timesSinceAgo.getTimeAgo(heirModelList.get(position).getTimestamp(), holder.itemView.getContext());
        holder.binding.Time.setText(LastTime);

        holder.binding.Contact.setText(
                heirModelList.get(position).getLocation()+"\n"
                +heirModelList.get(position).getPhone()+"\n"
                +heirModelList.get(position).getEmail()
        );

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClick.Click(heirModelList.get(position).getWork(), heirModelList.get(position).getLocation(), heirModelList.get(position).getCVLink(), heirModelList.get(position).getCVPosterPath(), heirModelList.get(position).getUID());
            }
        });


    }



    @Override
    public int getItemCount() {
        if(heirModelList == null){
            return 0;
        }else {
            return heirModelList.size();
        }
    }

    public interface OnClick{
        void Click(String JobTitle, String Location, String FileUri, String CVDownloadUrl, String UID);
    }

    public void SetOnclickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
