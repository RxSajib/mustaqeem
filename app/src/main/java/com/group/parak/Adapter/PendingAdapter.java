package com.group.parak
        .Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.Data.DataManager;
import com.group.parak.Data.TimesSinceAgo;
import com.group.parak.Model.JobModel;
import com.group.parak.R;
import com.group.parak.ViewHolder.PendingViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PendingAdapter extends RecyclerView.Adapter<PendingViewHolder> {

    private List<JobModel> jobModelList;
    private SetOnclick SetOnclick;

    public void setJobModelList(List<JobModel> jobModelList) {
        this.jobModelList = jobModelList;
    }

    @NonNull
    @NotNull
    @Override
    public PendingViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.job_pending_layout, parent, false);
        return new PendingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull PendingViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.Industry.setText(jobModelList.get(position).getIndustry());
        holder.Salary.setText(DataManager.AED + " "+jobModelList.get(position).getStartSalary()+" - "+jobModelList.get(position).getEndSalary());
        holder.Location.setText(jobModelList.get(position).getLocation());
        holder.Experience.setText(jobModelList.get(position).getExperience());

        TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
        String TimeDay = timesSinceAgo.getTimeAgo(jobModelList.get(position).getTimestamp(), holder.itemView.getContext());
        holder.TimeDate.setText(TimeDay);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                SetOnclick.Click(jobModelList.get(position).getTimestamp(), holder.getAdapterPosition());
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        if(jobModelList == null){
            return 0;
        }else {
            return jobModelList.size();
        }
    }

    public interface SetOnclick{
        void Click(long Key, int adpterposition);
    }

    public void SetOnclickLisiner(SetOnclick SetOnclick){
        this.SetOnclick  = SetOnclick;
    }
}
