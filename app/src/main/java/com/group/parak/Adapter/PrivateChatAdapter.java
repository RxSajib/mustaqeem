package com.group.parak.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Data;
import com.group.parak.Model.OneTwoMessageModel;
import com.group.parak.R;
import com.group.parak.ViewHolder.PrivateChatReceiverViewHolder;
import com.group.parak.ViewHolder.PrivateChatSenderViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PrivateChatAdapter extends RecyclerView.Adapter {

    private final int ITEM_SEND = 1;
    private final int ITEM_RECEIVED = 2;
    private List<OneTwoMessageModel> list;

    public void setList(List<OneTwoMessageModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == ITEM_SEND){
            return new PrivateChatSenderViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.privatechatsenderlayout, parent, false)
            );
        }else {
            return new PrivateChatReceiverViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.privatechatreceiverlayout, parent, false)
            );
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder.getClass() == PrivateChatSenderViewHolder.class){
            String type = list.get(position).getType();

            if(type.equals(DataManager.Text)){
                PrivateChatSenderViewHolder viewHolder = (PrivateChatSenderViewHolder) holder;
                viewHolder.binding.SenderMessageText.setText(list.get(position).getMessage());
                Picasso.get().load(list.get(position).getProfileImageUri()).into(viewHolder.binding.SenderProfileImageViewID);
                viewHolder.binding.SenderName.setText("Me");
            }
        }
        else {
            String type = list.get(position).getType();
            if(type.equals(DataManager.Text)){
                PrivateChatReceiverViewHolder viewHolder = (PrivateChatReceiverViewHolder) holder;
                viewHolder.binding.ReceiverMessageText.setText(list.get(position).getMessage());
                Picasso.get().load(list.get(position).getProfileImageUri()).into(viewHolder.binding.ReceiverProfileImageViewID);
                viewHolder.binding.ReceiverName.setText(list.get(position).getName());
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        OneTwoMessageModel oneTwoMessageModel = list.get(position);
        if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(oneTwoMessageModel.getSenderID())){
            return ITEM_SEND;
        }else {
            return ITEM_RECEIVED;
        }
    }

    @Override
    public int getItemCount() {
        if(list == null){
            return 0;
        }else {
            return list.size();
        }
    }
}
