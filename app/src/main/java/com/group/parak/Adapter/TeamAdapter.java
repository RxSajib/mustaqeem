package com.group.parak.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.Model.Team;
import com.group.parak.Model.TeamModel;
import com.group.parak.R;
import com.group.parak.ViewHolder.TeamViewHolder;
import com.group.parak.databinding.TeamSingleLayoutBinding;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TeamAdapter extends RecyclerView.Adapter<TeamViewHolder> {

    private List<TeamModel> teamList;
    public void setTeamList(List<TeamModel> teamList) {
        this.teamList = teamList;
    }
    private LayoutInflater layoutInflater;

    @NonNull
    @NotNull
    @Override
    public TeamViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        TeamSingleLayoutBinding binding = TeamSingleLayoutBinding.inflate(layoutInflater, parent, false);
        return new TeamViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TeamViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.binding.TeamPersonNameID.setText(teamList.get(position).getName());
        holder.binding.TeamDegID.setText(teamList.get(position).getDesignation());
        Picasso.get().load(teamList.get(position).getPosterPath()).placeholder(R.drawable.profileimage_placeholder)
                .into(holder.binding.TeamImageID, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(teamList.get(position).getPosterPath()).into(holder.binding.TeamImageID);
                    }
                });
    }

    @Override
    public int getItemCount() {

        if(teamList == null){
            return 0;
        }
        else {
            return teamList.size();
        }

    }
}
