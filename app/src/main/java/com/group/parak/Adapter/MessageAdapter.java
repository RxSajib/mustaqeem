package com.group.parak.Adapter;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.group.parak.Data.DataManager;
import com.group.parak.Data.TimesSinceAgo;
import com.group.parak.Model.Message;
import com.group.parak.R;
import com.group.parak.ViewHolder.MessageViewHolder;
import com.group.parak.databinding.GlobalchatviewBinding;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageViewHolder> {

    private List<Message> messageList;
    public LayoutInflater layoutInflater;
    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }

    private FirebaseAuth Mauth;
    private String CurrentUsrID;
    private OnChatTime OnChatTime;

    @NonNull
    @NotNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        GlobalchatviewBinding binding = GlobalchatviewBinding.inflate(layoutInflater, parent, false);
        return new MessageViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MessageViewHolder holder, int position) {
        holder.binding.SenderMessageBox.setVisibility(View.GONE);
        holder.binding.ReceiverMessageBox.setVisibility(View.GONE);
        holder.binding.SenderMessageTime.setVisibility(View.GONE);
        String type = messageList.get(position).getType();
        Mauth = FirebaseAuth.getInstance();
        CurrentUsrID = Mauth.getCurrentUser().getUid();

        if (type.equals(DataManager.Text)) {
            if(CurrentUsrID.equals(messageList.get(position).getUID())){
                holder.binding.SenderMessageBox.setVisibility(View.VISIBLE);
                holder.binding.SenderMessage.setText(messageList.get(position).getMessage());
                holder.binding.SenderMessageTime.setVisibility(View.VISIBLE);

                TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
                String Time = timesSinceAgo.getTimeAgo(messageList.get(position).getTimestamp(), holder.itemView.getContext());
                holder.binding.SenderMessageTime.setText(Time);
            }
            else {
                holder.binding.ReceiverMessageBox.setVisibility(View.VISIBLE);
                holder.binding.ReceiverMessageText.setText(messageList.get(position).getMessage());
                holder.binding.ReceiverTextMessageUserName.setText(messageList.get(position).getName());
                Picasso.get().load(messageList.get(position).getProfileImageUri()).into(holder.binding.ReceiverProfileImageID);


                TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
                String Time = timesSinceAgo.getTimeAgo(messageList.get(position).getTimestamp(), holder.itemView.getContext());
                holder.binding.ReceiverTextMessageTime.setText(Time);
            }
        }

        OnChatTime.Time(messageList.get(position).getTimestamp());

    }

    @Override
    public int getItemCount() {
        if (messageList == null) {
            return 0;
        } else {
            return messageList.size();
        }
    }

    public interface OnChatTime{
        void Time(long Timestamp);
    }
    public void OnTimeLisiner(OnChatTime OnChatTime){
        this.OnChatTime = OnChatTime;
    }

}
