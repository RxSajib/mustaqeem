package com.group.parak.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group.parak.Data.TimesSinceAgo;
import com.group.parak.Model.NotifactionModel;
import com.group.parak.ViewHolder.NotificationViewHolder;
import com.group.parak.databinding.NotificationlayoutBinding;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewHolder> {

    private List<NotifactionModel> data;
    private LayoutInflater layoutInflater;
    private OnClick OnClick;
    private OnLongCLick OnLongCLick;

    public void setData(List<NotifactionModel> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        NotificationlayoutBinding binding = NotificationlayoutBinding.inflate(layoutInflater, parent, false);
        return new NotificationViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        holder.binding.Message.setText(data.get(position).getMessage());
        holder.binding.Title.setText(data.get(position).getIndustry());
        TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
        String LastTime = timesSinceAgo.getTimeAgo(data.get(position).getTimestamp(), holder.itemView.getContext());
        holder.binding.TimeDate.setText(LastTime);

        holder.itemView.setOnClickListener(view -> {
            OnClick.CLick(data.get(position).getSenderID(), data.get(position).getIndustry());
        });

        holder.itemView.setOnLongClickListener(view -> {
            OnLongCLick.Click(data.get(position).getSenderID());
            return true;
        });
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        } else {
            return data.size();
        }
    }

    public interface OnClick {
        void CLick(String UID, String Industry);
    }

    public void SetOnCLickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }

    public interface OnLongCLick{
        void Click(String SenderID);
    }
    public void SetOnLongClickLisiner(OnLongCLick OnLongCLick){
        this.OnLongCLick = OnLongCLick;
    }
}
