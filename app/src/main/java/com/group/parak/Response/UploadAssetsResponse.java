package com.group.parak.Response;

public class UploadAssetsResponse {

    private int Code;
    private String Posterpath;

    public UploadAssetsResponse(){
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getPosterpath() {
        return Posterpath;
    }

    public void setPosterpath(String posterpath) {
        Posterpath = posterpath;
    }
}
