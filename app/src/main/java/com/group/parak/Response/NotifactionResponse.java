package com.group.parak.Response;

import com.google.gson.annotations.SerializedName;
import com.group.parak.Model.Data;

public class NotifactionResponse {

    @SerializedName("notification")
    private Data data;

    @SerializedName("to")
    private String to;

    @SerializedName("priority")
    private String priority;



    public NotifactionResponse() {
    }

    public NotifactionResponse(Data data, String to, String priority) {
        this.data = data;
        this.to = to;
        this.priority = priority;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
