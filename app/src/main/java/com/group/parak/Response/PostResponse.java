package com.group.parak.Response;

import com.group.parak.Network.JobPostRepository;

public class PostResponse {

    private boolean isJobpost;

    public PostResponse(){

    }

    public boolean isJobpost() {
        return isJobpost;
    }

    public void setJobpost(boolean jobpost) {
        isJobpost = jobpost;
    }
}
