package com.group.parak.Fcm;

import com.group.parak.Data.DataManager;
import com.group.parak.Model.Response;
import com.group.parak.Response.NotifactionResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface FcmApi {

    @Headers(
            {
                    DataManager.MSG_CONTENT_TYPE+":"+DataManager.MSG_CONTENT_TYPE_VAL,
                    DataManager.MSG_AUTHORIZATION+":"+DataManager.AuthorizationKey
            }
    )

    @POST("fcm/send")
    Call<Response> SendNotifaction(
            @Body NotifactionResponse response
    );

}
