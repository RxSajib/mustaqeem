package com.group.parak.Fcm;

import com.group.parak.Data.DataManager;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FcmClint {

    public static Retrofit retrofit;
    public Retrofit getRetrofit(){
        if(retrofit == null){
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(DataManager.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
