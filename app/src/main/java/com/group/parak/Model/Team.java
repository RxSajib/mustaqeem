package com.group.parak.Model;

import com.google.gson.annotations.SerializedName;

public class Team {

    @SerializedName("Name")
    private String Name;
    @SerializedName("ProfileImageUri")
    private String ProfileImageUri;
    @SerializedName("Designation")
    private String Designation;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getProfileImageUri() {
        return ProfileImageUri;
    }

    public void setProfileImageUri(String profileImageUri) {
        ProfileImageUri = profileImageUri;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }
}
