package com.group.parak.Model;

import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("Message")
    private String Message;

    @SerializedName("Type")
    private String Type;

    @SerializedName("UID")
    private String UID;

    @SerializedName("Timestamp")
    private long Timestamp;

    @SerializedName("Name")
    private String Name;

    @SerializedName("ProfileImageUri")
    private String ProfileImageUri;

    public Message(){

    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getProfileImageUri() {
        return ProfileImageUri;
    }

    public void setProfileImageUri(String profileImageUri) {
        ProfileImageUri = profileImageUri;
    }
}
