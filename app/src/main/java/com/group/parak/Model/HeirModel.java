package com.group.parak.Model;

public class HeirModel {

    private String CVLink;
    private String CVPosterPath;
    private String Email;
    private String Location;
    private String Name;
    private String Phone;
    private long Timestamp;
    private String Work;
    private String UID;

    public HeirModel(){

    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getCVLink() {
        return CVLink;
    }

    public void setCVLink(String CVLink) {
        this.CVLink = CVLink;
    }

    public String getCVPosterPath() {
        return CVPosterPath;
    }

    public void setCVPosterPath(String CVPosterPath) {
        this.CVPosterPath = CVPosterPath;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public String getWork() {
        return Work;
    }

    public void setWork(String work) {
        Work = work;
    }
}
