package com.group.parak.Model;

public class OneTwoMessageModel {

    private String Message;
    private String ReceiverID;
    private String SenderID;
    private long Timestamp;
    private String Type;
    private String Name;
    private String ProfileImageUri;

    public OneTwoMessageModel(){

    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getReceiverID() {
        return ReceiverID;
    }

    public void setReceiverID(String receiverID) {
        ReceiverID = receiverID;
    }

    public String getSenderID() {
        return SenderID;
    }

    public void setSenderID(String senderID) {
        SenderID = senderID;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public String getType() {
        return Type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getProfileImageUri() {
        return ProfileImageUri;
    }

    public void setProfileImageUri(String profileImageUri) {
        ProfileImageUri = profileImageUri;
    }

    public void setType(String type) {
        Type = type;
    }
}
