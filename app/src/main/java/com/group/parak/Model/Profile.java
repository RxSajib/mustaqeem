package com.group.parak.Model;

public class Profile {


    private int image;
    private String Email;
    private String Name;
    private String Phone;
    private String Token;
    private String Birthday;
    private boolean CardActiveStatus;
    private String DateOFExp;
    private String CardNumber;
    private String DateOfIssue;
    private String Gender;
    private String Location;
    private String Owner;
    private String ProfileImage;
    private String UID;

    public Profile(){

    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public boolean isCardActiveStatus() {
        return CardActiveStatus;
    }

    public void setCardActiveStatus(boolean cardActiveStatus) {
        CardActiveStatus = cardActiveStatus;
    }

    public String getDateOFExp() {
        return DateOFExp;
    }

    public void setDateOFExp(String dateOFExp) {
        DateOFExp = dateOFExp;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String cardNumber) {
        CardNumber = cardNumber;
    }

    public String getDateOfIssue() {
        return DateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        DateOfIssue = dateOfIssue;
    }

    public String getOwner() {
        return Owner;
    }

    public void setOwner(String owner) {
        Owner = owner;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }



    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }
}
