package com.group.parak.Model;

public class JobCurrencySpinnerModel {

    private String CurrencyName;
    private int Icon;

    public String getCurrencyName() {
        return CurrencyName;
    }

    public void setCurrencyName(String currencyName) {
        CurrencyName = currencyName;
    }

    public int getIcon() {
        return Icon;
    }

    public void setIcon(int icon) {
        Icon = icon;
    }
}
