package com.group.parak.Model;

import com.google.gson.annotations.SerializedName;

public class NotificationResponse {

    @SerializedName("notification")
    public Data data;

    @SerializedName("to")
    public String to;

    public NotificationResponse(){}


    public NotificationResponse(Data data, String to) {
        this.data = data;
        this.to = to;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }


}
