package com.group.parak.Data;

import com.group.parak.Model.JobCurrencySpinnerModel;
import com.group.parak.R;

import java.util.ArrayList;
import java.util.List;

public class JobCurrencySpinnerData {

    public static List<JobCurrencySpinnerModel> models(){

        List<JobCurrencySpinnerModel> modelList = new ArrayList<>();

        JobCurrencySpinnerModel empty = new JobCurrencySpinnerModel();
        empty.setCurrencyName("Select your currency");
        modelList.add(empty);

        JobCurrencySpinnerModel Pakistan = new JobCurrencySpinnerModel();
        Pakistan.setCurrencyName("Pakistan Rupee");
        Pakistan.setIcon(R.drawable.ic_pakistan);
        modelList.add(Pakistan);

        JobCurrencySpinnerModel India = new JobCurrencySpinnerModel();
            India.setCurrencyName("India Rupee");
        India.setIcon(R.drawable.ic_india);
        modelList.add(India);

        JobCurrencySpinnerModel UAE = new JobCurrencySpinnerModel();
        UAE.setCurrencyName("AED");
        UAE.setIcon(R.drawable.ic_uae);
        modelList.add(UAE);

        JobCurrencySpinnerModel Bangladesh = new JobCurrencySpinnerModel();
        Bangladesh.setCurrencyName("BDT");
        Bangladesh.setIcon(R.drawable.ic_bangladesh);
        modelList.add(Bangladesh);

        JobCurrencySpinnerModel Saudi_Arab = new JobCurrencySpinnerModel();
        Saudi_Arab.setCurrencyName("Saudi Riyal");
        Saudi_Arab.setIcon(R.drawable.ic_saudiarabia);
        modelList.add(Saudi_Arab);

        return modelList;
    }
}
