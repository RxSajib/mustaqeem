package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.HeirModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SearchHireGET {

    private Application application;
    private MutableLiveData<List<HeirModel>> data;
    private CollectionReference MHeirRef;

    public SearchHireGET(Application application) {
        this.application = application;
        MHeirRef = FirebaseFirestore.getInstance().collection(DataManager.Hairs);
    }

    public LiveData<List<HeirModel>> gethiresearch(String Type, long Limit) {
        data = new MutableLiveData<>();
        Query query = MHeirRef.orderBy(DataManager.Work).startAt(Type).endAt(Type + "\uf8ff").limit(Limit);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    boolean isEmpty = task.getResult().isEmpty();
                    if (isEmpty) {
                        data.setValue(null);
                        data.setValue(null);
                    }
                    data.setValue(task.getResult().toObjects(HeirModel.class));

                } else {
                    Toast.makeText(application, "No data found", Toast.LENGTH_SHORT).show();
                    data.setValue(null);
                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(application, "No data found", Toast.LENGTH_SHORT).show();
                        data.setValue(null);
                    }
                });
        return data;
    }
}
