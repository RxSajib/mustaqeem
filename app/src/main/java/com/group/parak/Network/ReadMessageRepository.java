package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Message;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ReadMessageRepository {

    private Application application;
    private MutableLiveData<List<Message>> mutableLiveData;
    private List<Message> messageList;
    private FirebaseFirestore MessageDatabase;
    private FirebaseAuth Mauth;
    private String CurrentUserID;

    public ReadMessageRepository(Application application) {
        this.application = application;
        messageList = new ArrayList<>();
        Mauth = FirebaseAuth.getInstance();
        MessageDatabase = FirebaseFirestore.getInstance();
    }

    public LiveData<List<Message>> getmessage(String Root, long Limit) {
        mutableLiveData = new MutableLiveData<>();
        CollectionReference Ref = MessageDatabase.collection(Root);
        Query query = Ref.orderBy(DataManager.Timestamp, Query.Direction.ASCENDING).limitToLast(Limit);
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    mutableLiveData.setValue(null);
                    return;
                }

                for(DocumentChange ds : value.getDocumentChanges()){
                    if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                        mutableLiveData.setValue(value.toObjects(Message.class));
                    }
                }
                if(value.isEmpty()){
                    mutableLiveData.setValue(null);
                }
            }
        });


        return mutableLiveData;
    }
}
