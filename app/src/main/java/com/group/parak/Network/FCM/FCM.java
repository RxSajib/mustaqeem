package com.group.parak.Network.FCM;

import com.group.parak.Data.DataManager;
import com.group.parak.Model.FcmResponse;
import com.group.parak.Model.NotificationResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface FCM {

    @Headers(
            {
                    DataManager.MSG_CONTENT_TYPE + ":" + DataManager.MSG_CONTENT_TYPE_VAL,
                    DataManager.MSG_AUTHORIZATION + ":" + DataManager.AuthorizationKey
            }
    )

    @POST("fcm/send")
    Call<FcmResponse> sendNotification(
            @Body NotificationResponse response
    );


}
