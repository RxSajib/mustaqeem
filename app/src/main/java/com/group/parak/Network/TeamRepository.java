package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Team;
import com.group.parak.Model.TeamModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class TeamRepository {

   private Application application;
   private MutableLiveData<List<TeamModel>> data;
   public FirebaseFirestore MTeamDatabase;
   public CollectionReference MTeamCollection;

   public TeamRepository(Application application){
       this.application = application;
       MTeamDatabase = FirebaseFirestore.getInstance();
       MTeamCollection = MTeamDatabase.collection(DataManager.TeamRoot);
   }

   public LiveData<List<TeamModel>> getteamdata(int Limit){
       data = new MutableLiveData<>();
       Query DESC = MTeamCollection.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);

       DESC.addSnapshotListener(new EventListener<QuerySnapshot>() {
           @Override
           public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
               if(error != null){
                   data.setValue(null);
                   return;
               }
               if(!value.isEmpty()){
                   for(DocumentChange ds : value.getDocumentChanges()){
                       if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                           data.setValue(value.toObjects(TeamModel.class));
                       }
                   }
               }else {
                   data.setValue(null);
               }
           }
       });

       return data;
   }

}
