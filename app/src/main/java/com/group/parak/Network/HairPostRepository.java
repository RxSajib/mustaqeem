package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Response.UploadResponse;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HairPostRepository {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseFirestore HIRERef;
    private FirebaseAuth Mauth;

    public HairPostRepository(Application application) {
        this.application = application;
        HIRERef = FirebaseFirestore.getInstance();
        Mauth = FirebaseAuth.getInstance();

    }

    public LiveData<Boolean> uploadhirepost(String name, String location, String phone, String email, String work, String cvlink, String cvpdfUri) {
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if (Muser != null) {
            long Timestamp = System.currentTimeMillis() / 1000;
            String TimestampString = String.valueOf(Timestamp);

            Map<String, Object> postmap = new HashMap<String, Object>();
            postmap.put(DataManager.Name, name);
            postmap.put(DataManager.Location, location);
            postmap.put(DataManager.Phone, phone);
            postmap.put(DataManager.Email, email);
            postmap.put(DataManager.Work, work);
            postmap.put(DataManager.CVLink, cvlink);
            postmap.put(DataManager.CVPosterPath, cvpdfUri);
            postmap.put(DataManager.Timestamp, Timestamp);
            postmap.put(DataManager.UID, Muser.getUid());

            HIRERef.collection(DataManager.HireRequest)
                    .document(TimestampString)
                    .set(postmap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull @NotNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                data.setValue(true);
                            } else {
                                Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                                data.setValue(false);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                            data.setValue(false);
                        }
                    });


        }
        return data;
    }

}
