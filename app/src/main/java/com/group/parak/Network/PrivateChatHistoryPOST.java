package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.NotifactionModel;
import com.group.parak.Model.ResponseCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrivateChatHistoryPOST extends AndroidViewModel {

    private Application application;
    private MutableLiveData<Boolean> data;
    private NotifactionModel notifactionModel;
    private CollectionReference MuserRef, NotifactionRef;
    private FirebaseAuth Mauth;

    public PrivateChatHistoryPOST(@NonNull Application application) {
        super(application);

        this.application = application;
        notifactionModel = new NotifactionModel();
        MuserRef = FirebaseFirestore.getInstance().collection(DataManager.Users);
        NotifactionRef = FirebaseFirestore.getInstance().collection(DataManager.ChatNotification);
        Mauth = FirebaseAuth.getInstance();
    }


    public LiveData<Boolean> PostChatHistory(String ReceiverID, String Message, String Industry){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            MuserRef.document(Muser.getUid())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){
                        long Timestamp = System.currentTimeMillis();
                        Map<String, Object> map = new HashMap<>();
                        map.put(DataManager.Timestamp, Timestamp);
                        map.put(DataManager.Message, Message);
                        map.put(DataManager.Email, task.getResult().getString(DataManager.Email));
                        map.put(DataManager.ProfileImageUri, task.getResult().getString(DataManager.UserProfileImage));
                        map.put(DataManager.Name,task.getResult().getString(DataManager.Name));
                        map.put(DataManager.UID, ReceiverID);
                        map.put(DataManager.SenderID, Muser.getUid());
                        map.put(DataManager.SendBy, DataManager.Users);
                        map.put(DataManager.PostType, DataManager.Message);
                        map.put(DataManager.Phone, task.getResult().getString(DataManager.Phone));
                        map.put(DataManager.Industry, Industry);


                        NotifactionRef.document(ReceiverID)
                                .collection(DataManager.Message).document(Mauth.getUid()).set(map)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            data.setValue(true);
                                        }else {
                                            data.setValue(true);
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                data.setValue(true);
                            }
                        });
                    }
                }
            });
        }
        return data;

    }
}
