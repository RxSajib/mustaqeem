package com.group.parak.Network;

import android.app.Application;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.group.parak.Data.DataManager;
import com.group.parak.Response.PostResponse;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class EmailUserInfoGET {

    private Application application;
    private MutableLiveData<PostResponse> data;
    private PostResponse postResponse;
    private CollectionReference MUserCollectionRef;
    private FirebaseAuth Mauth;
    private String FcmToken;


    public EmailUserInfoGET(Application application) {
        this.application = application;
        postResponse = new PostResponse();
        MUserCollectionRef = FirebaseFirestore.getInstance().collection(DataManager.Users);
        Mauth = FirebaseAuth.getInstance();

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                FcmToken = task.getResult().getToken();
            }
        });

    }

    public LiveData<PostResponse> google_auth_info(String email, String name, String phonenumber, Uri emailposterpath) {

        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if (Muser != null) {

            MUserCollectionRef.document(Muser.getUid())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){
                        DocumentSnapshot ds = task.getResult();
                        if(ds.exists()){
                            Toast.makeText(application, "Login Success", Toast.LENGTH_SHORT).show();
                            postResponse.setJobpost(true);
                            data.setValue(postResponse);
                            return;
                        }else {

                            long Timestamp = System.currentTimeMillis();
                            Map<String, Object> usermap = new HashMap<>();
                            usermap.put(DataManager.Email, email);
                            usermap.put(DataManager.Name, name);
                            usermap.put(DataManager.Phone, phonenumber);
                            usermap.put(DataManager.UserProfileImage, String.valueOf(emailposterpath));
                            usermap.put(DataManager.Location, null);
                            usermap.put(DataManager.Gender, null);
                            usermap.put(DataManager.Birthday, null);
                            usermap.put(DataManager.CardNumber, null);
                            usermap.put(DataManager.CardActiveStatus, false);
                            usermap.put(DataManager.Owner, name);
                            usermap.put(DataManager.DateOFIssue, null);
                            usermap.put(DataManager.DateOFExp, null);
                            usermap.put(DataManager.Timestamp, Timestamp);
                            usermap.put(DataManager.UID, Muser.getUid());


                            if(FcmToken != null){
                                usermap.put(DataManager.Token, FcmToken);
                            }


                            MUserCollectionRef.document(Muser.getUid()).set(usermap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull @NotNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(application, "Login Success", Toast.LENGTH_SHORT).show();
                                                postResponse.setJobpost(true);
                                                data.setValue(postResponse);
                                            } else {
                                                Toast.makeText(application, "Error Login Account", Toast.LENGTH_SHORT).show();
                                                postResponse.setJobpost(false);
                                                data.setValue(postResponse);
                                            }
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull @NotNull Exception e) {
                                            Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                                            postResponse.setJobpost(false);
                                            data.setValue(postResponse);
                                        }
                                    });
                        }
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(application, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    postResponse.setJobpost(false);
                    data.setValue(postResponse);
                }
            });


        }

        return data;

    }

}
