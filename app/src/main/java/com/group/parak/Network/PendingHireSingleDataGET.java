package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.HeirModel;

public class PendingHireSingleDataGET {

    private Application application;
    private MutableLiveData<HeirModel> data;
    private CollectionReference MHireRef;

    public PendingHireSingleDataGET(Application application){
        this.application = application;
        MHireRef = FirebaseFirestore.getInstance().collection(DataManager.HireRequest);
    }

    public LiveData<HeirModel> PendingHireSingleData(String ID){
        data = new MutableLiveData<>();
        MHireRef.document(ID).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(value.exists()){
                    data.setValue(value.toObject(HeirModel.class));

                }else {
                    data.setValue(null);
                }
            }
        });
        return data;
    }
}
