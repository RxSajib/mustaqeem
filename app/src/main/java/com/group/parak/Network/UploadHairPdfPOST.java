package com.group.parak.Network;

import android.app.Application;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.group.parak.Data.DataManager;
import com.group.parak.Response.UploadAssetsResponse;

import org.jetbrains.annotations.NotNull;

public class UploadHairPdfPOST {

    private Application application;
    private MutableLiveData<UploadAssetsResponse> data;
    private UploadAssetsResponse uploadAssetsResponse;
    private StorageReference MHairPdfStores;

    public UploadHairPdfPOST(Application application) {
        this.application = application;
        uploadAssetsResponse = new UploadAssetsResponse();
        MHairPdfStores = FirebaseStorage.getInstance().getReference();
    }

    public LiveData<UploadAssetsResponse> uploadharpdf(Uri posterpath) {
        data = new MutableLiveData<>();
        MHairPdfStores.child(DataManager.HairCVRef).child(posterpath.getLastPathSegment())
                .putFile(posterpath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if (taskSnapshot.getMetadata() != null) {
                            if (taskSnapshot.getMetadata().getReference() != null) {
                                Task<Uri> result_task = taskSnapshot.getStorage().getDownloadUrl();
                                result_task.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        uploadAssetsResponse.setPosterpath(uri.toString());
                                        uploadAssetsResponse.setCode(DataManager.SuccessCode);
                                        data.setValue(uploadAssetsResponse);
                                    }
                                })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull @NotNull Exception e) {
                                                Toast.makeText(application, "", Toast.LENGTH_SHORT).show();
                                                uploadAssetsResponse.setCode(DataManager.ErrorCode);
                                                data.setValue(uploadAssetsResponse);
                                            }
                                        });
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        uploadAssetsResponse.setCode(DataManager.ErrorCode);
                        data.setValue(uploadAssetsResponse);
                    }
                });
        return data;

    }
}
