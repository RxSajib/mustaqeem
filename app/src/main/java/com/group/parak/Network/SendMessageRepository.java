package com.group.parak.Network;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Message;
import com.group.parak.Response.PostResponse;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SendMessageRepository {

    private Application application;
    private MutableLiveData<PostResponse> data;
    private PostResponse postResponse;
    private FirebaseFirestore MessageDatabase;
    private FirebaseAuth Mauth;
    private CollectionReference MuserRef;

    public SendMessageRepository(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MessageDatabase = FirebaseFirestore.getInstance();
        postResponse = new PostResponse();
        MuserRef  = FirebaseFirestore.getInstance().collection(DataManager.Users);
    }


    public LiveData<PostResponse> setmessage(String Root, String Message, String Type) {
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null) {

            if (Message.isEmpty()) {
                Toast.makeText(application, "Enter your message", Toast.LENGTH_SHORT).show();
            } else {

                MuserRef.document(Muser.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()){
                            long Timestamplong = System.currentTimeMillis() / 1000;
                            String Timestamp = String.valueOf(Timestamplong);
                            Map<String, Object> messagemap = new HashMap<>();
                            messagemap.put(DataManager.Message, Message);
                            messagemap.put(DataManager.Timestamp, Timestamplong);
                            messagemap.put(DataManager.Type, Type);
                            messagemap.put(DataManager.UID, Mauth.getCurrentUser().getUid());
                            messagemap.put(DataManager.Name, task.getResult().getString(DataManager.Name));
                            messagemap.put(DataManager.ProfileImageUri, task.getResult().getString(DataManager.UserProfileImage));

                            MessageDatabase.collection(Root)
                                    .document(Timestamp).set(messagemap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull @NotNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                postResponse.setJobpost(true);
                                                data.setValue(postResponse);
                                            } else {
                                                postResponse.setJobpost(false);
                                                data.setValue(postResponse);
                                                Toast.makeText(application, "Error Sending Message", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull @NotNull Exception e) {
                                            postResponse.setJobpost(false);
                                            data.setValue(postResponse);
                                            Toast.makeText(application, "Error Sending Message", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    }
                });


            }
        }
        
        else {
            Toast.makeText(application, "Please sign in account", Toast.LENGTH_SHORT).show();
        }
        return data;
       
    }


}
