package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;

public class ActiveHireRemove {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference HireActiveRef;
    private FirebaseAuth Mauth;

    public ActiveHireRemove(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        HireActiveRef = FirebaseFirestore.getInstance().collection(DataManager.Hairs);
    }

    public LiveData<Boolean> ActiveHireRemove(long ID){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            HireActiveRef.document(String.valueOf(ID))
                    .delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        data.setValue(true);
                        Toast.makeText(application, "Success", Toast.LENGTH_SHORT).show();
                    }else {
                        data.setValue(false);
                        Toast.makeText(application, "Error "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    data.setValue(false);
                    Toast.makeText(application, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        return data;
    }
}
