package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Response;
import com.group.parak.Response.PostResponse;

public class NotifactionRemovePOST {

    private Application application;
    private MutableLiveData<PostResponse> data;
    private PostResponse response;
    private CollectionReference MNotifactionRef;

    public NotifactionRemovePOST(Application application){
        this.application = application;
        response = new PostResponse();
        MNotifactionRef = FirebaseFirestore.getInstance().collection(DataManager.ChatNotification);
    }

    public LiveData<PostResponse> NotifactionRemove(long  position){
        data = new MutableLiveData<>();
        MNotifactionRef.document(String.valueOf(position)).delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            response.setJobpost(true);
                            data.setValue(response);
                        }else {
                            response.setJobpost(false);
                            data.setValue(response);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                response.setJobpost(false);
                data.setValue(response);
            }
        });
        return data;
    }
}
