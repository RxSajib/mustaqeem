package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.HeirModel;

public class ActiveHireSingleViewGET {

    private Application application;
    private MutableLiveData<HeirModel> data;
    private CollectionReference MHeirRef;

    public ActiveHireSingleViewGET(Application application){
        this.application = application;
        MHeirRef = FirebaseFirestore.getInstance().collection(DataManager.Hairs);
    }

    public LiveData<HeirModel> ActiveHireData(String ID){
        data = new MutableLiveData<>();

        MHeirRef.document(ID)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            data.setValue(null);
                            return;
                        }
                        if(value.exists()){
                            data.setValue(value.toObject(HeirModel.class));
                        }else {
                            data.setValue(null);
                        }
                    }
                });
        return data;
    }
}
