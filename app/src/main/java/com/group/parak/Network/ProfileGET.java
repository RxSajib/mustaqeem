package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Profile;

import org.jetbrains.annotations.NotNull;

public class ProfileGET {

    private Application application;
    private MutableLiveData<Profile> data;
    private CollectionReference MuserRef;
    private FirebaseAuth Mauth;

    public ProfileGET(Application application){
        this.application = application;
        MuserRef = FirebaseFirestore.getInstance().collection(DataManager.Users);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Profile> getprofile(){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            MuserRef.document(Muser.getUid())
                    .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                        @Override
                        public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                            if(error != null){
                                data.setValue(null);
                                return;
                            }
                            if(value.exists()){
                                data.setValue(value.toObject(Profile.class));
                            }else {
                                data.setValue(null);
                            }
                        }
                    });
        }
       return data;
    }
}
