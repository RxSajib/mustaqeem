package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Response.PostResponse;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class CommentPOST {

    private Application application;
    private MutableLiveData<PostResponse>  data;
    private FirebaseFirestore MCommentRef;
    private PostResponse postResponse;
    private FirebaseAuth Mauth;


    public CommentPOST(Application application){
        this.application = application;
        MCommentRef = FirebaseFirestore.getInstance();
        postResponse = new PostResponse();
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<PostResponse> comment(long Key, String Message, String Type) {
        data = new MutableLiveData<>();
        if (Message.isEmpty()) {
            Toast.makeText(application, "Enter your message", Toast.LENGTH_SHORT).show();
        } else {

            long Timestamp = System.currentTimeMillis() / 1000;
            String TimestampString = String.valueOf(Timestamp);

            Map<String, Object> messagemap = new HashMap<>();
            messagemap.put(DataManager.Message, Message);
            messagemap.put(DataManager.Timestamp, Timestamp);
            messagemap.put(DataManager.Type, Type);
            messagemap.put(DataManager.SenderUID, Mauth.getCurrentUser().getUid());


            MCommentRef.collection(DataManager.CommentRoot)
                    .document(String.valueOf(Key))
                    .collection(DataManager.CommentRoot).document(TimestampString).set(messagemap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull @NotNull Task<Void> task) {
                            if(task.isSuccessful()){
                                postResponse.setJobpost(true);
                                data.setValue(postResponse);
                            }else {
                                Toast.makeText(application, "No Data Found", Toast.LENGTH_SHORT).show();
                                postResponse.setJobpost(false);
                                data.setValue(postResponse);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            Toast.makeText(application, "No Data Found", Toast.LENGTH_SHORT).show();
                            postResponse.setJobpost(false);
                            data.setValue(postResponse);
                        }
                    });
        }
        return data;
    }
}
