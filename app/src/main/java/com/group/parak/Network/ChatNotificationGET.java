package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.NotifactionModel;
import com.group.parak.UI.TabFragement.Update;

import java.util.List;

public class ChatNotificationGET {

    private Application application;
    private MutableLiveData<List<NotifactionModel>> data;
    private CollectionReference ChatNotificationRef;
    private FirebaseAuth Mauth;

    public ChatNotificationGET(Application application) {
        this.application = application;
        ChatNotificationRef = FirebaseFirestore.getInstance().collection(DataManager.ChatNotification);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<NotifactionModel>> NotificationData() {
        data = new MutableLiveData<>();

        FirebaseUser Muser = Mauth.getCurrentUser();
        if (Muser != null) {

            ChatNotificationRef.document(Muser.getUid())
                    .collection(DataManager.Message).addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if (error != null) {
                        data.setValue(null);
                        return;
                    }
                    for (DocumentChange ds : value.getDocumentChanges()) {
                        if (value.isEmpty()) {
                            data.setValue(null);
                        } else {
                            if (ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED) {
                                data.setValue(value.toObjects(NotifactionModel.class));
                            }
                        }
                    }
                }
            });
        }
        return data;
    }

}
