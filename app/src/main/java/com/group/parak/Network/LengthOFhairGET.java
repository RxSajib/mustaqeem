package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.LengthOFHair;

public class LengthOFhairGET {

    private Application application;
    private MutableLiveData<Integer> data;
    private FirebaseAuth Mauth;
    private CollectionReference MHairRef;

    public LengthOFhairGET(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MHairRef = FirebaseFirestore.getInstance().collection(DataManager.Hairs);
    }

    public LiveData<Integer> getlengthofHair() {
        FirebaseUser Muser = Mauth.getCurrentUser();
        data = new MutableLiveData<>();

        if (Muser != null) {
            Query FirebaseQuary = MHairRef.whereEqualTo(DataManager.UID, Muser.getUid());
            FirebaseQuary.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }
                    if(value.isEmpty()){
                        data.setValue(null);
                    }else {
                        data.setValue(value.size());
                    }
                }
            });

        }
        return data;

    }
}
