package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.ResponseCode;

import java.util.HashMap;
import java.util.Map;

public class NotifactionPOST {

    private Application application;
    private MutableLiveData<ResponseCode> data;
    private ResponseCode responseCode;
    private CollectionReference MNotifactionRef, MuserRef;
    private FirebaseAuth Mauth;


    public NotifactionPOST(Application application){
        this.application = application;
        responseCode = new ResponseCode();
        MNotifactionRef = FirebaseFirestore.getInstance().collection(DataManager.ChatNotification);
        MuserRef = FirebaseFirestore.getInstance().collection(DataManager.Users);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<ResponseCode> send_notifactionhistory(String Message, String Type){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            MuserRef.document(Muser.getUid())
                    .collection(DataManager.Message).document()
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){

                        long Timestamp = System.currentTimeMillis();
                        String TimestampString = String.valueOf(Timestamp);

                        Map<String, Object> map = new HashMap<>();
                        map.put(DataManager.UID, Muser.getUid());
                        map.put(DataManager.Message, Message);
                        map.put(DataManager.Timestamp, Timestamp);
                        map.put(DataManager.Type, Type);
                        map.put(DataManager.ProfileImageUri, task.getResult().getString(DataManager.UserProfileImage));
                        map.put(DataManager.Name, task.getResult().getString(DataManager.Name));
                        map.put(DataManager.Email, task.getResult().getString(DataManager.Email));
                        map.put(DataManager.Phone, task.getResult().getString(DataManager.Phone));
                        map.put(DataManager.SendBy, DataManager.User);

                        MNotifactionRef.document(TimestampString).set(map)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    responseCode.setCode(DataManager.SuccessCode);
                                    data.setValue(responseCode);
                                }else {
                                    responseCode.setCode(DataManager.ErrorCode);
                                    data.setValue(responseCode);
                                }
                            }
                        }) .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                responseCode.setCode(DataManager.ErrorCode);
                                data.setValue(responseCode);
                            }
                        })  ;

                    }else {
                        responseCode.setCode(DataManager.ErrorCode);
                        data.setValue(responseCode);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    responseCode.setCode(DataManager.ErrorCode);
                    data.setValue(responseCode);
                }
            });
        }
        return data;
    }
}
