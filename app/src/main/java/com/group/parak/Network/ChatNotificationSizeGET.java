package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.NotifactionModel;

import java.util.List;

public class ChatNotificationSizeGET {

    private Application application;
    private MutableLiveData<Integer> data;
    private CollectionReference MChatNotificationRef;
    private FirebaseAuth Mauth;

    public ChatNotificationSizeGET(Application application) {
        this.application = application;
        MChatNotificationRef = FirebaseFirestore.getInstance().collection(DataManager.ChatNotification);
        Mauth = FirebaseAuth.getInstance();
    }


    public LiveData<Integer> ChatNotificationSize() {
        data = new MutableLiveData<>();

        FirebaseUser Muser = Mauth.getCurrentUser();
        if (Muser != null) {
            MChatNotificationRef.document(Muser.getUid())
                    .collection(DataManager.Message)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                            if (error != null) {
                                data.setValue(null);
                                return;
                            }
                            if(value.isEmpty()){
                                data.setValue(null);
                            }else {
                                data.setValue(value.size());
                            }
                        }
                    });

        }
        return data;
    }
}
