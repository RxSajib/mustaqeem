package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.OneTwoMessageModel;

import java.util.ArrayList;
import java.util.List;

public class OneTwoMessageGET extends AndroidViewModel {

    private MutableLiveData<List<OneTwoMessageModel>> data;
    private Application application;
    private DatabaseReference MMessageRef;
    private FirebaseAuth Mauth;
    private List<OneTwoMessageModel> list;
    private OneTwoMessageModel OneTwoMessageModel;


    public OneTwoMessageGET(@NonNull Application application) {
        super(application);

        this.application = application;


        Mauth = FirebaseAuth.getInstance();
        list = new ArrayList<>();
        OneTwoMessageModel = new OneTwoMessageModel();
    }


    public LiveData<List<OneTwoMessageModel>> getOneTwoMessage(String ReceiverUID, int Limit) {
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();

        if (Muser != null) {
            String SenderRoom = Muser.getUid() + ReceiverUID;
            String ReceiverRoom = ReceiverUID + Muser.getUid();

            MMessageRef = FirebaseDatabase.getInstance().getReference().child(DataManager.Chat)
                    .child(SenderRoom).child("Message");
            MMessageRef.keepSynced(true);

            Query MyQuary = MMessageRef.orderByChild(DataManager.Timestamp).limitToLast(Limit);
            MyQuary.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    list.clear();
                    if(snapshot.exists()){
                        for(DataSnapshot ds : snapshot.getChildren()){
                            OneTwoMessageModel oneTwoMessageModel = ds.getValue(OneTwoMessageModel.class);
                            list.add(oneTwoMessageModel);
                            data.setValue(list);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });


        }
        return data;
    }


}
