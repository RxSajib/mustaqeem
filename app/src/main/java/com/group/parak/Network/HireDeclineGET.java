package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.HeirModel;

import java.util.List;

public class HireDeclineGET {

    private Application application;
    private MutableLiveData<List<HeirModel>> data;
    private CollectionReference MHireRef;
    private FirebaseAuth Mauth;

    public HireDeclineGET(Application application){
        this.application = application;
        MHireRef = FirebaseFirestore.getInstance().collection(DataManager.HairRejected);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<HeirModel>> HireDecline(int Limit){
        data = new MutableLiveData<>();

        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            Query query = MHireRef.whereEqualTo(DataManager.UID, Muser.getUid()).limit(Limit);

            query.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }
                    if(value.isEmpty()){
                        data.setValue(null);
                    }else {
                        for(DocumentChange ds : value.getDocumentChanges()){
                            if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                data.setValue(value.toObjects(HeirModel.class));
                            }
                        }
                    }
                }
            });
        }

        return data;
    }
}
