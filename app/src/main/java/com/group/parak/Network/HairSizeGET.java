package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;

public class HairSizeGET {

    private Application application;
    private MutableLiveData<Integer> data;
    private CollectionReference MHairRef;
    private FirebaseAuth Mauth;

    public HairSizeGET(Application application){
        this.application = application;
        MHairRef = FirebaseFirestore.getInstance().collection(DataManager.HireRequest);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Integer> HairSize(){
        data = new MutableLiveData<>();

        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            Query query = MHairRef.whereEqualTo(DataManager.UID, Muser.getUid());
            query.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }
                    if(!value.isEmpty()){
                        for(DocumentChange ds : value.getDocumentChanges()){
                            if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                data.setValue(value.size());
                            }
                        }
                    }else {
                        data.setValue(null);
                    }
                }
            });
        }

        return data;
    }
}
