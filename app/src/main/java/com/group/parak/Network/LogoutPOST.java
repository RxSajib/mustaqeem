package com.group.parak.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.group.parak.Response.PostResponse;

import java.util.Map;

public class LogoutPOST {

    private Application application;
    private MutableLiveData<PostResponse> data;
    private FirebaseAuth Mauth;
    private PostResponse postResponse;

    public LogoutPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        postResponse = new PostResponse();
    }

    public LiveData<PostResponse> logoutaccount(){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            Mauth.signOut();
            postResponse.setJobpost(true);
            data.setValue(postResponse);
        }else {
            postResponse.setJobpost(false);
            data.setValue(postResponse);
        }
        return data;
    }

}
