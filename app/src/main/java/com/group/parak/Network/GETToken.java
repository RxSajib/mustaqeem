package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Model.Token;

public class GETToken {

    private Application application;
    private MutableLiveData<Token> data;
    private ResponseCode responseCode;
    private CollectionReference MuserRef;

    public GETToken(Application application){
        this.application = application;

        responseCode = new ResponseCode();
        MuserRef  = FirebaseFirestore.getInstance().collection(DataManager.Users);
    }

    public LiveData<Token> getToken(String UID){
        data = new MutableLiveData<>();
        MuserRef.document(UID)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    data.setValue(task.getResult().toObject(Token.class));
                }else {
                    data.setValue(null);
                }
            }
        });
        return data;
    }
}
