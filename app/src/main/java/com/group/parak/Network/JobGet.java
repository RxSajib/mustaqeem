package com.group.parak.Network;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.JobModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class JobGet {

    private Application application;
    private MutableLiveData<List<JobModel>> data;
    private FirebaseFirestore MJobFireStore;
    private CollectionReference collectionReference;

    public JobGet(Application application) {
        this.application = application;
        MJobFireStore = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
                .build();
        MJobFireStore.setFirestoreSettings(settings);
        collectionReference = MJobFireStore.collection(DataManager.JobRoot);

    }

    public LiveData<List<JobModel>> getjondata(long limit) {
        data = new MutableLiveData<>();
        Query FirebaseSortAss = collectionReference.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING)
                .limit(limit);

        FirebaseSortAss.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                for(DocumentChange ds : value.getDocumentChanges()){
                    if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                        data.setValue(value.toObjects(JobModel.class));
                    }
                }
                if(value.isEmpty()){
                    data.setValue(null);
                }
            }
        });

        return data;
    }

}
