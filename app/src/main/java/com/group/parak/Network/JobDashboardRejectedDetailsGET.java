package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.JobModel;

public class JobDashboardRejectedDetailsGET {

    private Application application;
    private MutableLiveData<JobModel> data;
    private CollectionReference MRejectedDataRef;

    public JobDashboardRejectedDetailsGET(Application application){
        this.application = application;
        MRejectedDataRef = FirebaseFirestore.getInstance().collection(DataManager.RejectJobs);
    }

    public LiveData<JobModel> getjob_dashboardrejected_details(long key){
        data = new MutableLiveData<>();
        MRejectedDataRef.document(String.valueOf(key))
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            data.setValue(null);
                            return;
                        }
                        if(value.exists()){
                            data.setValue(value.toObject(JobModel.class));
                        }else {
                            data.setValue(null);
                        }
                    }
                });
        return data;
    }

}
