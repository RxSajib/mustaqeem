package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.NotifactionModel;
import com.group.parak.UI.TabFragement.Update;

import java.util.List;

public class NotifactionGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference MNotifactionRef;
    private MutableLiveData<List<NotifactionModel>> data;

    public NotifactionGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MNotifactionRef = FirebaseFirestore.getInstance().collection(DataManager.ChatNotification);
    }

    public LiveData<List<NotifactionModel>> getNotifaction(long Limit){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            String CurrentUserID = Mauth.getCurrentUser().getUid();

         /*   FirebaseQuary.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        return;
                    }

                    if(!value.isEmpty()){
                        for(DocumentChange ds : value.getDocumentChanges()){
                            if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                data.setValue(value.toObjects(NotifactionModel.class));
                            }
                        }
                    }else {
                        data.setValue(null);
                    }
                }
            });*/
        }
        return data;
    }
}
