package com.group.parak.Network.FCM;

import com.group.parak.Data.DataManager;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FCMClint {

    private static Retrofit retrofit;

    public static   Retrofit getfcmclint(){
        if(retrofit == null){
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(DataManager.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
