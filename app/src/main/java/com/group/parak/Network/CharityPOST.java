package com.group.parak.Network;

import android.app.Application;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.math.Quantiles;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Response.PostResponse;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CharityPOST {

    private Application application;
    private ResponseCode responseCode;
    private MutableLiveData<ResponseCode> data;
    private StorageReference CharityStoresRef;
    private FirebaseAuth Mauth;
    private FirebaseFirestore MCharityDatabase;
    private CollectionReference MuserRef;

    public CharityPOST(Application application){
        this.application = application;
        responseCode = new ResponseCode();
        CharityStoresRef = FirebaseStorage.getInstance().getReference().child(DataManager.CharityStores);
        Mauth = FirebaseAuth.getInstance();
        MCharityDatabase = FirebaseFirestore.getInstance();
        MuserRef = FirebaseFirestore.getInstance().collection(DataManager.Users);
    }

    public LiveData<ResponseCode> charitypost(Uri DocUri){
        data = new MutableLiveData<>();
        CharityStoresRef.child(DocUri.getLastPathSegment())
                .putFile(DocUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if(taskSnapshot.getMetadata() != null){
                            if(taskSnapshot.getMetadata().getReference() != null){
                                Task<Uri> result_task = taskSnapshot.getStorage().getDownloadUrl();
                                result_task.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        if(uri != null){

                                            FirebaseUser Muser = Mauth.getCurrentUser();
                                            if(Muser != null){
                                                MuserRef.document(Muser.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if(task.isSuccessful()){

                                                            long Timestamp = System.currentTimeMillis();
                                                            String TimestampString = String.valueOf(Timestamp);
                                                            Map<String, Object> charitymap = new HashMap<>();
                                                            charitymap.put(DataManager.UID, Muser.getUid());
                                                            charitymap.put(DataManager.Name, task.getResult().getString(DataManager.Name));
                                                            charitymap.put(DataManager.Location, task.getResult().getString(DataManager.Location));
                                                            charitymap.put(DataManager.Phone, task.getResult().getString(DataManager.Phone));
                                                            charitymap.put(DataManager.Email, task.getResult().getString(DataManager.Email));
                                                            charitymap.put(DataManager.Timestamp, Timestamp);
                                                            charitymap.put(DataManager.DownloadUri, uri.toString());

                                                            MCharityDatabase.collection(DataManager.CharityRoot)
                                                                    .document(TimestampString)
                                                                    .set(charitymap)
                                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull @NotNull Task<Void> task) {
                                                                            if(task.isSuccessful()){
                                                                                responseCode.setCode(DataManager.SuccessCode);
                                                                                data.setValue(responseCode);

                                                                            }else {
                                                                                responseCode.setCode(DataManager.ErrorCode);
                                                                                data.setValue(responseCode);
                                                                                Toast.makeText(application, "Error Sending File", Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        }
                                                                    })
                                                                    .addOnFailureListener(new OnFailureListener() {
                                                                        @Override
                                                                        public void onFailure(@NonNull @NotNull Exception e) {
                                                                            responseCode.setCode(DataManager.ErrorCode);
                                                                            data.setValue(responseCode);
                                                                            Toast.makeText(application, "Error Sending File", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    });

                                                        }else {
                                                            responseCode.setCode(DataManager.ErrorCode);
                                                            data.setValue(responseCode);
                                                            Toast.makeText(application, "Error Sending File", Toast.LENGTH_SHORT).show();

                                                        }
                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        responseCode.setCode(DataManager.ErrorCode);
                                                        data.setValue(responseCode);
                                                        Toast.makeText(application, "Error Sending File", Toast.LENGTH_SHORT).show();

                                                    }
                                                });
                                            }


                                        }
                                    }
                                })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull @NotNull Exception e) {
                                                responseCode.setCode(DataManager.ErrorCode);
                                                data.setValue(responseCode);
                                                Toast.makeText(application, "Error Sending File", Toast.LENGTH_SHORT).show();

                                            }
                                        });
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        responseCode.setCode(DataManager.ErrorCode);
                        data.setValue(responseCode);
                        Toast.makeText(application, "Error Sending File", Toast.LENGTH_SHORT).show();

                    }
                });
        return data;
    }



}
