package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;

import java.util.HashMap;
import java.util.Map;

public class UpdateJobPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference MjobRef;
    private FirebaseAuth Mauth;

    public UpdateJobPOST(Application application){
        this.application = application;
        MjobRef = FirebaseFirestore.getInstance().collection(DataManager.JobRequestRoot);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UpdateJob(String JobType, String StartSalary, String EndSalary, String Experiences, String Reward, String Location, String Contact, String Industry, String Currency, long UID){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            Map<String , Object> postmap = new HashMap<>();
            postmap.put(DataManager.JobType, JobType);
            postmap.put(DataManager.StartSalary, StartSalary);
            postmap.put(DataManager.EndSalary, EndSalary);
            postmap.put(DataManager.Experience, Experiences);
            postmap.put(DataManager.Rewards, Reward);
            postmap.put(DataManager.Location, Location);
            postmap.put(DataManager.Contract, Contact);
            postmap.put(DataManager.Industry, Industry);
            postmap.put(DataManager.UID, Mauth.getCurrentUser().getUid());
            postmap.put(DataManager.Currency, Currency);

            MjobRef.document(String.valueOf(UID))
                    .update(postmap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        data.setValue(true);
                        Toast.makeText(application, "Update success", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(application, "Error "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        data.setValue(false);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    data.setValue(false);
                    Toast.makeText(application, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        return data;
    }
}
