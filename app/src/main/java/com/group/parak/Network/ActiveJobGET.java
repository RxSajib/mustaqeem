package com.group.parak.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.JobModel;
import com.group.parak.UI.TabFragement.Update;

import java.util.List;

public class ActiveJobGET {

    private Application application;
    private MutableLiveData<List<JobModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference MActiveJobRef;

    public ActiveJobGET(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MActiveJobRef = FirebaseFirestore.getInstance().collection(DataManager.JobRoot);
    }

    public LiveData<List<JobModel>> getActiveJob(long Limit) {
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if (Muser != null) {
            Query FirebaseSearchingQuary = MActiveJobRef.orderBy(DataManager.UID).startAt(Muser.getUid()).endAt(Muser.getUid())
                    .limit(Limit);

            FirebaseSearchingQuary.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }
                    if(value.isEmpty()){
                        data.setValue(null);
                    }else {
                        for(DocumentChange ds: value.getDocumentChanges()){
                            if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                data.setValue(value.toObjects(JobModel.class));
                            }
                        }
                    }
                }
            });
        }
        return data;
    }
}
