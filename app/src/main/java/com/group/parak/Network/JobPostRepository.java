package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Response.PostResponse;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class JobPostRepository {

    private Application application;
    private MutableLiveData<ResponseCode> data;
    private FirebaseFirestore MPostFirestore;
    private FirebaseAuth Mauth;
    private ResponseCode responseCode;



    public JobPostRepository(Application application){
        this.application = application;
        MPostFirestore = FirebaseFirestore.getInstance();
        Mauth = FirebaseAuth.getInstance();
        responseCode = new ResponseCode();
    }

    public LiveData<ResponseCode> postjobs(String JobType, String StartSalary, String EndSalary, String Experiences, String Reward, String Location, String Contact, String Industry, String Currency){
        data = new MutableLiveData<>();


            long Timestamp = System.currentTimeMillis()/1000;
            String TimestampString = String.valueOf(Timestamp);

            Map<String , Object> postmap = new HashMap<>();
            postmap.put(DataManager.JobType, JobType);
            postmap.put(DataManager.StartSalary, StartSalary);
            postmap.put(DataManager.EndSalary, EndSalary);
            postmap.put(DataManager.Experience, Experiences);
            postmap.put(DataManager.Rewards, Reward);
            postmap.put(DataManager.Location, Location);
            postmap.put(DataManager.Contract, Contact);
            postmap.put(DataManager.Industry, Industry);
            postmap.put(DataManager.Timestamp, Timestamp);
            postmap.put(DataManager.UID, Mauth.getCurrentUser().getUid());
            postmap.put(DataManager.Currency, Currency);

            MPostFirestore.collection(DataManager.JobRequestRoot).document(TimestampString)
                    .set(postmap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull @NotNull Task<Void> task) {
                            if(task.isSuccessful()){

                                responseCode.setCode(DataManager.SuccessCode);
                                data.setValue(responseCode);
                            }else {
                                responseCode.setCode(DataManager.ErrorCode);
                                Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                                data.setValue(responseCode);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            responseCode.setCode(DataManager.ErrorCode);
                            Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                            data.setValue(responseCode);
                        }
                    });



        return data;
    }
}
