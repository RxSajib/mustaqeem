package com.group.parak.Network;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;

public class NotificationRemovePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference MNotificationRef;
    private FirebaseAuth Mauth;

    public NotificationRemovePOST(Application application){
        this.application = application;
        MNotificationRef = FirebaseFirestore.getInstance().collection(DataManager.ChatNotification);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> RemoveChatNotification(String ID){
        data = new MutableLiveData<>();

        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            MNotificationRef.document(Muser.getUid()).collection(DataManager.Message).document(ID).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        data.setValue(true);
                        Log.d("UserID", ID);

                        Toast.makeText(application, "Success", Toast.LENGTH_SHORT).show();
                    }else {
                        data.setValue(false);
                        Toast.makeText(application, "Error "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    data.setValue(false);
                    Toast.makeText(application, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        return data;
    }
}
