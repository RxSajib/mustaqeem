package com.group.parak.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.group.parak.Response.PostResponse;

public class GETUserExists {

    private Application application;
    private MutableLiveData<PostResponse> data;
    private PostResponse postResponse;
    private FirebaseAuth Mauth;


    public GETUserExists(Application application){
        this.application = application;
        postResponse = new PostResponse();
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<PostResponse> chackuser_login(){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            postResponse.setJobpost(true);
            data.setValue(postResponse);
        }else {
            postResponse.setJobpost(false);
            data.setValue(postResponse);
        }
        return data;
    }

}
