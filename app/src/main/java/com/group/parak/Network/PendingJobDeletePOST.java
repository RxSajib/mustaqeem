package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Response;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Response.PostResponse;

public class PendingJobDeletePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference MPendingRef;

    public PendingJobDeletePOST(Application application){
        this.application = application;
        MPendingRef = FirebaseFirestore.getInstance().collection(DataManager.JobRequestRoot);
    }

    public LiveData<Boolean> delete_pendingjob(long Key){
        data = new MutableLiveData<>();
        MPendingRef.document(String.valueOf(Key)).delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(application, "Remove success", Toast.LENGTH_SHORT).show();
                            data.setValue(true);
                        }else {
                            Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                            data.setValue(false);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                data.setValue(false);
            }
        });
        return data;
    }
}
