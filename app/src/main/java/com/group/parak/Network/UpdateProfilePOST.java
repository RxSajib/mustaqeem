package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.NetworkResponse;
import com.group.parak.Response.PostResponse;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class UpdateProfilePOST {

    private Application application;
    private MutableLiveData<NetworkResponse> data;
    private NetworkResponse networkResponse;
    private FirebaseAuth Mauth;
    private CollectionReference MuserRef;

    public UpdateProfilePOST(Application application) {
        this.application = application;
        networkResponse = new NetworkResponse();
        Mauth = FirebaseAuth.getInstance();
        MuserRef = FirebaseFirestore.getInstance().collection(DataManager.Users);
    }

    public LiveData<NetworkResponse> updatprofile(String Name, String Location, String MobileNumber, String Email, String Gender, String Birthday) {
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if (Muser != null) {

            Map<String, Object> map = new HashMap<>();
            map.put(DataManager.Name, Name);
            map.put(DataManager.Location, Location);
            map.put(DataManager.Phone, MobileNumber);
            map.put(DataManager.Email, Email);
            map.put(DataManager.Gender, Gender);
            map.put(DataManager.Birthday, Birthday);
            MuserRef.document(Muser.getUid())
                    .update(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        networkResponse.setCode(DataManager.SuccessCode);
                        data.setValue(networkResponse);
                        Toast.makeText(application, "Update success", Toast.LENGTH_SHORT).show();
                    } else {
                        networkResponse.setCode(DataManager.ErrorCode);
                        data.setValue(networkResponse);
                        Toast.makeText(application, "Error update " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    networkResponse.setCode(DataManager.ErrorCode);
                    data.setValue(networkResponse);
                    Toast.makeText(application, "Error update " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        return data;

    }
}
