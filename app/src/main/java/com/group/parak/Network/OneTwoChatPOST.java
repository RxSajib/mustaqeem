package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Response.PostResponse;
import com.group.parak.Response.UploadResponse;

import java.util.HashMap;
import java.util.Map;

public class OneTwoChatPOST extends AndroidViewModel {

    private Application application;
    private FirebaseAuth Mauth;
    private MutableLiveData<Boolean> data;
    private DatabaseReference MMessageDatabase;
    private FirebaseFirestore MuserRef;


    public OneTwoChatPOST(@NonNull Application application) {
        super(application);

        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MMessageDatabase = FirebaseDatabase.getInstance().getReference().child(DataManager.Chat);
        MuserRef = FirebaseFirestore.getInstance();
    }

    public LiveData<Boolean> send_message(String ReceiverID, String Message, String Type){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){

            MuserRef.collection(DataManager.Users).document(Muser.getUid())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){

                        long Timestamp = System.currentTimeMillis();
                        Map<String, Object> map = new HashMap<>();
                        map.put(DataManager.SenderID, Muser.getUid());
                        map.put(DataManager.ReceiverID, ReceiverID);
                        map.put(DataManager.Message, Message);
                        map.put(DataManager.Timestamp, Timestamp);
                        map.put(DataManager.Type, Type);
                        map.put(DataManager.Name, task.getResult().getString(DataManager.Name));
                        map.put(DataManager.ProfileImageUri, task.getResult().getString(DataManager.UserProfileImage));


                        String SenderRoom =  Muser.getUid()+ReceiverID;
                        String ReceiverRoom = ReceiverID+Muser.getUid();


                        MMessageDatabase.child(SenderRoom)
                                .child("Message")
                                .push()
                                .updateChildren(map)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            MMessageDatabase.child(ReceiverRoom).child("Message")
                                                    .push().updateChildren(map)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful()){
                                                                data.setValue(true);
                                                            }else {
                                                                Toast.makeText(application, "Error  sending", Toast.LENGTH_SHORT).show();
                                                                data.setValue(true);
                                                            }
                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Toast.makeText(application, "Error  sending", Toast.LENGTH_SHORT).show();
                                                    data.setValue(true);
                                                }
                                            });
                                        }else {
                                            Toast.makeText(application, "Error  sending", Toast.LENGTH_SHORT).show();
                                            data.setValue(true);
                                        }
                                    }
                                });

                    }else {

                    }
                }
            });







        }
        return data;
    }
}
