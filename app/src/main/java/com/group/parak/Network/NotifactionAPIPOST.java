package com.group.parak.Network;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.group.parak.Data.DataManager;
import com.group.parak.Fcm.FcmApi;
import com.group.parak.Fcm.FcmClint;
import com.group.parak.Model.Response;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Response.NotifactionResponse;
import com.group.parak.Response.PostResponse;

import retrofit2.Call;
import retrofit2.Callback;

public class NotifactionAPIPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FcmApi fcmApi;

    public NotifactionAPIPOST(Application application){
        this.application = application;
        fcmApi = new FcmClint().getRetrofit().create(FcmApi.class);
    }

    public LiveData<Boolean> send_message(NotifactionResponse response){
        data = new MutableLiveData<>();
        fcmApi.SendNotifaction(response)
                .enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(@Nullable Call<Response> call,@Nullable retrofit2.Response<Response> response) {
                        if(response.isSuccessful()){
                            data.postValue(true);
                            Log.d("Code", String.valueOf(response.code()));

                        }else {
                            Toast.makeText(application, "Error sending message", Toast.LENGTH_SHORT).show();
                            data.postValue(false);
                        }
                    }

                    @Override
                    public void onFailure(@Nullable Call<Response> call,@Nullable Throwable t) {
                        Toast.makeText(application, "Error sending message", Toast.LENGTH_SHORT).show();
                        data.postValue(false);
                    }
                });
        return data;
    }
}
