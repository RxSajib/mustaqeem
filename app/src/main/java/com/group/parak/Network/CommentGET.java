package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.StorageReference;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.CommentModel;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.TestOnly;

import java.util.ArrayList;
import java.util.List;

public class CommentGET {

    private Application application;
    private MutableLiveData<List<CommentModel>> data;
    private FirebaseFirestore MCommentRef;
    private List<CommentModel> commentModelList;


    public CommentGET(Application application) {
        this.application = application;
        commentModelList = new ArrayList<>();
        MCommentRef = FirebaseFirestore.getInstance();
    }

    public LiveData<List<CommentModel>> getcomment(long Position, int TotalItem) {
        data = new MutableLiveData<>();
        CollectionReference McollectionRef = MCommentRef.collection(DataManager.CommentRoot)
                .document(String.valueOf(Position)).collection(DataManager.CommentRoot);
        Query FirestoreLimitData = McollectionRef.orderBy(DataManager.Timestamp).limitToLast(TotalItem);

        FirestoreLimitData.addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable @org.jetbrains.annotations.Nullable QuerySnapshot value, @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                        commentModelList.clear();

                        if(!value.isEmpty()){
                            for (QueryDocumentSnapshot ds : value) {
                                commentModelList.add(ds
                                        .toObject(CommentModel.class));
                                data.setValue(commentModelList);
                            }
                        }
                        else {

                        }


                    }
                });

        return data;
    }
}
