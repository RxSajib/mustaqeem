package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.HeirModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HairGetRepository {

    private Application application;
    private MutableLiveData<List<HeirModel>> data;
    private FirebaseFirestore MheirRef;
    private CollectionReference CollectionRef;


    public HairGetRepository(Application application){
        this.application = application;

        MheirRef = FirebaseFirestore.getInstance();
        CollectionRef = MheirRef.collection(DataManager.Hairs);
    }

    public LiveData<List<HeirModel>> gethairdata(int limit){
        data = new MutableLiveData<>();
        Query FirebaseQry = CollectionRef.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(limit);

        FirebaseQry.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(DocumentChange ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(HeirModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }

            }
        });
        return data;
    }
}
