package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.JobModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class JobSearchGET {

    private Application application;
    private MutableLiveData<List<JobModel>> data;
    private CollectionReference MJobCollection;

    public JobSearchGET(Application application){
        this.application = application;
        MJobCollection = FirebaseFirestore.getInstance().collection(DataManager.JobRoot);
    }

    public LiveData<List<JobModel>> getsearch_job(String Type, int Limit){
        data = new MutableLiveData<>();
        Query SearchQuary = MJobCollection.orderBy(DataManager.Industry).startAt(Type).endAt(Type+"\uf8ff").limit(Limit);
        SearchQuary.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {

                if(task.isSuccessful()){
                    boolean isEmpty = task.getResult().isEmpty();
                    if(isEmpty){
                        data.setValue(null);
                    }
                    data.setValue(task.getResult().toObjects(JobModel.class));
                }else {
                    Toast.makeText(application, "No data found", Toast.LENGTH_SHORT).show();
                    data.setValue(null);
                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(application, "No data found", Toast.LENGTH_SHORT).show();
                        data.setValue(null);
                    }
                });

        return data;
    }
}
