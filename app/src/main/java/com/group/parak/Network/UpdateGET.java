package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.UpdateModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class UpdateGET {

    private Application application;
    private MutableLiveData<List<UpdateModel>> data;
    private FirebaseFirestore MUpdateDatabase;
    private CollectionReference MCollectionRef;

    public UpdateGET(Application application) {
        this.application = application;
        MUpdateDatabase = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
                .build();
        MUpdateDatabase.setFirestoreSettings(settings);
        MCollectionRef = MUpdateDatabase.collection(DataManager.UpdateRoot);
    }

    public LiveData<List<UpdateModel>> getupdate(long Limit) {
        data = new MutableLiveData<>();
        Query DES = MCollectionRef.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);

        DES.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for (DocumentChange ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(UpdateModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }

            }
        });

        return data;
    }
}
