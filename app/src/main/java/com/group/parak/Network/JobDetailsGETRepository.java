package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.JobModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class JobDetailsGETRepository {

    private Application application;
    private MutableLiveData<JobModel> data;
    private FirebaseFirestore MJobFireStore;
    private CollectionReference collectionReference;

    public JobDetailsGETRepository(Application application){
        this.application = application;
        MJobFireStore = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
                .build();
        MJobFireStore.setFirestoreSettings(settings);
        collectionReference = MJobFireStore.collection(DataManager.JobRoot);
    }

    public LiveData<JobModel> getjobdetails(long key){
        data = new MutableLiveData<>();
        collectionReference.document(String.valueOf(key))
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            data.setValue(null);
                            return;
                        }
                        if(value.exists()){
                            data.setValue(value.toObject(JobModel.class));
                        }else {
                            data.setValue(null);
                        }
                    }
                });

        return data;
    }
}
