package com.group.parak.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Data.DataManager;

import java.util.HashMap;
import java.util.Map;

public class HireUpdatePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference PendingHireRef;
    private FirebaseAuth Mauth;

    public HireUpdatePOST(Application application){
        this.application = application;
        PendingHireRef = FirebaseFirestore.getInstance().collection(DataManager.HireRequest);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UpdateHireData(String name, String location, String phone, String email, String work, String cvlink, String cvpdfUri, long UID){
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if(Muser != null){
            Map<String, Object> postmap = new HashMap<String, Object>();
            postmap.put(DataManager.Name, name);
            postmap.put(DataManager.Location, location);
            postmap.put(DataManager.Phone, phone);
            postmap.put(DataManager.Email, email);
            postmap.put(DataManager.Work, work);
            postmap.put(DataManager.CVLink, cvlink);
            postmap.put(DataManager.CVPosterPath, cvpdfUri);
            postmap.put(DataManager.UID, Muser.getUid());

            PendingHireRef.document(String.valueOf(UID)).update(postmap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                data.setValue(true);
                                Toast.makeText(application, "Update success", Toast.LENGTH_SHORT).show();
                            }else {
                                data.setValue(false);
                                Toast.makeText(application, "Error "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    data.setValue(false);
                    Toast.makeText(application, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }
        return data;
    }
}
