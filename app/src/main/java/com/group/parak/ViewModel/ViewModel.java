package com.group.parak.ViewModel;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.group.parak.Model.HeirModel;
import com.group.parak.Model.JobModel;
import com.group.parak.Model.NetworkResponse;
import com.group.parak.Model.NotifactionModel;
import com.group.parak.Model.NotificationResponse;
import com.group.parak.Model.OneTwoMessageModel;
import com.group.parak.Model.Profile;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Model.TeamModel;
import com.group.parak.Model.Token;
import com.group.parak.Model.UpdateModel;
import com.group.parak.Network.ActiveHireRemove;
import com.group.parak.Network.ActiveHireSingleViewGET;
import com.group.parak.Network.DeclineHireSizeGET;
import com.group.parak.Network.ActiveJobGET;
import com.group.parak.Network.CharityPOST;
import com.group.parak.Network.ChatNotificationGET;
import com.group.parak.Network.ChatNotificationSizeGET;
import com.group.parak.Network.EmailUserInfoGET;
import com.group.parak.Network.GETToken;
import com.group.parak.Network.GETUserExists;
import com.group.parak.Network.HairGetRepository;
import com.group.parak.Network.HairSizeGET;
import com.group.parak.Network.HireDeclineGET;
import com.group.parak.Network.HireDeclineRemove;
import com.group.parak.Network.HirePendingGET;
import com.group.parak.Network.HirePendingRemovePOST;
import com.group.parak.Network.HireUpdatePOST;
import com.group.parak.Network.JobActiveRemove;
import com.group.parak.Network.JobDashboardRejectedDetailsGET;
import com.group.parak.Network.JobDeclineRemove;
import com.group.parak.Network.JobDetailsGETRepository;
import com.group.parak.Network.LengthOFJobGET;
import com.group.parak.Network.LengthOFPendingJobGET;
import com.group.parak.Network.LengthOFRejectedJobGET;
import com.group.parak.Network.LengthOFhairGET;
import com.group.parak.Network.LogoutPOST;
import com.group.parak.Network.MyHireActiveGET;
import com.group.parak.Network.MyJobPostSizeGET;
import com.group.parak.Network.MyjobPendingGET;
import com.group.parak.Network.NotifactionAPIPOST;
import com.group.parak.Network.NotifactionGET;
import com.group.parak.Network.NotifactionPOST;
import com.group.parak.Network.NotifactionRemovePOST;
import com.group.parak.Network.NotificationRemovePOST;
import com.group.parak.Network.OneTwoChatPOST;
import com.group.parak.Network.OneTwoMessageGET;
import com.group.parak.Network.PendingHireSingleDataGET;
import com.group.parak.Network.PendingHireSize;
import com.group.parak.Network.PendingJobDeletePOST;
import com.group.parak.Network.PendingJobDetailsGET;
import com.group.parak.Network.PrivateChatHistoryPOST;
import com.group.parak.Network.ProfileGET;
import com.group.parak.Network.UpdateJobPOST;
import com.group.parak.Network.UpdateProfilePOST;
import com.group.parak.Network.RejectedJobGET;
import com.group.parak.Network.SearchHireGET;
import com.group.parak.Response.NotifactionResponse;
import com.group.parak.Response.PostResponse;
import com.group.parak.Response.UploadAssetsResponse;
import com.group.parak.Network.HairPostRepository;
import com.group.parak.Network.TeamRepository;
import com.group.parak.Network.UpdateGET;
import com.group.parak.Network.UploadHairPdfPOST;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ViewModel extends AndroidViewModel {

    private UpdateGET updateRepository;
    private TeamRepository teamRepository;
    private SearchHireGET searchHireGET;
    private LogoutPOST logoutPOST;
    private EmailUserInfoGET infoGET;
    private UploadHairPdfPOST uploadHairPdfRepository;
    private HairPostRepository hairPostRepository;
    private HairGetRepository hairGetRepository;
    private JobDetailsGETRepository jobDetailsGETRepository;
    private GETUserExists getUserExists;
    private CharityPOST charityPOST;
    private UpdateProfilePOST updateProfilePOST;
    private ProfileGET profileGET;
    private NotifactionAPIPOST notifactionPOST;
    private MyjobPendingGET myjobPendingGET;
    private LengthOFJobGET lengthOFJobGET;
    private LengthOFhairGET lengthOFhairGET;
    private LengthOFPendingJobGET lengthOFPendingJobGET;
    private LengthOFRejectedJobGET lengthOFRejectedJobGET;
    private RejectedJobGET rejectedJobGET;
    private ActiveJobGET activeJobGET;
    private NotifactionGET notifactionGET;
    private NotifactionRemovePOST notifactionRemovePOST;
    private JobDashboardRejectedDetailsGET jobDashboardRejectedDetailsGET;
    private PendingJobDeletePOST pendingJobDeletePOST;
    private PendingJobDetailsGET pendingJobDetailsGET;
    private NotifactionPOST postnotifaction_history;

    private OneTwoChatPOST oneTwoChatPOST;
    private OneTwoMessageGET oneTwoMessageGET;

    private PrivateChatHistoryPOST privateChatHistoryPOST;
    private GETToken getToken;
    private MyJobPostSizeGET jobPostSizeGET;
    private HairSizeGET hairSizeGET;
    private ChatNotificationSizeGET chatNotificationSizeGET;
    private ChatNotificationGET chatNotificationGET;
    private DeclineHireSizeGET declineHireSizeGET;
    private PendingHireSize pendingHireSize;
    private HirePendingGET hirePendingGET;
    private HireDeclineGET hireDeclineGET;
    private MyHireActiveGET myHireActiveGET;
    private ActiveHireSingleViewGET activeHireSingleViewGET;
    private PendingHireSingleDataGET pendingHireSingleDataGET;
    private HirePendingRemovePOST hirePendingRemovePOST;
    private HireUpdatePOST hireUpdatePOST;
    private UpdateJobPOST updateJobPOST;
    private HireDeclineRemove hireDeclineRemove;
    private JobDeclineRemove jobDeclineRemove;
    private JobActiveRemove jobActiveRemove;
    private ActiveHireRemove activeHireRemove;
    private NotificationRemovePOST notificationRemovePOST;

    public ViewModel(@NonNull @NotNull Application application) {
        super(application);

        updateRepository = new UpdateGET(application);
        teamRepository = new TeamRepository(application);
        uploadHairPdfRepository = new UploadHairPdfPOST(application);
        hairPostRepository = new HairPostRepository(application);
        hairGetRepository = new HairGetRepository(application);

        jobDetailsGETRepository = new JobDetailsGETRepository(application);

        getUserExists = new GETUserExists(application);
         charityPOST = new CharityPOST(application);
        profileGET = new ProfileGET(application);
        searchHireGET = new SearchHireGET(application);
        logoutPOST = new LogoutPOST(application);
        infoGET = new EmailUserInfoGET(application);
        notifactionPOST = new NotifactionAPIPOST(application);
        myjobPendingGET = new MyjobPendingGET(application);
        lengthOFJobGET = new LengthOFJobGET(application);
        lengthOFhairGET = new LengthOFhairGET(application);

        lengthOFPendingJobGET = new LengthOFPendingJobGET(application);
        lengthOFRejectedJobGET = new LengthOFRejectedJobGET(application);

        rejectedJobGET = new RejectedJobGET(application);
        activeJobGET = new ActiveJobGET(application);

        notifactionGET = new NotifactionGET(application);
        notifactionRemovePOST = new NotifactionRemovePOST(application);
        jobDashboardRejectedDetailsGET = new JobDashboardRejectedDetailsGET(application);

        pendingJobDeletePOST = new PendingJobDeletePOST(application);
        pendingJobDetailsGET = new PendingJobDetailsGET(application);
        postnotifaction_history = new NotifactionPOST(application);

        oneTwoChatPOST = new OneTwoChatPOST(application);
        oneTwoMessageGET = new OneTwoMessageGET(application);

        privateChatHistoryPOST = new PrivateChatHistoryPOST(application);
        getToken = new GETToken(application);
        updateProfilePOST = new UpdateProfilePOST(application);
        jobPostSizeGET = new MyJobPostSizeGET(application);
        hairSizeGET = new HairSizeGET(application);
        chatNotificationSizeGET = new ChatNotificationSizeGET(application);
        chatNotificationGET = new ChatNotificationGET(application);
        declineHireSizeGET = new DeclineHireSizeGET(application);
        pendingHireSize = new PendingHireSize(application);
        hirePendingGET = new HirePendingGET(application);
        hireDeclineGET = new HireDeclineGET(application);
        myHireActiveGET = new MyHireActiveGET(application);

        activeHireSingleViewGET = new ActiveHireSingleViewGET(application);
        pendingHireSingleDataGET = new PendingHireSingleDataGET(application);
        hirePendingRemovePOST = new HirePendingRemovePOST(application);
        hireUpdatePOST = new HireUpdatePOST(application);
        updateJobPOST = new UpdateJobPOST(application);
        hireDeclineRemove = new HireDeclineRemove(application);
        jobDeclineRemove = new JobDeclineRemove(application);
        jobActiveRemove = new JobActiveRemove(application);
        activeHireRemove = new ActiveHireRemove(application);
        notificationRemovePOST = new NotificationRemovePOST(application);
    }

    public LiveData<List<UpdateModel>> getupdate(long Limit) {
        return updateRepository.getupdate(Limit);
    }

    public LiveData<List<TeamModel>> getteamdata(int Limit) {
        return teamRepository.getteamdata(Limit);
    }

    public LiveData<UploadAssetsResponse> upload_hairCV(Uri posterpath) {
        return uploadHairPdfRepository.uploadharpdf(posterpath);
    }

    public LiveData<Boolean> uploadhairdata(String name, String location, String phone, String email, String work, String cvlink, String cvpdfUri) {
        return hairPostRepository.uploadhirepost(name, location, phone, email, work, cvlink, cvpdfUri);
    }

    public LiveData<List<HeirModel>> getheirdata(int limit) {
        return hairGetRepository.gethairdata(limit);
    }

    public LiveData<JobModel> getJobdetails(long Key) {
        return jobDetailsGETRepository.getjobdetails(Key);
    }



    public LiveData<PostResponse> getUserExists() {
        return getUserExists.chackuser_login();
    }

    public LiveData<ResponseCode> charitypost(Uri DocUri) {
        return charityPOST.charitypost(DocUri);
    }

    public LiveData<NetworkResponse> UpdateProfile(String Name, String Location, String MobileNumber, String Email, String Gender, String Birthday) {
        return updateProfilePOST.updatprofile(Name, Location, MobileNumber, Email, Gender, Birthday);
    }

    public LiveData<Profile> getprofile() {
        return profileGET.getprofile();
    }

    public LiveData<List<HeirModel>> getsearchhire(String SearchType, long Limit) {
        return searchHireGET.gethiresearch(SearchType, Limit);
    }

    public LiveData<PostResponse> logoutaccount() {
        return logoutPOST.logoutaccount();
    }

    public LiveData<PostResponse> getemailuser_info(String email, String name, String phonenumber, Uri emailposterpath) {
        return infoGET.google_auth_info(email, name, phonenumber, emailposterpath);
    }

    public LiveData<Boolean> send_notifaction(NotifactionResponse notifactionResponse) {
        return notifactionPOST.send_message(notifactionResponse);
    }

    public LiveData<List<JobModel>> myjobpending(long Limit) {
        return myjobPendingGET.getMyPendingJobs(Limit);
    }

    public LiveData<Integer> getLengthofjob(){
        return lengthOFJobGET.getlength_ofjobs();
    }

    public LiveData<Integer> getLengthofHire(){
        return lengthOFhairGET.getlengthofHair();
    }

    public LiveData<Integer> getLengthOFPendingJobs(){
        return lengthOFPendingJobGET.getLengthofPendingJobs();
    }

    public LiveData<Integer> getlengthOFRejectedJob(){
        return lengthOFRejectedJobGET.getlengthofRejectedJobs();
    }

    public LiveData<List<JobModel>> getRejectedJob(long Limit){
        return rejectedJobGET.getRejectedJob(Limit);
    }

    public LiveData<List<JobModel>> getActiveJob(long Limit){
        return activeJobGET.getActiveJob(Limit);
    }

    public LiveData<List<NotifactionModel>> getMynotifaction(long Limit){
        return notifactionGET.getNotifaction(Limit);
    }

    public LiveData<PostResponse> notifactionremove(long Key){
        return notifactionRemovePOST.NotifactionRemove(Key);
    }

    public LiveData<JobModel> getjobdashboard_rejected_iteamdetails(long key){
        return jobDashboardRejectedDetailsGET.getjob_dashboardrejected_details(key);
    }

    public LiveData<Boolean> delete_requestjob(long Key){
        return pendingJobDeletePOST.delete_pendingjob(Key);
    }

    public LiveData<JobModel> getPendingJobdetails(long Key){
        return pendingJobDetailsGET.getjobdetails(Key);
    }

    public LiveData<ResponseCode> send_notifactionhistory(String Message, String Type){
        return postnotifaction_history.send_notifactionhistory(Message, Type);
    }



    public LiveData<Boolean> OneTwoChatSend(String ReceiverID, String Message, String Type){
        return oneTwoChatPOST.send_message(ReceiverID, Message, Type);
    }
    public LiveData<List<OneTwoMessageModel>> getOneTwoMessage(String ReceiverID, int Limit){
        return oneTwoMessageGET.getOneTwoMessage(ReceiverID, Limit);
    }

    public LiveData<Boolean> send_private_chathistory(String ReceiverUID, String Message, String Industry){
        return privateChatHistoryPOST.PostChatHistory(ReceiverUID, Message, Industry);
    }

    public LiveData<Token> getuser_token(String UID){
        return getToken.getToken(UID);
    }

    public LiveData<Integer> MyJobPostPendingSize(){
        return jobPostSizeGET.MyJobPostSize();
    }

    public LiveData<Integer> HairSize(){
        return hairSizeGET.HairSize();
    }


    public LiveData<Integer> ChatNotificationSize(){
        return chatNotificationSizeGET.ChatNotificationSize();
    }

    public LiveData<List<NotifactionModel>> ChatNotification(){
        return chatNotificationGET.NotificationData();
    }

    public LiveData<Integer> DeclineHairSize(){
        return declineHireSizeGET.DeclineHairSize();
    }

    public LiveData<Integer> PendingHireSize(){
        return pendingHireSize.HireRequestSize();
    }

    public LiveData<List<HeirModel>> HirePending(int Limit){
        return hirePendingGET.HirePending(Limit);
    }

    public LiveData<List<HeirModel>> DeclineHire(int Limit){
        return hireDeclineGET.HireDecline(Limit);
    }

    public LiveData<List<HeirModel>> MyHireActive(int Limit){
        return myHireActiveGET.MyHire(Limit);
    }

    public LiveData<HeirModel> ActiveHireSingleData(String ID){
        return activeHireSingleViewGET.ActiveHireData(ID);
    }

    public LiveData<HeirModel> PendingHireSingleData(String ID){
        return pendingHireSingleDataGET.PendingHireSingleData(ID);
    }

    public LiveData<Boolean> HirePendingRemove(String ID){
        return hirePendingRemovePOST.RemovePendingHire(ID);
    }

    public LiveData<Boolean> HireUpdate(String name, String location, String phone, String email, String work, String cvlink, String cvpdfUri, long UID){
        return hireUpdatePOST.UpdateHireData(name, location, phone, email, work, cvlink, cvpdfUri, UID);
    }

    public LiveData<Boolean> UpdateJob(String JobType, String StartSalary, String EndSalary, String Experiences, String Reward, String Location, String Contact, String Industry, String Currency, long UID){
        return updateJobPOST.UpdateJob(JobType, StartSalary, EndSalary, Experiences, Reward, Location, Contact, Industry, Currency, UID);
    }
    public LiveData<Boolean> HireDeclineRemove(long ID){
        return hireDeclineRemove.HireDeclineRemove(ID);
    }

    public LiveData<Boolean> JobDeclineRemove(long ID){
        return jobDeclineRemove.JobDeclineRemove(ID);
    }

    public LiveData<Boolean> JobActiveRemove(long ID){
        return jobActiveRemove.JobActiveRemove(ID);
    }
    public LiveData<Boolean> ActiveHireRemove(long ID){
        return activeHireRemove.ActiveHireRemove(ID);
    }

    public LiveData<Boolean> NotificationRemove(String ID){
        return notificationRemovePOST.RemoveChatNotification(ID);
    }
}
