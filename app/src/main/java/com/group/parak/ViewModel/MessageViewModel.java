package com.group.parak.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.group.parak.Model.Message;
import com.group.parak.Network.ReadMessageRepository;
import com.group.parak.Network.SendMessageRepository;
import com.group.parak.Response.PostResponse;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MessageViewModel extends AndroidViewModel {

    private SendMessageRepository messageRepository;
    private ReadMessageRepository readMessageRepository;

    public MessageViewModel(@NonNull @NotNull Application application) {
        super(application);
        messageRepository = new SendMessageRepository(application);
        readMessageRepository = new ReadMessageRepository(application);
    }

    public LiveData<PostResponse> setmessage(String Root, String Message, String Type){
        return messageRepository.setmessage(Root, Message, Type);
    }

    public LiveData<List<Message>> getmessage(String Root, long Limit){
        return readMessageRepository.getmessage(Root, Limit);
    }
}
