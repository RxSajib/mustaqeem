package com.group.parak.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.group.parak.Model.JobModel;
import com.group.parak.Network.JobGet;
import com.group.parak.Network.JobSearchGET;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.group.parak.Data.DataManager.Type;

public class JobViewModel extends AndroidViewModel {

    private JobGet jobRepository;
    private JobSearchGET jobSearchGET;

    public JobViewModel(@NonNull @NotNull Application application) {
        super(application);

        jobRepository = new JobGet(application);
        jobSearchGET = new JobSearchGET(application);
    }

    public LiveData<List<JobModel>> getjobdata(long Limit){
        return jobRepository.getjondata(Limit);
    }

    public LiveData<List<JobModel>> getsearchjob(String Type, int Limit){
       return jobSearchGET.getsearch_job(Type, Limit);
    }


}
