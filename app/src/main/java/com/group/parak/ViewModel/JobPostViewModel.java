package com.group.parak.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.group.parak.Model.ResponseCode;
import com.group.parak.Response.PostResponse;
import com.group.parak.Network.JobPostRepository;

import org.jetbrains.annotations.NotNull;

public class JobPostViewModel extends AndroidViewModel {

    private JobPostRepository jobPostRepository;

    public JobPostViewModel(@NonNull @NotNull Application application) {
        super(application);

        jobPostRepository = new JobPostRepository(application);
    }

    public LiveData<ResponseCode> postjob(String JobType, String StartSalary, String EndSalary, String Experiences, String Reward, String Location, String Contact, String Industry, String Currency){
        return jobPostRepository.postjobs(JobType, StartSalary, EndSalary, Experiences, Reward, Location, Contact, Industry, Currency);
    }
}
