package com.group.parak.MainPage;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.group.parak.Data.DataManager;
import com.group.parak.R;
import com.group.parak.Response.PostResponse;
import com.group.parak.UI.Activity.HireDashboardPage;
import com.group.parak.UI.Activity.MyJobDashBoard;
import com.group.parak.UI.Activity.MyNotifaction;
import com.group.parak.UI.Activity.SearchPage;
import com.group.parak.UI.TabFragement.Communities;
import com.group.parak.UI.TabFragement.Hair;
import com.group.parak.UI.TabFragement.Job;
import com.group.parak.UI.TabFragement.More;
import com.group.parak.UI.TabFragement.Update;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.ExitdialogBinding;
import com.group.parak.databinding.HomaPageBinding;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;

public class HomaPage extends AppCompatActivity {

    private android.view.animation.Animation fadin_animation;
    private HomaPageBinding binding;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.homa_page);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        toolbarjobsize();
        init_view();
        getsizeof_chatnotification();
    }

    private void MobileMood(){
        int nightModeFlags =
                getApplicationContext().getResources().getConfiguration().uiMode &
                        Configuration.UI_MODE_NIGHT_MASK;
        switch (nightModeFlags) {
            case Configuration.UI_MODE_NIGHT_YES:
                NightTheme();
                break;

            case Configuration.UI_MODE_NIGHT_NO:
                DayTheme();
                break;

            case Configuration.UI_MODE_NIGHT_UNDEFINED:
                DayTheme();
                break;
        }
    }

    private void NightTheme(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_87));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_87));
        }
    }
    private void DayTheme(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void getsizeof_chatnotification(){
        viewModel.ChatNotificationSize().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer != null){
                    binding.MessageCounter.setText(String.valueOf(integer));
                    binding.MessageCounterBg.setVisibility(View.VISIBLE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                }else {
                    binding.MessageCounterBg.setVisibility(View.GONE);
                    binding.MessageIcon.setVisibility(View.GONE);
                }
            }
        });
    }

    private void toolbarjobsize(){
        viewModel.getUserExists().observe(this, new Observer<PostResponse>() {
            @Override
            public void onChanged(PostResponse postResponse) {
                if(postResponse.isJobpost()){
                    getsizeofjobpost();
                    getsizeofhair();
                }else {
                }
            }
        });

    }

    private void getsizeofhair(){
        viewModel.HairSize().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer != null){
                    binding.HairPendingSizeLayout.setVisibility(View.VISIBLE);
                    binding.HairPendingSizeText.setText(String.valueOf(integer)+" Pending");

                }else {
                    binding.HairPendingSizeLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getsizeofjobpost(){
        viewModel.MyJobPostPendingSize().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer != null){
                    binding.JobPendingSizeText.setText(String.valueOf(integer)+" Pending");
                    binding.JobPendingSizeLayout.setVisibility(View.VISIBLE);
                }else {
                    binding.JobPendingSizeLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    private void init_view(){
        fadin_animation = AnimationUtils.loadAnimation(HomaPage.this, R.anim.fadin_animaction);

        JobpageLoad(new Job());
        binding.ClipNavagationViewID.setItemSelected(R.id.JobID, true);

        click_searchbutton(DataManager.JOBS);
        binding.ClipNavagationViewID.setOnItemSelectedListener(new ChipNavigationBar.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int i) {
                if(i == R.id.JobID){
                    binding.ToolbarID.setVisibility(View.VISIBLE);
                    JobpageLoad(new Job());
                    MobileMood();
                    click_searchbutton(DataManager.JOBS);
                    binding.SearchButtonID.setVisibility(View.VISIBLE);
                }

                if(i == R.id.CommunitiesID){
                    binding.ToolbarID.setVisibility(View.VISIBLE);
                    Communitiespageload(new Communities());
                    MobileMood();
                    binding.SearchButtonID.setVisibility(View.GONE);
                }

                if(i == R.id.UpdateID){
                    binding.ToolbarID.setVisibility(View.VISIBLE);
                    UpdatepageLoad(new Update());
                    MobileMood();
                    binding.SearchButtonID.setVisibility(View.GONE);
                }

                if(i == R.id.HairID){
                    binding.ToolbarID.setVisibility(View.VISIBLE);
                    Hairpageload(new Hair());
                    MobileMood();
                    binding.SearchButtonID.setVisibility(View.VISIBLE);
                    click_searchbutton(DataManager.HAIAER);
                }

                if(i == R.id.MoreID){
                    Log.d("TAG", "TAG");
                    binding.ToolbarID.setVisibility(View.GONE);

                    change_actionbar_color();
                    goto_more(new More());

                }
            }
        });

        binding.MessageIcon.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), MyNotifaction.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            Animatoo.animateSlideLeft(HomaPage.this);
        });

        binding.JobPendingSizeLayout.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), MyJobDashBoard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            Animatoo.animateSlideLeft(HomaPage.this);
        });
        binding.HairPendingSizeLayout.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), HireDashboardPage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            Animatoo.animateSlideLeft(HomaPage.this);
        });
    }


    private void change_actionbar_color(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_green_400));
        }
        else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_green_400));
        }
    }



    private void goto_more(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.popenter, R.anim.popexit);
            transaction.replace(R.id.HomeFrame, fragment);
            transaction.commit();
        }
    }

    private void Communitiespageload(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.popenter, R.anim.popexit);
            transaction.replace(R.id.HomeFrame, fragment);
            transaction.commit();

            toolbar_name(DataManager.COMMUNITIES);
        }
    }

    private void Hairpageload(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.popenter, R.anim.popexit);
            transaction.replace(R.id.HomeFrame, fragment);
            transaction.commit();
            toolbar_name(DataManager.HAIAER);
        }
    }

    private void JobpageLoad(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.popenter, R.anim.popexit);
            transaction.replace(R.id.HomeFrame, fragment);
            transaction.commit();
            toolbar_name(DataManager.JOBS);
        }
    }

    private void UpdatepageLoad(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.popenter, R.anim.popexit);
            transaction.replace(R.id.HomeFrame, fragment);
            transaction.commit();
            toolbar_name(DataManager.UPDATE);
        }
    }


    @Override
    public void onBackPressed() {
        MaterialAlertDialogBuilder Mdialog = new MaterialAlertDialogBuilder(HomaPage.this, R.style.PauseDialog);
        ExitdialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.exitdialog, null, false);

        Mdialog.setView(binding.getRoot());

        AlertDialog alertDialog = Mdialog.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        binding.CancelButton.setOnClickListener(v ->{
            alertDialog.dismiss();
        });
        binding.LogoutButton.setOnClickListener(v ->{
            alertDialog.dismiss();
            finish();
        });
    }

    private void toolbar_name(String Title){
        binding.AppNameText.startAnimation(fadin_animation);
        fadin_animation.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
            @Override
            public void onAnimationStart(android.view.animation.Animation animation) {
                binding.AppNameText.setText(Title);
            }

            @Override
            public void onAnimationEnd(android.view.animation.Animation animation) {

            }

            @Override
            public void onAnimationRepeat(android.view.animation.Animation animation) {

            }
        });
    }

    private void click_searchbutton(String Type){

        binding.SearchButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SearchPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(DataManager.Type, Type);
                startActivity(intent);
                Animatoo.animateSlideLeft(HomaPage.this);
            }
        });

    }
}