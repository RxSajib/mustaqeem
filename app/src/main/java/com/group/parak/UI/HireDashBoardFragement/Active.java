package com.group.parak.UI.HireDashBoardFragement;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.group.parak.Adapter.MyHireDashboardAdapter;
import com.group.parak.Model.HeirModel;
import com.group.parak.Model.Token;
import com.group.parak.R;
import com.group.parak.UI.Activity.LoginRegPage;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.DeclineDashboardLayoutBinding;
import com.group.parak.databinding.DeleteDashboardLayoutBinding;
import com.group.parak.databinding.HireactiveBinding;
import com.group.parak.databinding.HirebottomsheedBinding;

import java.util.List;

public class Active extends Fragment {

    private HireactiveBinding binding;
    private MyHireDashboardAdapter adapter;
    private ViewModel viewModel;
    private IOSDialog iosDialog;

    public Active() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.hireactive, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        getdatafromserver();
        return binding.getRoot();
    }

    private void getdatafromserver(){
        adapter = new MyHireDashboardAdapter();
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        binding.RecyclerView.setAdapter(adapter);
        viewModel.MyHireActive(10).observe(getActivity(), new Observer<List<HeirModel>>() {
            @Override
            public void onChanged(List<HeirModel> heirModels) {
                if(heirModels != null){
                    adapter.setList(heirModels);
                    adapter.notifyDataSetChanged();
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    adapter.SetOnclickLisiner(new MyHireDashboardAdapter.SetOnclick() {
                        @Override
                        public void Click(long Key, int adpterposition) {
                            open_dialoag(Key);
                        }
                    });
                }else{
                    adapter.setList(heirModels);
                    adapter.notifyDataSetChanged();
                    binding.MessageText.setVisibility(View.VISIBLE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void open_dialoag(long Key) {
        BottomSheetDialog Mdialoag = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
        DeclineDashboardLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.decline_dashboard_layout, null, false);
        Mdialoag.setContentView(binding.getRoot());

        Mdialoag.show();
        binding.ViewPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mdialoag.dismiss();
                open_show_dialoag(Key);
            }
        });

        binding.DeleteButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mdialoag.dismiss();
                remove_post(Key);
            }
        });
    }

    private void open_show_dialoag(long ID){
        viewModel.ActiveHireSingleData(String.valueOf(ID)).observe(this, new Observer<HeirModel>() {
            @Override
            public void onChanged(HeirModel heirModel) {
                if(heirModel != null){
                    BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
                    HirebottomsheedBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.hirebottomsheed, null, false);
                    bottomSheetDialog.setContentView(binding.getRoot());
                    bottomSheetDialog.show();

                    binding.CloseButtonID.setOnClickListener(view -> {
                        bottomSheetDialog.dismiss();
                    });

                    binding.MainView.setVisibility(View.VISIBLE);
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    binding.Name.setText(heirModel.getName());
                    binding.Location.setText(heirModel.getLocation());
                    binding.Phone.setText(heirModel.getPhone());
                    binding.Email.setText(heirModel.getEmail());
                    binding.Work.setText(heirModel.getWork());
                    binding.CVLink.setText(heirModel.getCVLink());

                    binding.Copy.setOnClickListener(view -> {
                        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("lebel", heirModel.getCVLink());
                        clipboard.setPrimaryClip(clip);
                        Toast.makeText(getActivity(), "copy success", Toast.LENGTH_SHORT).show();
                    });

                }else {
                    //todo do other message
                    BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
                    HirebottomsheedBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.hirebottomsheed, null, false);
                    bottomSheetDialog.setContentView(binding.getRoot());
                    bottomSheetDialog.show();
                    //todo do other message
                    binding.MainView.setVisibility(View.GONE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);

                    binding.CloseButtonID.setOnClickListener(view -> {
                        bottomSheetDialog.dismiss();
                    });


                }
            }
        });
    }

    private void remove_post(long Key){
        BottomSheetDialog Mdialoag = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
        DeleteDashboardLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.delete_dashboard_layout, null, false);

        Mdialoag.setContentView(binding.getRoot());
        Mdialoag.show();

        binding.CancelButton.setOnClickListener(view -> {
            Mdialoag.dismiss();
        });

        binding.DeleteButton.setOnClickListener(view -> {
            Mdialoag.dismiss();
            progressdialoag();
            viewModel.ActiveHireRemove(Key).observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if(aBoolean){
                       iosDialog.dismiss();
                    }else {
                        iosDialog.dismiss();
                    }
                }
            });
        });
    }


    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(getActivity())
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Removing")
                .build();

        iosDialog.show();
    }
}