package com.group.parak.UI.TabFragement;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.group.parak.Adapter.UpdateAdapter;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.UpdateModel;
import com.group.parak.R;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.UpdateBinding;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class Update extends Fragment {

    private ViewModel viewModel;
    private UpdateAdapter updateAdapter;
    private long Limit = 20;
    private UpdateBinding binding;

    public Update() {
    }

 

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.update, container, false);
        init_view();

        return binding.getRoot();
    }

    private void init_view(){
        binding.ShimmerView.setVisibility(View.VISIBLE);
        binding.RecylerViewID.setHasFixedSize(true);
        updateAdapter = new UpdateAdapter();
        binding.RecylerViewID.setAdapter(updateAdapter);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.SwipRefreshLayoutID.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getdata_fromserver(Limit);
            }
        });

        binding.RecylerViewID.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {

                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+20;
                    getdata_fromserver(Limit);
                }

                super.onScrolled(recyclerView, dx, dy);
            }
        });

        getdata_fromserver(Limit);
    }

    private void getdata_fromserver(long Limit){

        if(20 < Limit){
            binding.SwipRefreshLayoutID.setRefreshing(true);
            viewModel.getupdate(Limit).observe(getViewLifecycleOwner(), new Observer<List<UpdateModel>>() {
                @Override
                public void onChanged(List<UpdateModel> updateModels) {

                    if(updateModels != null){
                        updateAdapter.setUpdateModelList(updateModels);
                        updateAdapter.notifyDataSetChanged();
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.ShimmerView.setVisibility(View.GONE);

                        binding.MessageIcon.setVisibility(View.GONE);
                        binding.MessageText.setVisibility(View.GONE);
                    }else {
                        updateAdapter.setUpdateModelList(updateModels);
                        updateAdapter.notifyDataSetChanged();
                        binding.ShimmerView.setVisibility(View.GONE);
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.MessageIcon.setVisibility(View.VISIBLE);
                        binding.MessageText.setVisibility(View.VISIBLE);
                    }


                }
            });
        }
        else {
            viewModel.getupdate(Limit).observe(getViewLifecycleOwner(), new Observer<List<UpdateModel>>() {
                @Override
                public void onChanged(List<UpdateModel> updateModels) {

                    if(updateModels != null){
                        updateAdapter.setUpdateModelList(updateModels);
                        updateAdapter.notifyDataSetChanged();
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.ShimmerView.setVisibility(View.GONE);

                        binding.MessageIcon.setVisibility(View.GONE);
                        binding.MessageText.setVisibility(View.GONE);
                    }
                    else {
                        updateAdapter.setUpdateModelList(updateModels);
                        updateAdapter.notifyDataSetChanged();
                        binding.ShimmerView.setVisibility(View.GONE);
                        binding.SwipRefreshLayoutID.setRefreshing(false);

                        binding.MessageIcon.setVisibility(View.VISIBLE);
                        binding.MessageText.setVisibility(View.VISIBLE);
                    }

                }
            });
        }

    }


}