package com.group.parak.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.rpc.Code;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.group.parak.Model.Profile;
import com.group.parak.R;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.IdCardPageBinding;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.Random;
import java.util.UUID;

public class IDCardPage extends AppCompatActivity {

    private MultiFormatWriter multiFormatWriter;
    private IdCardPageBinding binding;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.id_card_page);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        

        init_view();
        getidcarddata();

    }

    private void getidcarddata(){
        viewModel.getprofile().observe(this, new Observer<Profile>() {
            @Override
            public void onChanged(Profile profile) {
                if(profile != null){

                    if(profile.isCardActiveStatus()){
                        binding.OwnerName.setText(profile.getOwner());
                        binding.IssueDate.setText(profile.getDateOfIssue());
                        binding.ExpDate.setText(profile.getDateOFExp());

                        String UserID = profile.getCardNumber();
                        String FirstIndex = UserID.substring(0,3);
                        String SecondIndex = UserID.substring(3,6);
                        String ThirdIndex = UserID.substring(6,9);
                        String FourthIndex = UserID.substring(9,12);

                        binding.CardIDNumberFirst.setText(FirstIndex);
                        binding.CardIDSecond.setText(SecondIndex);
                        binding.CardIDThired.setText(ThirdIndex);
                        binding.CardIDFour.setText(FourthIndex);


                        try {
                            getbarcode(UserID);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        binding.Icon.setImageResource(R.drawable.ic_verified);
                        binding.VerificationStatus.setText("Verified Success");
                        binding.BackGround.setBackgroundResource(R.drawable.verifiedbg);

                    }else {
                        binding.Icon.setImageResource(R.drawable.ic_verified);
                        binding.VerificationStatus.setText("Verification Pending");
                        binding.BackGround.setBackgroundResource(R.drawable.unverifiedbg);
                    }


                }else {
                    binding.Icon.setImageResource(R.drawable.ic_verified);
                    binding.VerificationStatus.setText("Verification Pending");
                    binding.BackGround.setBackgroundResource(R.drawable.unverifiedbg);
                }
            }
        });
    }

    private void init_view(){
        multiFormatWriter = new MultiFormatWriter();

        binding.BackButtonID.setOnClickListener(v -> {
            finish();
            Animatoo.animateSlideRight(IDCardPage.this);
        });

    }




    private void getbarcode(String CodeID) throws WriterException {
        BitMatrix bitMatrix = multiFormatWriter.encode(CodeID, BarcodeFormat.CODE_128, 600, 200, null);
        BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
        Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
        binding.BarCodeImageID.setImageBitmap(bitmap);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Animatoo.animateSlideRight(IDCardPage.this);
    }
}