package com.group.parak.UI.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.firebase.auth.FirebaseAuth;
import com.group.parak.Data.DataManager;
import com.group.parak.Data.TimesSinceAgo;
import com.group.parak.Model.JobModel;
import com.group.parak.R;
import com.group.parak.Response.PostResponse;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.HeairDetailsPageBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JobDetailsPage extends AppCompatActivity {

    private long Key;
    private ViewModel viewModel;
    private static final int PERMISSIONCODE = 100;
    private Pattern regexPattern;
    private Matcher regMatcher;
    private HeairDetailsPageBinding binding;
    private String ReceiverUID;
    private String Industry;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.heair_details_page);
        viewModel = new ViewModelProvider(JobDetailsPage.this).get(ViewModel.class);


        init_view();

    }


    private void user_exists(String ID) {
        viewModel.getUserExists().observe(this, new Observer<PostResponse>() {
            @Override
            public void onChanged(PostResponse postResponse) {
                if (postResponse != null) {
                    if (postResponse.isJobpost()) {
                        if (!ID.equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                            setSupportActionBar(binding.ToolbarID);
                    }
                }
            }
        });
    }

    private void init_view() {
        binding.BackButtonID.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(JobDetailsPage.this);

        });

        Key = getIntent().getLongExtra(DataManager.Position, 0);
        getdata_server(Key);
    }


    private void getdata_server(long Position) {
        viewModel.getJobdetails(Position).observe(this, new Observer<JobModel>() {
            @Override
            public void onChanged(JobModel jobModel) {

                if(jobModel != null){
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);
                    binding.ShimmerView.setVisibility(View.GONE);
                    binding.MainviewID.setVisibility(View.VISIBLE);


                    Industry = jobModel.getIndustry();
                    binding.ToolbarTitleIndustryText.setText(jobModel.getIndustry());
                    binding.LocationText.setText(jobModel.getLocation());
                    binding.Currency.setText(jobModel.getCurrency());
                    binding.JobType.setText(jobModel.getJobType());
                    binding.Industry.setText(jobModel.getIndustry());
                    binding.Experiences.setText(jobModel.getExperience());
                    binding.Reward.setText(jobModel.getRewards());
                    binding.Price.setText(jobModel.getStartSalary() + " - " + jobModel.getEndSalary() + "par month");
                    ReceiverUID = jobModel.getUID();


                    Log.d("ID", ReceiverUID);
                    user_exists(ReceiverUID);
                    TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
                    String Times = timesSinceAgo.getTimeAgo(jobModel.getTimestamp(), getApplication());
                    binding.Times.setText(Times);




                    binding.ContactButtonID.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String Contact = jobModel.getContract();
                            if (emailaddress(Contact)) {
                                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                                emailIntent.setData(Uri.parse("mailto:" + Contact));
                                startActivity(emailIntent);
                            } else {
                                if (callpermission()) {
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Contact));
                                    startActivity(intent);
                                }

                            }
                        }
                    });
                }else {
                    //todo do other work
                    binding.MainviewID.setVisibility(View.GONE);
                    binding.ShimmerView.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.VISIBLE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Animatoo.animateSlideRight(JobDetailsPage.this);
    }

    private boolean callpermission() {
        if (ContextCompat.checkSelfPermission(JobDetailsPage.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(JobDetailsPage.this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSIONCODE);
            return false;
        }
    }

    private boolean emailaddress(String emailAddress) {
        regexPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$");
        regMatcher = regexPattern.matcher(emailAddress);
        if (regMatcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validateMobileNumber(String mobileNumber) {
        regexPattern = Pattern.compile("^\\+[0-9]{2,3}+-[0-9]{10}$");
        regMatcher = regexPattern.matcher(mobileNumber);
        if (regMatcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    private void email_intent(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat_menu, menu);


        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.Chat) {
            goto_chatpage();
        }
        return true;
    }

    private void goto_chatpage() {
        viewModel.getUserExists().observe(this, new Observer<PostResponse>() {
            @Override
            public void onChanged(PostResponse postResponse) {
                if (postResponse != null) {
                    if (postResponse.isJobpost()) {
                        Intent intent = new Intent(getApplicationContext(), ChatWithUser.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(DataManager.UID, ReceiverUID);
                        intent.putExtra(DataManager.Industry, Industry);
                        startActivity(intent);
                        Animatoo.animateSlideLeft(JobDetailsPage.this);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), LoginRegPage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        Animatoo.animateSlideLeft(JobDetailsPage.this);
                    }
                }
            }
        });

    }
}