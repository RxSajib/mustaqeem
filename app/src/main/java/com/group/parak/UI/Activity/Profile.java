package com.group.parak.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.NetworkResponse;
import com.group.parak.R;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.ProfileBinding;

import java.util.Calendar;
import java.util.Locale;

public class Profile extends AppCompatActivity {

    private ProfileBinding binding;
    private ViewModel viewModel;
    private String GenderText;
    private IOSDialog iosDialog;
    private DatePickerDialog.OnDateSetListener setListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.profile);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        init_view();
    }


    private void init_view(){

        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        final  int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);


        binding.BirthdayInput.setOnClickListener(v ->{
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    Profile.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    setListener, year, month, day
            );
            datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            datePickerDialog.show();
        });

        setListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                binding.BirthdayInput.setText(i+" / "+i1+" / "+i2);
            }
        };

        binding.BackButtonID.setOnClickListener(v ->{
            finish();
            Animatoo.animateSlideRight(Profile.this);
        });

        viewModel.getprofile().observe(this, new Observer<com.group.parak.Model.Profile>() {
            @Override
            public void onChanged(com.group.parak.Model.Profile profile) {
                if(profile != null){
                    binding.NameInput.setText(profile.getName());
                    binding.EmailInputID.setText(profile.getEmail());
                    binding.MobileInput.setText(profile.getPhone());
                    binding.LocationInput.setText(profile.getLocation());
                    binding.BirthdayInput.setText(profile.getBirthday());
                    String Gender = profile.getGender();
                    if(Gender != null){
                        if(Gender.equals(DataManager.Male)){
                            binding.MaleRadioButton.setEnabled(true);
                            GenderText = DataManager.Male;
                        }
                        if(Gender.equals(DataManager.Female)){
                            binding.FemaleRadioButton.setEnabled(true);
                            GenderText = DataManager.Female;
                        }
                    }
                }
            }
        });

        binding.GenderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton rb= (RadioButton) findViewById(i);
                GenderText = rb.getText().toString();
            }
        });

        binding.UpdateButton.setOnClickListener(v ->{
            String NameText = binding.NameInput.getText().toString().trim();
            String LocationText = binding.LocationInput.getText().toString().trim();
            String MobileText = binding.MobileInput.getText().toString().trim();
            String EmailText = binding.EmailInputID.getText().toString().trim();
            String BirthdayText = binding.BirthdayInput.getText().toString().trim();

            progressdialoag();
            viewModel.UpdateProfile(NameText, LocationText, MobileText, EmailText, GenderText, BirthdayText)
                    .observe(this, new Observer<NetworkResponse>() {
                        @Override
                        public void onChanged(NetworkResponse networkResponse) {
                            if(networkResponse.getCode() == DataManager.SuccessCode){
                                iosDialog.dismiss();
                            }else if(networkResponse.getCode() == DataManager.ErrorCode){
                                iosDialog.dismiss();
                            }
                        }
                    });
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(Profile.this);
    }

    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(Profile.this)
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Updating profile")
                .build();

        iosDialog.show();
    }
}