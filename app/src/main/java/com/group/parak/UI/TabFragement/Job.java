package com.group.parak.UI.TabFragement;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.group.parak.Adapter.JobAdapter;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.JobModel;
import com.group.parak.Model.Profile;
import com.group.parak.R;
import com.group.parak.Response.PostResponse;
import com.group.parak.UI.Activity.JobDetailsPage;
import com.group.parak.UI.Activity.JobPostPage;
import com.group.parak.UI.Activity.LoginRegPage;
import com.group.parak.ViewModel.JobViewModel;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.IdcardnotactiveDialogBinding;
import com.group.parak.databinding.JobBinding;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class Job extends Fragment {

    private JobViewModel jobViewModel;
    private JobAdapter jobAdapter;
    private long Limit = 20;
    private ViewModel viewModel;
    private JobBinding binding;
    private MediaPlayer mediaPlayer;


    public Job() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.job, container, false);

        init_view();
        click();

        return binding.getRoot();

    }

    private void init_view() {
        viewModel = new ViewModelProvider(getActivity()).get(ViewModel.class);
        jobViewModel = new ViewModelProvider(this).get(JobViewModel.class);
        binding.SwipRefreshLayoutID.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getdataserver(Limit);
                Log.d("Refresh", "ok");
            }
        });
        jobAdapter = new JobAdapter();
        binding.RecylerViewID.setHasFixedSize(true);
        binding.RecylerViewID.setAdapter(jobAdapter);

        binding.RecylerViewID.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+20;
                    getdataserver(Limit);
                }

            }
        });

        getdataserver(Limit);


    }

    private void getdataserver(long Limit) {

        if(20 <Limit){
            binding.SwipRefreshLayoutID.setRefreshing(true);
            jobViewModel.getjobdata(Limit).observe(getViewLifecycleOwner(), new Observer<List<JobModel>>() {
                @Override
                public void onChanged(List<JobModel> jobModels) {

                    if(jobModels != null){
                        binding.MessageIcon.setVisibility(View.GONE);
                        binding.MessageText.setVisibility(View.GONE);
                        jobAdapter.setJobModelList(jobModels);
                        jobAdapter.notifyDataSetChanged();
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.JobShimmerID.setVisibility(View.GONE);
                        binding.SwipRefreshLayoutID.setRefreshing(false);

                        jobAdapter.SetOnClickLisiner(new JobAdapter.SetOnclick() {
                            @Override
                            public void Onclick(long key) {
                                goto_job_details(key);
                            }
                        });
                    }
                    else {
                        jobAdapter.setJobModelList(jobModels);
                        jobAdapter.notifyDataSetChanged();
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.MessageIcon.setVisibility(View.VISIBLE);
                        binding.MessageText.setVisibility(View.VISIBLE);
                        binding.JobShimmerID.setVisibility(View.GONE);
                    }

                }
            });
        }
        else {
            jobViewModel.getjobdata(Limit).observe(getViewLifecycleOwner(), new Observer<List<JobModel>>() {
                @Override
                public void onChanged(List<JobModel> jobModels) {

                    if(jobModels != null){
                        binding.MessageIcon.setVisibility(View.GONE);
                        binding.MessageText.setVisibility(View.GONE);
                        jobAdapter.setJobModelList(jobModels);
                        jobAdapter.notifyDataSetChanged();
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.JobShimmerID.setVisibility(View.GONE);
                        binding.SwipRefreshLayoutID.setRefreshing(false);

                        jobAdapter.SetOnClickLisiner(new JobAdapter.SetOnclick() {
                            @Override
                            public void Onclick(long key) {
                                goto_job_details(key);
                            }
                        });
                    }
                    else {
                        jobAdapter.setJobModelList(jobModels);
                        jobAdapter.notifyDataSetChanged();
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.MessageIcon.setVisibility(View.VISIBLE);
                        binding.MessageText.setVisibility(View.VISIBLE);
                        binding.JobShimmerID.setVisibility(View.GONE);
                    }

                }
            });
        }

    }

    private void click() {
        binding.AddJobButtonID.setOnClickListener(v -> viewModel.getUserExists().observe(getViewLifecycleOwner(), new Observer<PostResponse>() {
            @Override
            public void onChanged(PostResponse postResponse) {

                    if(postResponse.isJobpost()){
                        viewModel.getprofile().observe(getViewLifecycleOwner(), new Observer<Profile>() {
                            @Override
                            public void onChanged(Profile profile) {
                                if(profile != null){
                                    if(profile.isCardActiveStatus()){
                                        goto_jobpage();
                                    }else {
                                        openfeedbackdialog();
                                    }
                                }
                            }
                        });

                    }

                    else {
                        goto_loginpage();
                    }


            }
        }));
    }

    private void openfeedbackdialog(){

        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(getActivity(), R.raw.notification);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }


        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity(), R.style.PauseDialog);
        IdcardnotactiveDialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.idcardnotactive_dialog, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        binding.CloseButtonID.setOnClickListener(v -> {
            alertDialog.dismiss();
        });

        binding.FeedbackButton.setOnClickListener(v -> {
            gotofeedbackemailclint();
            alertDialog.dismiss();
        });

    }

    private void gotofeedbackemailclint(){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:rakpak2020@gmail.com"));
        startActivity(Intent.createChooser(emailIntent, "Send feedback"));
    }

    private void goto_jobpage() {
        Intent intent = new Intent(getActivity(), JobPostPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void goto_job_details(long position){
        Intent intent = new Intent(getActivity(), JobDetailsPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(DataManager.Position, position);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void goto_loginpage(){
        Intent intent = new Intent(getActivity(), LoginRegPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

}