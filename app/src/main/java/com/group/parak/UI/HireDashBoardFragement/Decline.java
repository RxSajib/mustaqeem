package com.group.parak.UI.HireDashBoardFragement;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.group.parak.Adapter.MyHireDashboardAdapter;
import com.group.parak.Model.HeirModel;
import com.group.parak.R;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.HiredeclineBinding;

import java.util.List;

public class Decline extends Fragment {

    private HiredeclineBinding binding;
    private MyHireDashboardAdapter adapter;
    private ViewModel viewModel;

    public Decline() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.hiredecline, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        getdatafromserver();
        return binding.getRoot();
    }

    private void getdatafromserver(){
        adapter = new MyHireDashboardAdapter();
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        viewModel.DeclineHire(10).observe(getActivity(), new Observer<List<HeirModel>>() {
            @Override
            public void onChanged(List<HeirModel> heirModels) {
                if(heirModels != null){
                    adapter.setList(heirModels);
                    adapter.notifyDataSetChanged();
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    adapter.SetOnclickLisiner(new MyHireDashboardAdapter.SetOnclick() {
                        @Override
                        public void Click(long Key, int adpterposition) {
                            
                        }
                    });
                }else {
                    adapter.setList(heirModels);
                    adapter.notifyDataSetChanged();
                    binding.MessageText.setVisibility(View.VISIBLE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}