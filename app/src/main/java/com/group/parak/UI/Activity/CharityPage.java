package com.group.parak.UI.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Data;
import com.group.parak.Model.Profile;
import com.group.parak.Model.ResponseCode;
import com.group.parak.R;
import com.group.parak.Response.NotifactionResponse;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.CharitypageBinding;
import com.group.parak.databinding.CharityuploadsuccessdialogBinding;
import com.group.parak.databinding.ErrorDialogBinding;
import com.group.parak.databinding.IdcardnotactiveDialogBinding;

import de.mateware.snacky.Snacky;


public class CharityPage extends AppCompatActivity {

    private static final int PERMISSIONCODE = 30;
    private static final int RESULTCODE = 100;
    private ViewModel viewModel;
    private CharitypageBinding binding;
    private IOSDialog iosDialog;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.charitypage);



        init_view();
    }


    private void init_view(){
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Animatoo.animateSlideRight(CharityPage.this);
            }
        });

        binding.HelpButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilevalidation();
            }
        });
    }

    private void profilevalidation(){
        viewModel.getprofile().observe(this, new Observer<Profile>() {
            @Override
            public void onChanged(Profile profile) {
                if(!profile.isCardActiveStatus()){
                    openfeedbackdialog();
                }
                else if(profile.getEmail() == null){
                    usermessage("Your email is empty Its required complete profile first");
                }else if(profile.getLocation() == null){
                    usermessage("Your email is empty Its required complete profile first");
                }else {
                    if(IsMemoeryPermission()){
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("application/pdf");
                        startActivityForResult(intent, RESULTCODE);
                    }
                }
            }
        });
    }


    private void openfeedbackdialog() {

        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(CharityPage.this, R.raw.notification);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(CharityPage.this, R.style.PauseDialog);
        IdcardnotactiveDialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.idcardnotactive_dialog, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        binding.CloseButtonID.setOnClickListener(v -> {
            alertDialog.dismiss();
        });

        binding.FeedbackButton.setOnClickListener(v -> {
            emailfeedback();
            alertDialog.dismiss();
        });
    }

    private void emailfeedback(){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"+DataManager.FeedbackEmail));
        startActivity(Intent.createChooser(emailIntent, "Send feedback"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Animatoo.animateSlideRight(CharityPage.this);
    }

    private boolean IsMemoeryPermission(){
        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            ActivityCompat.requestPermissions(CharityPage.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONCODE);
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULTCODE && resultCode == RESULT_OK){
            progressdialoag();
            viewModel.charitypost(data.getData())
                    .observe(this, new Observer<ResponseCode>() {
                        @Override
                        public void onChanged(ResponseCode responseCode) {
                            if(responseCode.getCode() == DataManager.SuccessCode){
                                iosDialog.dismiss();
                                responseCode.setCode(DataManager.DefaultCode);
                                send_notifaction();
                                send_notifactionhistory();
                                successdialoag();
                            }
                            else if(responseCode.getCode() == DataManager.ErrorCode){
                                iosDialog.dismiss();
                                errordialog();
                                responseCode.setCode(DataManager.DefaultCode);

                            }
                        }
                    });

        }

    }

    private void errordialog(){
        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(CharityPage.this, R.raw.alert);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(CharityPage.this, R.style.PauseDialog);
        ErrorDialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.error_dialog, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        binding.CloseButton.setOnClickListener(view -> {
            alertDialog.dismiss();
        });

        binding.FeedbackButton.setOnClickListener(view -> {
            alertDialog.dismiss();
            sendfeedback();
        });
    }
    private void sendfeedback(){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"+DataManager.FeedbackEmail));
        startActivity(intent);
    }


    private void send_notifaction(){
       Data data = new Data("send hair post", "charity post", "https://www.learndash.com/wp-content/uploads/Notification-Add-on.png", "");
       NotifactionResponse response = new NotifactionResponse(data, "ccddIBD9T9efuU-GGNF5um:APA91bGwCheYFJafrIU8dTfkQAjxtTbufeLq6KP0pIQ1fxGM1qu8GPZl65fpT-oEjXXsArQzgNA98Jjo_jjyW_sBcySSCZEmSBolJprS5uOYyJmGcwM1p1MlqjZ-6V0prwTCKtE9sclL", DataManager.High);

       viewModel.send_notifaction(response).observe(this, new Observer<Boolean>() {
           @Override
           public void onChanged(Boolean responseCode) {
               if(responseCode){

               }else {

               }
           }
       });
   }

    private void send_notifactionhistory(){
        viewModel.send_notifactionhistory("Send a hair post request", "hair")
                .observe(this, new Observer<ResponseCode>() {
                    @Override
                    public void onChanged(ResponseCode responseCode) {
                        if(responseCode.getCode() == DataManager.SuccessCode){
                            responseCode.setCode(DataManager.DefaultCode);
                        }else if(responseCode.getCode() == DataManager.ErrorCode){
                            responseCode.setCode(DataManager.DefaultCode);
                        }
                    }
                });
    }


    private void successdialoag(){

        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(CharityPage.this, R.raw.success);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }

        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(CharityPage.this, R.style.PauseDialog);
        CharityuploadsuccessdialogBinding Dialoag = DataBindingUtil.inflate(LayoutInflater.from(CharityPage.this), R.layout.charityuploadsuccessdialog, null, false);
        Mbuilder.setView(Dialoag.getRoot());

        androidx.appcompat.app.AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();


        Dialoag.OkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(CharityPage.this);
            }
        });

    }

    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(CharityPage.this)
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Updating application")
                .build();

        iosDialog.show();
    }

    private void usermessage(String Message){
        Snacky.builder()
                .setView(binding.RootView)
                .setText(Message)
                .setIcon(R.drawable.ic_pen)
                .setActionText("goto profile")
                .setActionClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gotoprofile();
                    }
                })
                .setDuration(Snacky.LENGTH_SHORT)
                .setTextColor(getResources().getColor(R.color.white))
                .build()
                .show();
    }

    private void gotoprofile(){
        Intent intent = new Intent(getApplicationContext(), com.group.parak.UI.Activity.Profile.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(CharityPage.this);
    }

}
