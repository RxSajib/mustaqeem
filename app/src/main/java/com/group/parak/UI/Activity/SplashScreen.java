package com.group.parak.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.group.parak.MainPage.HomaPage;
import com.group.parak.R;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);




        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                goto_homepage();
            }
        }, 3000);
    }


    private void goto_homepage(){
        Intent intent = new Intent(getApplicationContext(), HomaPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(SplashScreen.this);
        finish();
    }
}