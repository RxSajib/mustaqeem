package com.group.parak.UI.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Adapter.NotificationAdapter;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.NotifactionModel;
import com.group.parak.R;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.ActivityMyNotifactionBinding;
import com.group.parak.databinding.NotificationremovedialogBinding;

import java.util.List;


public class MyNotifaction extends AppCompatActivity {

   private ActivityMyNotifactionBinding binding;
   private ViewModel viewModel;
   private NotificationAdapter notificationAdapter;
   private IOSDialog iosDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_notifaction);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        init_view();
        loaddataserver();
    }

    private void init_view(){
        binding.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(MyNotifaction.this);
        });
    }



    private void loaddataserver(){
        notificationAdapter = new NotificationAdapter();
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(notificationAdapter);

        viewModel.ChatNotification().observe(this, new Observer<List<NotifactionModel>>() {
            @Override
            public void onChanged(List<NotifactionModel> notifactionModels) {
                if(notifactionModels != null){
                    notificationAdapter.setData(notifactionModels);
                    notificationAdapter.notifyDataSetChanged();
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    notificationAdapter.SetOnCLickLisiner(new NotificationAdapter.OnClick() {
                        @Override
                        public void CLick(String UID, String Industry) {
                            Intent intent = new Intent(getApplicationContext(), ChatWithUser.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra(DataManager.UID, UID);
                            intent.putExtra(DataManager.Industry, Industry);
                            startActivity(intent);
                            Animatoo.animateSlideLeft(MyNotifaction.this);

                        }
                    });

                    notificationAdapter.SetOnLongClickLisiner(new NotificationAdapter.OnLongCLick() {
                        @Override
                        public void Click(String SenderID) {
                            removedialog(SenderID);
                        }
                    });
                }else {
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);
                    notificationAdapter.setData(notifactionModels);
                    notificationAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(MyNotifaction.this);
    }

    public void removedialog(String UID){

        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(MyNotifaction.this);
        NotificationremovedialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.notificationremovedialog, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        binding.DeleteButton.setOnClickListener(view -> {
            alertDialog.dismiss();
            progressdialoag();
            viewModel.NotificationRemove(UID).observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if(aBoolean){
                        iosDialog.dismiss();
                    }else {
                        iosDialog.dismiss();
                    }
                }
            });
        });

        binding.CancelButton.setOnClickListener(view -> {
            alertDialog.dismiss();
        });
    }

    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(MyNotifaction.this)
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Removing")
                .build();

        iosDialog.show();
    }

}