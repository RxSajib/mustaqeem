package com.group.parak.UI.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.group.parak.Adapter.HeirAdapter;
import com.group.parak.Adapter.JobAdapter;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.HeirModel;
import com.group.parak.Model.JobModel;
import com.group.parak.R;
import com.group.parak.ViewModel.JobViewModel;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.SearchPageBinding;

import java.util.ArrayList;
import java.util.List;

public class SearchPage extends AppCompatActivity {

    private JobViewModel jobViewModel;
    private JobAdapter jobAdapter;
    private List<JobModel> jobModelList = new ArrayList<>();
    private int CurrentPage = 6;
    private String Type;
    private ViewModel viewModel;
    private HeirAdapter heirAdapter;
    private SearchPageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.search_page);


        init_view();
    }


    private void init_view() {
        Type = getIntent().getStringExtra(DataManager.Type);
        binding.Title.setText("Search by " + Type);


       if(Type.equals(DataManager.JOBS)){
           jobViewModel = new ViewModelProvider(this).get(JobViewModel.class);
            jobAdapter = new JobAdapter();
            binding.RecylerViewID.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            binding.RecylerViewID.setAdapter(jobAdapter);
        }

        if (Type.equals(DataManager.HAIAER)) {
            viewModel = new ViewModelProvider(this).get(ViewModel.class);
            heirAdapter = new HeirAdapter();

            binding.RecylerViewID.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            binding.RecylerViewID.setAdapter(heirAdapter);
        }


        binding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Animatoo.animateSlideRight(SearchPage.this);
            }
        });



        binding.FilterIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(SearchPage.this);
                View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.job_list_dialoag, null, false);
                Mbuilder.setView(view);

                AlertDialog alertDialog = Mbuilder.create();
                alertDialog.show();

                ListView listView = view.findViewById(R.id.ListViewID);

                if(Type.equals(DataManager.JOBS)){
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(SearchPage.this, R.layout.search_industry_iteam, R.id.NameID, getResources().getStringArray(R.array.industry));
                    listView.setAdapter(adapter);
                    Log.d("JOB", "OK");
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            search_jobs(parent.getItemAtPosition(position).toString());
                            binding.SearchInput.setText(parent.getItemAtPosition(position).toString());
                            alertDialog.dismiss();
                        }
                    });

                    binding.SearchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                            if(actionId == EditorInfo.IME_ACTION_DONE){
                                search_jobs(binding.SearchInput.getText().toString().trim());
                            }

                            return true;
                        }
                    });
                }

                if (Type.equals(DataManager.HAIAER)) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(SearchPage.this, R.layout.search_industry_iteam, R.id.NameID, getResources().getStringArray(R.array.work));
                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            binding.SearchInput.setText(parent.getItemAtPosition(position).toString());
                            Hiresearch(parent.getItemAtPosition(position).toString());
                            alertDialog.dismiss();
                        }
                    });

                    binding.SearchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                            if (actionId == EditorInfo.IME_ACTION_DONE) {
                                Hiresearch(binding.SearchInput.getText().toString().trim());
                            }

                            return true;
                        }
                    });
                }

            }
        });


    }

    private void search_jobs(String Job) {
        binding.MessageIcon.setVisibility(View.GONE);
        binding.MessageText.setVisibility(View.GONE);

        binding.JobShimmerID.setVisibility(View.VISIBLE);
        jobViewModel.getsearchjob(Job, 20).observe(this, new Observer<List<JobModel>>() {
            @Override
            public void onChanged(List<JobModel> jobModels) {
                if (jobModels != null) {
                    jobAdapter.setJobModelList(jobModels);
                    jobAdapter.notifyDataSetChanged();
                    binding.JobShimmerID.setVisibility(View.GONE);

                    jobAdapter.SetOnClickLisiner(new JobAdapter.SetOnclick() {
                        @Override
                        public void Onclick(long key) {
                            goto_job_details(key);
                        }
                    });

                } else {
                    jobAdapter.setJobModelList(jobModels);
                    jobAdapter.notifyDataSetChanged();
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);
                    binding.JobShimmerID.setVisibility(View.GONE);
                    binding.MessageIcon.setImageResource(R.drawable.ic_search_notfound);
                    binding.MessageText.setText("Result Not Found");

                }
            }
        });
    }

    private void Hiresearch(String SearchKey) {
        viewModel.getsearchhire(SearchKey, 20)
                .observe(this, new Observer<List<HeirModel>>() {
                    @Override
                    public void onChanged(List<HeirModel> heirModels) {
                        if (heirModels != null) {
                            heirAdapter.setHeirModelList(heirModels);
                            heirAdapter.notifyDataSetChanged();
                        }
                        else {
                            binding.MessageIcon.setImageResource(R.drawable.ic_search_notfound);
                            binding.MessageText.setText("Result Not Found");
                        }
                    }
                });
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(SearchPage.this);
    }

    private void goto_job_details(long position) {
        Intent intent = new Intent(getApplicationContext(), JobDetailsPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(DataManager.Position, position);
        startActivity(intent);
        Animatoo.animateSlideLeft(SearchPage.this);
    }
}