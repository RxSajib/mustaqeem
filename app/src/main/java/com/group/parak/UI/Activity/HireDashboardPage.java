package com.group.parak.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.group.parak.Model.Profile;
import com.group.parak.R;
import com.group.parak.UI.HireDashBoardFragement.Active;
import com.group.parak.UI.HireDashBoardFragement.Decline;
import com.group.parak.UI.HireDashBoardFragement.Pending;
import com.group.parak.UI.JobDashboardFragement.ActiveJob;
import com.group.parak.UI.JobDashboardFragement.DeclintJob;
import com.group.parak.UI.JobDashboardFragement.PendingJob;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.HiredashboardPageBinding;
import com.squareup.picasso.Picasso;

public class HireDashboardPage extends AppCompatActivity {

    private HiredashboardPageBinding binding;
    private boolean IsPending = false, IsDecline = true, IsActive = true;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding  = DataBindingUtil.setContentView(this, R.layout.hiredashboard_page);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        init_view();
        sizeofdata();
        setuo_profile();
    }

    private void sizeofdata(){
        viewModel.DeclineHairSize().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer != null){
                    binding.LengthOFRejectedHair.setText(String.valueOf(integer));
                }
            }
        });
        viewModel.PendingHireSize().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer != null){
                    binding.LengthOFHirePending.setText(String.valueOf(integer));
                }
            }
        });
    }

    private void setuo_profile(){
        viewModel.getprofile().observe(this, new Observer<com.group.parak.Model.Profile>() {
            @Override
            public void onChanged(Profile profile) {

                if(profile.getProfileImage() != null){
                    Picasso.get().load(profile.getProfileImage()).into(binding.ProfileImage);
                }

                if(profile.getName() != null){
                    binding.Name.setText(profile.getName());
                }
                if(profile.getEmail() != null){
                    binding.Email.setText(profile.getEmail());
                }



            }
        });
    }

    private void init_view(){
        binding.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(HireDashboardPage.this);
        });

        goto_pendingpage(new Pending());
        binding.PendingButtonID.setOnClickListener(view -> {
            if(IsPending){
                IsPending = false;
                IsDecline = true;
                IsActive = true;
                binding.PendingButtonID.setBackgroundResource(R.drawable.coustom_tab_bg);
                binding.DeclineButtonID.setBackgroundResource(R.drawable.null_bg);
                binding.ActiveButtonID.setBackgroundResource(R.drawable.null_bg);
                binding.PendingButtonID.setTextColor(getResources().getColor(R.color.carbon_red_400));
                binding.DeclineButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));
                binding.ActiveButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));

                goto_pendingpage(new Pending());
            }

        });

        binding.DeclineButtonID.setOnClickListener(view -> {

            if(IsDecline){
                IsDecline = false;
                IsPending = true;
                IsActive = true;
                binding.PendingButtonID.setBackgroundResource(R.drawable.null_bg);
                binding.DeclineButtonID.setBackgroundResource(R.drawable.coustom_tab_bg);
                binding.ActiveButtonID.setBackgroundResource(R.drawable.null_bg);
                binding.DeclineButtonID.setTextColor(getResources().getColor(R.color.carbon_red_400));
                binding.PendingButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));
                binding.ActiveButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));

                goto_declinepage(new Decline());
            }

        });

        binding.ActiveButtonID.setOnClickListener(view -> {

            if(IsActive){
                IsActive = false;
                IsPending = true;
                IsDecline = true;
                binding.PendingButtonID.setBackgroundResource(R.drawable.null_bg);
                binding.DeclineButtonID.setBackgroundResource(R.drawable.null_bg);
                binding.ActiveButtonID.setBackgroundResource(R.drawable.coustom_tab_bg);
                binding.PendingButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));
                binding.ActiveButtonID.setTextColor(getResources().getColor(R.color.carbon_red_400));
                binding.DeclineButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));

                goto_active(new Active());
            }

        });

    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(HireDashboardPage.this);
    }

    private void goto_pendingpage(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }
    private void goto_declinepage(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }
    private void goto_active(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }
}