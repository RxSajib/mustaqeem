package com.group.parak.UI.TabFragement;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Profile;
import com.group.parak.R;
import com.group.parak.Response.PostResponse;
import com.group.parak.UI.Activity.ChatPage;
import com.group.parak.UI.Activity.LoginRegPage;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.CommunitiesBinding;
import com.group.parak.databinding.IdcardnotactiveDialogBinding;


public class Communities extends Fragment {

    private CommunitiesBinding binding;
    private ViewModel viewModel;
    private MediaPlayer mediaPlayer;

    public Communities() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.communities, container, false);
        init_view();

        return binding.getRoot();

    }

    private void init_view() {
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.IndiaGroup.setOnClickListener(v -> gotochatpage(DataManager.IndiaCommunity));


        binding.EnglishGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotochatpage(DataManager.EnglishCommunity);
            }
        });

        binding.HindeGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotochatpage(DataManager.HindiUrduCommunity);
            }
        });

        binding.PakistanGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotochatpage(DataManager.PakistanCommunity);
            }
        });

        binding.PhilippnesGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotochatpage(DataManager.PhilippinesCommunity);
            }
        });

        binding.UAEGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotochatpage(DataManager.UAECommunity);
            }
        });
    }

    private void gotochatpage(String Title) {
        viewModel.getUserExists().observe(getViewLifecycleOwner(), new Observer<PostResponse>() {
            @Override
            public void onChanged(PostResponse postResponse) {
                if(postResponse != null){
                    if(postResponse.isJobpost()){
                        viewModel.getprofile().observe(getViewLifecycleOwner(), new Observer<Profile>() {
                            @Override
                            public void onChanged(Profile profile) {
                                if(profile != null){
                                    if(profile.isCardActiveStatus()){
                                        Intent intent = new Intent(getActivity(), ChatPage.class);
                                        intent.putExtra(DataManager.Data, Title);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        Animatoo.animateSlideLeft(getActivity());
                                    }else {
                                        openfeedbackdialog();
                                    }
                                }
                            }
                        });


                    }else {
                        goto_loginpage();
                    }
                }
            }
        });
    }

    private void goto_loginpage(){
        Intent intent = new Intent(getActivity(), LoginRegPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }
    private void openfeedbackdialog(){
        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(getActivity(), R.raw.notification);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity(), R.style.PauseDialog);
        IdcardnotactiveDialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.idcardnotactive_dialog, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        binding.CloseButtonID.setOnClickListener(v -> {
            alertDialog.dismiss();
        });

        binding.FeedbackButton.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
    }
}