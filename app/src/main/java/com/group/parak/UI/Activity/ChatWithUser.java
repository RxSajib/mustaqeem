package com.group.parak.UI.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.group.parak.Adapter.PrivateChatAdapter;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Data;
import com.group.parak.Model.NotificationResponse;
import com.group.parak.Model.OneTwoMessageModel;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Model.Token;
import com.group.parak.R;
import com.group.parak.Response.NotifactionResponse;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.ActivityChatWithUserBinding;

import java.util.ArrayList;
import java.util.List;

public class ChatWithUser extends AppCompatActivity {

    private ActivityChatWithUserBinding binding;
    private ViewModel viewModel;
    private String ReceiverID;
    private PrivateChatAdapter adapter;
    private List<OneTwoMessageModel> list;
    private LinearLayoutManager linearLayoutManager;
    private int PageCount = 30;
    private String Industry;
    private TextView ToolbarTitle;
    private boolean Scrolled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat_with_user);
        Industry = getIntent().getStringExtra(DataManager.Industry);


        init_view();
        send_message();


    }


    private void init_view(){
        ToolbarTitle = findViewById(R.id.ToolbarTitle);
        ToolbarTitle.setText(Industry);
        ReceiverID = getIntent().getStringExtra(DataManager.UID);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(ChatWithUser.this);
            }
        });

        list = new ArrayList<>();
        binding.RecyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.RecyclerView.setLayoutManager(linearLayoutManager);
        adapter = new PrivateChatAdapter();
        binding.RecyclerView.setAdapter(adapter);
        linearLayoutManager.setStackFromEnd(true);

        binding.SwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PageCount = PageCount+30;
                getMessage(PageCount);
            }
        });

        binding.RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Scrolled = true;
                }else {
                    Scrolled = false;
                }
            }
        });
        getMessage(PageCount);
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(ChatWithUser.this);
    }

    private void send_message(){
        binding.SendButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Message = binding.MessageInput.getText().toString().trim();
                if(Message.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Enter your message", Toast.LENGTH_SHORT).show();
                }else {
                    send_messageto_user(Message);

                }
            }
        });
    }

    private void send_messageto_user(String Message){
        send_notifaction_user(ReceiverID, binding.MessageInput.getText().toString().trim());
        binding.MessageInput.setText(null);
        viewModel.OneTwoChatSend(ReceiverID, Message, DataManager.Text).observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){
                    send_notifaction_history(ReceiverID, Message, Industry);
                }else {

                }
            }
        });
    }

    private void getMessage(int PageCount){

        viewModel.getOneTwoMessage(ReceiverID, PageCount).observe(this, new Observer<List<OneTwoMessageModel>>() {
            @Override
            public void onChanged(List<OneTwoMessageModel> oneTwoMessageModels) {


                if(oneTwoMessageModels != null){
                    Log.d("CallTo", "Call");
                    adapter.setList(oneTwoMessageModels);
                    adapter.notifyDataSetChanged();
                    binding.SwipeRefreshLayout.setRefreshing(false);
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    if(Scrolled){
                        binding.RecyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                    }

                }else {
                    adapter.setList(oneTwoMessageModels);
                    adapter.notifyDataSetChanged();
                    binding.SwipeRefreshLayout.setRefreshing(false);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void send_notifaction_user(String ReceiverID, String Message){
        viewModel.getuser_token(ReceiverID).observe(this, new Observer<Token>() {
            @Override
            public void onChanged(Token token) {
                if(token != null){
                 //   send_notifaction_receiver(Message, "send", token.getToken());
                    Log.d("TOKEN", token.getToken());
                }
            }
        });
    }
    private void send_notifaction_receiver(String MessageBody, String Title, String Token){
        Data data = new Data( MessageBody,Title, "", "notifaction_chat_icon" );
        NotificationResponse notifactionResponse = new NotificationResponse(data, Token);

       // todo send notification code

    }


    private void send_notifaction_history(String ReceiverUID, String Message, String Industry){
        viewModel.send_private_chathistory(ReceiverUID, Message, Industry).observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){

                }else {

                }
            }
        });
    }
}