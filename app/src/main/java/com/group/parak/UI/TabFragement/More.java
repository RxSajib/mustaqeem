package com.group.parak.UI.TabFragement;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.LengthOFHair;
import com.group.parak.Model.LengthOFJob;
import com.group.parak.R;
import com.group.parak.Response.PostResponse;
import com.group.parak.UI.Activity.CharityPage;
import com.group.parak.UI.Activity.HireDashboardPage;
import com.group.parak.UI.Activity.IDCardPage;
import com.group.parak.UI.Activity.LoginRegPage;
import com.group.parak.UI.Activity.MembershipPage;
import com.group.parak.UI.Activity.Mission_Vision;
import com.group.parak.UI.Activity.MyJobDashBoard;
import com.group.parak.UI.Activity.Profile;
import com.group.parak.UI.Activity.SplashScreen;
import com.group.parak.UI.Activity.TeamPage;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.LogoutlayoutBinding;
import com.group.parak.databinding.MoreBinding;
import com.squareup.picasso.Picasso;


public class More extends Fragment {

    private ViewModel viewModel;
    private MoreBinding binding;

    public More() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.more, container, false);

        init_view();

        cheack_login();
        getlengthof_item();

        return binding.getRoot();
    }


    private void init_view(){
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        binding.JobSButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewModel.getUserExists().observe(getViewLifecycleOwner(), new Observer<PostResponse>() {
                    @Override
                    public void onChanged(PostResponse postResponse) {

                        if(postResponse.isJobpost()){
                            goto_dashboardpage();
                        }else {
                            goto_loginpage();
                        }


                    }
                });

            }
        });

        binding.JobProvidingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewModel.getUserExists().observe(getViewLifecycleOwner(), new Observer<PostResponse>() {
                    @Override
                    public void onChanged(PostResponse postResponse) {

                        if(postResponse.isJobpost()){
                            goto_hairdashboard();
                        }else {
                            goto_loginpage();
                        }


                    }
                });
            }
        });



        binding.LoginButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goto_loginpage();
            }
        });

        binding.PhoneNumberID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.getUserExists().observe(getViewLifecycleOwner(), new Observer<PostResponse>() {
                    @Override
                    public void onChanged(PostResponse postResponse) {
                        if(postResponse != null){
                            if(postResponse.isJobpost()){
                                goto_profilepage();
                            }else {
                                goto_loginpage();
                            }
                        }
                    }
                });
            }
        });

        binding.ProfileButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.getUserExists().observe(getViewLifecycleOwner(), new Observer<PostResponse>() {
                    @Override
                    public void onChanged(PostResponse postResponse) {
                        if(postResponse != null){
                            if(postResponse.isJobpost()){
                                goto_profilepage();
                            }else {
                                goto_loginpage();
                            }
                        }
                    }
                });

            }
        });

        binding.MyID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.getUserExists().observe(getViewLifecycleOwner(), new Observer<PostResponse>() {
                    @Override
                    public void onChanged(PostResponse postResponse) {
                        if(postResponse != null){
                            if(postResponse.isJobpost()){
                                goto_idcardpage();
                            }else {
                                goto_loginpage();
                            }
                        }
                    }
                });

            }
        });

        binding.CharityID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goto_charitypage();
            }
        });

        binding.MissionAndVisionID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotomission_visionpage();
            }
        });

        binding.MemberID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goto_membershippage();
            }
        });

        binding.FacebookID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(DataManager.FacebookLinks));
                startActivity(intent);
            }
        });

        binding.YouTubeID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(DataManager.YoutubeLinks));
                startActivity(intent);
            }
        });

        binding.OurTeamID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TeamPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                Animatoo.animateSlideLeft(getActivity());
            }
        });


        setprofileimage();
    }

    private void setprofileimage(){
        viewModel.getprofile().observe(getViewLifecycleOwner(), new Observer<com.group.parak.Model.Profile>() {
            @Override
            public void onChanged(com.group.parak.Model.Profile profile) {
                if(profile != null){
                    Picasso.get().load(profile.getProfileImage()).into(binding.ImageID);
                }

            }
        });
    }

    private void goto_profilepage(){
        Intent intent = new Intent(getActivity(), Profile.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void goto_loginpage(){
        Intent intent = new Intent(getActivity(), LoginRegPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }

    private void goto_idcardpage(){
        Intent intent = new Intent(getActivity(), IDCardPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void goto_charitypage(){
        Intent intent = new Intent(getActivity(), CharityPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void gotomission_visionpage(){
        Intent intent = new Intent(getActivity(), Mission_Vision.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void goto_membershippage(){
        Intent intent = new Intent(getActivity(), MembershipPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void cheack_login(){
        viewModel.getUserExists().observe(getViewLifecycleOwner(), new Observer<PostResponse>() {
            @Override
            public void onChanged(PostResponse postResponse) {
                if(postResponse.isJobpost()){
                    binding.LoginButtonID.setVisibility(View.VISIBLE);
                    binding.LogOutID.setVisibility(View.VISIBLE);
                }else {
                    binding.LoginButtonID.setVisibility(View.VISIBLE);
                    binding.LogOutID.setVisibility(View.GONE);
                }
            }
        });

        binding.LogOutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity(), R.style.MaterialAlertDialog_rounded);
                LogoutlayoutBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.logoutlayout, null, false);
                Mbuilder.setView(binding.getRoot());

                AlertDialog alertDialog = Mbuilder.create();
                alertDialog.show();

                binding.CancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                binding.LogoutButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewModel.logoutaccount();
                        Intent intent = new Intent(getActivity(), SplashScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        Animatoo.animateSlideLeft(getActivity());
                    }
                });

            }
        });
    }

    private void goto_dashboardpage(){
        Intent intent = new Intent(getActivity(), MyJobDashBoard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void getlengthof_item(){
        viewModel.getLengthofjob().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer != null){
                    binding.LengthJob.setText(String.valueOf(integer));
                }
            }
        });


        viewModel.getLengthofHire().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer != null){
                    binding.LengthOFHire.setText(String.valueOf(integer));
                }
            }
        });
    }

    private void goto_hairdashboard(){
        Intent intent = new Intent(getActivity(), HireDashboardPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }
}