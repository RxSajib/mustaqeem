package com.group.parak.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Adapter.SpinnerAdapter;
import com.group.parak.CashMemory.SalaryPref;
import com.group.parak.Data.DataManager;
import com.group.parak.Data.JobCurrencySpinnerData;
import com.group.parak.Model.JobModel;
import com.group.parak.Model.ResponseCode;
import com.group.parak.R;
import com.group.parak.ViewModel.JobPostViewModel;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.JobPostPageBinding;
import com.group.parak.databinding.SuccessDialoagBinding;
import com.group.parak.databinding.UpdatedialogBinding;

public class UpdateJob extends AppCompatActivity {

    private com.group.parak.databinding.UpdatejobBinding binding;
    private ViewModel viewModel;
    private long UID;

    private String StartSalaryData, EndSalaryData;
    private SalaryPref salaryPref;

    private String JobWorkingStatus = "";
    private String IndustryStatus = "";


    private FirebaseAuth Mauth;
    private FirebaseFirestore MuserFireStore;
    private FirebaseFirestore MPostFireStore;
    private EditText rewardsInput;
    private EditText locationInput;
    private JobPostViewModel jobPostViewModel;
    private AlertDialog spotsDialog;
    private String Industry = "";
    private String Currency = null;

    private SpinnerAdapter spinnerAdapter;
    private IOSDialog iosDialog;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.updatejob);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        UID = getIntent().getLongExtra(DataManager.UID, UID);


        getdata();
        getCountryDialCode();
        initview();
        setCurrencySpinner();
    }

    private void getdata(){
        viewModel.getPendingJobdetails(UID).observe(this, new Observer<JobModel>() {
            @Override
            public void onChanged(JobModel jobModel) {
                if(jobModel != null){
                    if(jobModel.getJobType().equals(DataManager.Fulltime)){
                        JobWorkingStatus = DataManager.Fulltime;
                        binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                    }if(jobModel.getJobType().equals(DataManager.Freshers)){
                        JobWorkingStatus = DataManager.Freshers;
                        binding.FreshersButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                    }if(jobModel.getJobType().equals(DataManager.Work_At_home)){
                        JobWorkingStatus = DataManager.Work_At_home;
                        binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                    }if(jobModel.getJobType().equals(DataManager.Freelance)){
                        JobWorkingStatus = DataManager.Freelance;
                        binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                    }if(jobModel.getJobType().equals(DataManager.Intership)){
                        JobWorkingStatus = DataManager.Intership;
                        binding.IntershipButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                    }if(jobModel.getJobType().equals(DataManager.Partime)){
                        JobWorkingStatus = DataManager.Partime;
                        binding.PartimeButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                    }if(jobModel.getJobType().equals(DataManager.Contract)){
                        JobWorkingStatus = DataManager.Contract;
                        binding.ContractButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                    }
                    binding.StartSalaryID.setText(jobModel.getStartSalary());
                    binding.EndSalaryID.setText(jobModel.getEndSalary());
                    binding.JobDetailsID.setText(jobModel.getExperience());
                    binding.RewardInputID.setText(jobModel.getRewards());
                    binding.Location.setText(jobModel.getLocation());
                    binding.ContactInformactionID.setText(jobModel.getContract());

                    if(jobModel.getIndustry().equals(DataManager.IT_Software)){
                        IndustryStatus = DataManager.IT_Software;
                        binding.ITSoftwareID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.BpoCallCenter)){
                        IndustryStatus = DataManager.BpoCallCenter;
                        binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Banking)){
                        IndustryStatus = DataManager.Banking;
                        binding.BankingID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Consultancy)){
                        IndustryStatus = DataManager.Consultancy;
                        binding.ConsultancyID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Marketing)){
                        IndustryStatus = DataManager.Marketing;
                        binding.MarktingID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Human_Resource)){
                        IndustryStatus = DataManager.Human_Resource;
                        binding.HumanResourceID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Real_Estate)){
                        IndustryStatus = DataManager.Real_Estate;
                        binding.RealEsateID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Insurance)){
                        IndustryStatus = DataManager.Insurance;
                        binding.InsureanceID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Writer)){
                        IndustryStatus = DataManager.Writer;
                        binding.WriterID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.WebDesigner)){
                        IndustryStatus = DataManager.WebDesigner;
                        binding.WebDesignerID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Waiters)){
                        IndustryStatus = DataManager.Waiters;
                        binding.WaiterID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Vet)){
                        IndustryStatus = DataManager.Vet;
                        binding.VetID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.TvPresenter)){
                        IndustryStatus = DataManager.TvPresenter;
                        binding.TvPresenterID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.TvCameraman)){
                        IndustryStatus = DataManager.TvCameraman;
                        binding.TvCaeramanID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Trucker)){
                        IndustryStatus = DataManager.Trucker;
                        binding.TruckerID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.TravelAgent)){
                        IndustryStatus = DataManager.TravelAgent;
                        binding.TravelAgentID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(jobModel.getIndustry().equals(DataManager.Translator)){
                        IndustryStatus = DataManager.Translator;
                        binding.TranslatorID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else {
                        IndustryStatus = jobModel.getIndustry();
                        binding.IndustryCheckBox.setChecked(true);
                        binding.IndustryInput.setVisibility(View.VISIBLE);
                        binding.IndustryInput.setText(jobModel.getIndustry());
                    }
                }
            }
        });
    }

    private void setCurrencySpinner() {
        spinnerAdapter = new SpinnerAdapter(getApplicationContext(), JobCurrencySpinnerData.models());
        binding.SpinnerID.setAdapter(spinnerAdapter);

        binding.SpinnerID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int spinnerposition = (int) parent.getItemAtPosition(position);
                if (spinnerposition == 0) {
                    Currency = null;
                }
                if (spinnerposition == 1) {
                    Currency = DataManager.Pakistan_Rupee;
                }
                if (spinnerposition == 2) {
                    Currency = DataManager.India_Rupee;
                }
                if (spinnerposition == 3) {
                    Currency = DataManager.AED;
                }
                if (spinnerposition == 4) {
                    Currency = DataManager.BDT;
                }
                if (spinnerposition == 5) {
                    Currency = DataManager.Saudi_Riyal;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String getCountryDialCode() {
        String contryId = null;
        String contryDialCode = null;

        TelephonyManager telephonyMngr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        contryId = telephonyMngr.getSimCountryIso().toUpperCase();
        String[] arrContryCode = this.getResources().getStringArray(R.array.DialingCountryCode);
        for (int i = 0; i < arrContryCode.length; i++) {
            String[] arrDial = arrContryCode[i].split(",");
            if (arrDial[1].trim().equals(contryId.trim())) {
                contryDialCode = arrDial[0];
                break;
            }
        }
        return contryDialCode;
    }

    private void clearcheckindustry() {

        IndustryStatus = "";
        binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
    }

    private void initview() {
        jobPostViewModel = new ViewModelProvider(this).get(JobPostViewModel.class);
        locationInput = findViewById(R.id.Location);
        MPostFireStore = FirebaseFirestore.getInstance();
        rewardsInput = findViewById(R.id.RewardInputID);
        Mauth = FirebaseAuth.getInstance();
        MuserFireStore = FirebaseFirestore.getInstance();

        binding.IndustryCheckBox.setOnClickListener(v -> {
            if (binding.IndustryCheckBox.isChecked()) {
                binding.IndustryInput.setVisibility(View.VISIBLE);
                clearcheckindustry();
            } else {
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
            }
        });

        binding.ITSoftwareID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);

                IndustryStatus = DataManager.IT_Software;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.BpoCallCenterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.BpoCallCenter;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.BankingID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Banking;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.ConsultancyID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Consultancy;


                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.MarktingID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Marketing;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.HumanResourceID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Human_Resource;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.RealEsateID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Real_Estate;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.InsureanceID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Insurance;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.WriterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Writer;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.WebDesignerID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.WebDesigner;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.WaiterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Waiters;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.VetID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Vet;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TvPresenterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.TvPresenter;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TvCaeramanID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.TvCameraman;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TruckerID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Trucker;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TravelAgentID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.TravelAgent;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TranslatorID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Translator;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_button_click_event);
            }
        });
        //todo industry -----------

        //todo job type buttons

        binding.FullTimeButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Fulltime;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.FreshersButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Freshers;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.WorkAtHomeButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Work_At_home;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.FreelanceButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Freelance;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.IntershipButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Intership;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.PartimeButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Partime;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.ContractButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Contract;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_button_click_event);
            }
        });
        //todo job type buttons


        salaryPref = new SalaryPref(getApplicationContext());


        binding.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Animatoo.animateSlideRight(UpdateJob.this);
            }
        });


        binding.JobPostButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploading_data();
            }
        });
    }



    private void uploading_data() {


        String Start_salary = binding.StartSalaryID.getText().toString();
        String End_salary = binding.EndSalaryID.getText().toString();
        String Experience = binding.JobDetailsID.getText().toString().trim();
        String Reward = rewardsInput.getText().toString().trim();
        String Location = locationInput.getText().toString().trim();
        String Contact = binding.ContactInformactionID.getText().toString().trim();


        if (!binding.IndustryInput.getText().toString().trim().isEmpty()) {
            Industry = binding.IndustryInput.getText().toString().trim();
        } else {
            Industry = IndustryStatus;
        }

        if (JobWorkingStatus == "") {
            Toast.makeText(UpdateJob.this, "Job type require", Toast.LENGTH_SHORT).show();
        } else if (Currency == null) {
            Toast.makeText(UpdateJob.this, "Select your currency", Toast.LENGTH_SHORT).show();
        } else if (Start_salary.isEmpty()) {
            Toast.makeText(UpdateJob.this, "Start salary require", Toast.LENGTH_SHORT).show();
        } else if (End_salary.isEmpty()) {
            Toast.makeText(UpdateJob.this, "End salary require", Toast.LENGTH_SHORT).show();
        } else if (Integer.valueOf(Start_salary) > Integer.valueOf(End_salary)) {
            Toast.makeText(UpdateJob.this, "End salary must bigger then start salary", Toast.LENGTH_SHORT).show();
        } else if (Experience.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Experience request", Toast.LENGTH_SHORT).show();
        } else if (Reward.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Reward require", Toast.LENGTH_SHORT).show();
        } else if (Location.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Location require", Toast.LENGTH_SHORT).show();
        } else if (Contact.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Location require", Toast.LENGTH_SHORT).show();
        } else if (Industry == "") {
            Toast.makeText(getApplicationContext(), "Industry require", Toast.LENGTH_SHORT).show();
        } else {
            progressdialoag();
            viewModel.UpdateJob(JobWorkingStatus, Start_salary, End_salary, Experience, Reward, Location, Contact, Industry, Currency, UID)
                    .observe(this, new Observer<Boolean>() {
                        @Override
                        public void onChanged(Boolean aBoolean) {
                            if(aBoolean){
                                iosDialog.dismiss();
                            }else {
                                iosDialog.dismiss();
                            }
                        }
                    });
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Animatoo.animateSlideRight(UpdateJob.this);
    }

    private void successdialoag() {

        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(UpdateJob.this, R.raw.success);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }

        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(UpdateJob.this, R.style.PauseDialog);
        UpdatedialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(UpdateJob.this), R.layout.updatedialog, null, false);
        Mbuilder.setView(binding.getRoot());

        androidx.appcompat.app.AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        binding.OkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(UpdateJob.this);
            }
        });

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
                Animatoo.animateSlideRight(UpdateJob.this);
            }
        });
    }


    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(UpdateJob.this)
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Updating")
                .build();

        iosDialog.show();
    }
}