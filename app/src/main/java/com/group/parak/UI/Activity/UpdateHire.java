package com.group.parak.UI.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.HeirModel;
import com.group.parak.Model.ResponseCode;
import com.group.parak.R;
import com.group.parak.Response.UploadAssetsResponse;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.SuccessDialoagBinding;
import com.group.parak.databinding.UpdatedialogBinding;
import com.group.parak.databinding.UpdatehireBinding;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dmax.dialog.SpotsDialog;

public class UpdateHire extends AppCompatActivity {

    private UpdatehireBinding binding;
    private long UID;
    private ViewModel viewModel;
    private String JobType = "";
    private boolean isjobvalid = false;
    private String ValidLink;
    private String CvLink;
    private static final int PDFCODE = 1;
    private String PdfDownloadUri;
    private AlertDialog spotsDialog;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.updatehire);
        UID = getIntent().getLongExtra(DataManager.UID, 0);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);



        getdata(UID);
        init_view();
    }


    private void getdata(long UID){
        viewModel.PendingHireSingleData(String.valueOf(UID)).observe(this, new Observer<HeirModel>() {
            @Override
            public void onChanged(HeirModel heirModel) {
                if(heirModel != null){
                    binding.Name.setText(heirModel.getName());
                    binding.Location.setText(heirModel.getLocation());
                    binding.Phone.setText(heirModel.getPhone());
                    binding.Email.setText(heirModel.getEmail());
                    binding.CVLInk.setText(heirModel.getCVLink());

                    if(heirModel.getWork().equals(DataManager.Sales)){
                        binding.SalesID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Business_Development)){
                        binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Accounting)){
                        binding.AccountingID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Jobs_Wanted)){
                        binding.JobsWantedID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Media)){
                        binding.MediaID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Legal_Services)){
                        binding.LegalServicesID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Internet)){
                        binding.InternetID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.GraphicDesign)){
                        binding.GraphicDesignID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Writer)){
                        binding.WriterID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.WebDesigner)){
                        binding.WebDesignerID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Waiters)){
                        binding.WaiterID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Vet)){
                        binding.VetID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.TvPresenter)){
                        binding.TvPresenterID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.TvCameraman)){
                        binding.TvCaeramanID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Food_Beverages)){
                        binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Engineering)){
                        binding.EngineeringID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else if(heirModel.getWork().equals(DataManager.Translator)){
                        binding.TranslatorID.setBackgroundResource(R.drawable.job_button_click_event);
                    }else {
                        binding.CheckBoxID.setChecked(true);
                        binding.MyQulactionID.setVisibility(View.VISIBLE);
                        binding.MyQulactionID.setText(heirModel.getWork());
                    }
                }
            }
        });
    }

    private void init_view() {

        binding.SalesID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Sales;
                binding.SalesID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.BusinessDevelopmentID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Business_Development;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.AccountingID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Accounting;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.JobsWantedID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Jobs_Wanted;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.MediaID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Media;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.LegalServicesID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Legal_Services;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.InternetID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Internet;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.GraphicDesignID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.GraphicDesign;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.WriterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Waiters;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.WebDesignerID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.WebDesigner;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.WaiterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Writer;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.VetID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Vet;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.TvPresenterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.TvPresenter;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.TvCaeramanID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.TvCameraman;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.FoodBeveragesID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Food_Beverages;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.EngineeringID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Engineering;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.TranslatorID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Translator;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_button_click_event);

                clearcheackbox();
            }
        });

        binding.UploadCvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                startActivityForResult(intent, PDFCODE);
            }
        });

        binding.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Animatoo.animateSlideRight(UpdateHire.this);
            }
        });

        binding.CheckBoxID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.CheckBoxID.isChecked()) {
                    binding.MyQulactionID.setVisibility(View.VISIBLE);
                    JobType = "";
                    clerwork();
                } else {
                    binding.MyQulactionID.setVisibility(View.GONE);
                    Log.d("status", "unchecked");
                    JobType = "";
                    binding.MyQulactionID.setText(null);
                    clerwork();
                }
            }
        });


        binding.JobPostButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload_data();

            }
        });


    }

    private boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }
    private void clearcheackbox(){
        binding.MyQulactionID.setText(null);
        binding.CheckBoxID.setChecked(false);
        binding.MyQulactionID.setVisibility(View.GONE);
    }

    private void upload_data() {

        String Name = binding.Name.getText().toString().trim();
        String Location = binding.Location.getText().toString().trim();
        String PhoneNumber = binding.Phone.getText().toString().trim();
        String Email = binding.Email.getText().toString().trim();
        CvLink = binding.CVLInk.getText().toString().trim();
        String InputJob = binding.MyQulactionID.getText().toString().trim();


        if (Name.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Name require", Toast.LENGTH_SHORT).show();
        }
        else if (Location.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Location require", Toast.LENGTH_SHORT).show();
        }
        else if (PhoneNumber.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Location require", Toast.LENGTH_SHORT).show();
        }
        else if (PhoneNumber.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Phone require", Toast.LENGTH_SHORT).show();
        }
        else if (Email.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Email require", Toast.LENGTH_SHORT).show();
        }

        else if (JobType == "" && InputJob.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Job type require", Toast.LENGTH_SHORT).show();
        }
        else {
            if (JobType != "") {
                uploadpost(PdfDownloadUri, Name, Location, PhoneNumber, Email, JobType);
            } else if (!InputJob.isEmpty()) {
                uploadpost(PdfDownloadUri, Name, Location, PhoneNumber, Email, InputJob);
            }
        }


    }

    private void uploadpost(String Pdfuri, String Name, String Location, String Phone, String Email, String Work){
        spotsDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Updating")
                .build();
        spotsDialog.show();
        if(!CvLink.isEmpty()){
            if(!isValidUrl(CvLink)){
                Toast.makeText(getApplicationContext(), "Cv link not valid", Toast.LENGTH_SHORT).show();
                spotsDialog.dismiss();
            }else {
                viewModel.HireUpdate(Name, Location, Phone, Email, Work, CvLink, Pdfuri, UID).observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if(aBoolean){
                            spotsDialog.dismiss();
                            successuploaddialoag();
                        }else {
                            spotsDialog.dismiss();
                        }
                    }
                });

            }
        }else {
            viewModel.HireUpdate(Name, Location, Phone, Email, Work, CvLink, Pdfuri, UID).observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if(aBoolean){
                        spotsDialog.dismiss();
                    }else {
                        spotsDialog.dismiss();
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PDFCODE && resultCode == RESULT_OK) {
            spotsDialog = new SpotsDialog.Builder()
                    .setContext(this)
                    .setMessage("Uploading")
                    .build();
            spotsDialog.show();
            viewModel.upload_hairCV(data.getData()).observe(this, new Observer<UploadAssetsResponse>() {
                @Override
                public void onChanged(UploadAssetsResponse uploadAssetsResponse) {
                    if (uploadAssetsResponse.getCode() == DataManager.SuccessCode) {
                        uploadAssetsResponse.setCode(DataManager.DefaultCode);
                        PdfDownloadUri = uploadAssetsResponse.getPosterpath();
                        spotsDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Upload success", Toast.LENGTH_SHORT).show();
                        binding.PdfUploadImage.setVisibility(View.VISIBLE);
                        binding.PdfUploadText.setText("upload success");
                        binding.PdfUploadText.setTextColor(getResources().getColor(R.color.carbon_black_54));

                    } else {
                        uploadAssetsResponse.setCode(DataManager.DefaultCode);
                        Toast.makeText(getApplicationContext(), "Error upload document", Toast.LENGTH_SHORT).show();
                        spotsDialog.dismiss();
                        binding.PdfUploadImage.setVisibility(View.GONE);
                        binding.PdfUploadText.setText("Error upload");
                        binding.PdfUploadText.setTextColor(getResources().getColor(R.color.carbon_red_400));
                    }
                }
            });
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSlideRight(UpdateHire.this);
    }

    private void clerwork() {
        binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
    }


    private void successuploaddialoag(){

        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(UpdateHire.this, R.raw.success);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }

        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(UpdateHire.this, R.style.PauseDialog);
        UpdatedialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(UpdateHire.this), R.layout.updatedialog, null, false);
        Mbuilder.setView(binding.getRoot());

        androidx.appcompat.app.AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
                Animatoo.animateSlideRight(UpdateHire.this);
            }
        });

        binding.OkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(UpdateHire.this);
            }
        });
    }
}