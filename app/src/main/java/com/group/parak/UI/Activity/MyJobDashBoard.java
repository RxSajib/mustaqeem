package com.group.parak.UI.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.group.parak.UI.JobDashboardFragement.ActiveJob;
import com.group.parak.UI.JobDashboardFragement.DeclintJob;
import com.group.parak.UI.JobDashboardFragement.PendingJob;
import com.group.parak.Model.LengthOFPendingJobs;
import com.group.parak.Model.LengthOFRejectedJob;
import com.group.parak.Model.Profile;
import com.group.parak.R;
import com.group.parak.ViewModel.JobViewModel;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.MyDashBoardBinding;
import com.squareup.picasso.Picasso;

public class MyJobDashBoard extends AppCompatActivity {

    private ViewModel viewModel;
    private JobViewModel jobViewModel;
    private MyDashBoardBinding binding;
    private boolean IsPending = false, IsDecline = true, IsActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.my_dash_board);

        init_view();
    }

    private void init_view(){
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        jobViewModel = new ViewModelProvider(this).get(JobViewModel.class);

        binding.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(MyJobDashBoard.this);
            }
        });

        goto_pendingpage(new PendingJob());

        binding.PendingButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(IsPending){
                    IsPending = false;
                    IsDecline = true;
                    IsActive = true;
                    binding.PendingButtonID.setBackgroundResource(R.drawable.coustom_tab_bg);
                    binding.DeclineButtonID.setBackgroundResource(R.drawable.null_bg);
                    binding.ActiveButtonID.setBackgroundResource(R.drawable.null_bg);
                    binding.PendingButtonID.setTextColor(getResources().getColor(R.color.carbon_red_400));
                    binding.DeclineButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));
                    binding.ActiveButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));

                    goto_pendingpage(new PendingJob());
                }

            }
        });

        binding.DeclineButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(IsDecline){
                    IsDecline = false;
                    IsPending = true;
                    IsActive = true;
                    binding.PendingButtonID.setBackgroundResource(R.drawable.null_bg);
                    binding.DeclineButtonID.setBackgroundResource(R.drawable.coustom_tab_bg);
                    binding.ActiveButtonID.setBackgroundResource(R.drawable.null_bg);
                    binding.DeclineButtonID.setTextColor(getResources().getColor(R.color.carbon_red_400));
                    binding.PendingButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));
                    binding.ActiveButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));

                    goto_declinepage(new DeclintJob());
                }

            }
        });

        binding.ActiveButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(IsActive){
                    IsActive = false;
                    IsPending = true;
                    IsDecline = true;
                    binding.PendingButtonID.setBackgroundResource(R.drawable.null_bg);
                    binding.DeclineButtonID.setBackgroundResource(R.drawable.null_bg);
                    binding.ActiveButtonID.setBackgroundResource(R.drawable.coustom_tab_bg);
                    binding.PendingButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));
                    binding.ActiveButtonID.setTextColor(getResources().getColor(R.color.carbon_red_400));
                    binding.DeclineButtonID.setTextColor(getResources().getColor(R.color.carbon_black_54));

                    goto_activejobpage(new ActiveJob());
                }

            }
        });

        setuo_profile();


        getlength_ofjobs();
    }

    private void getlength_ofjobs(){
        viewModel.getLengthOFPendingJobs().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer != null){
                    binding.LengthOFJobPending.setText(String.valueOf(integer));
                }
            }
        });
        viewModel.getlengthOFRejectedJob().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer != null){
                    binding.LengthOFRejectedJob.setText(String.valueOf(integer));
                }
            }
        });
    }





    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(MyJobDashBoard.this);
    }

    private void setuo_profile(){
        viewModel.getprofile().observe(this, new Observer<Profile>() {
            @Override
            public void onChanged(Profile profile) {

                    if(profile.getProfileImage() != null){
                        Picasso.get().load(profile.getProfileImage()).into(binding.ProfileImage);
                    }

                    if(profile.getName() != null){
                        binding.Name.setText(profile.getName());
                    }
                    if(profile.getEmail() != null){
                        binding.Email.setText(profile.getEmail());
                    }



            }
        });
    }

    private void goto_pendingpage(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.FrameLayout, fragment);
            transaction.commit();
        }
    }

    private void goto_declinepage(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.FrameLayout, fragment);
            transaction.commit();
        }
    }

    private void goto_activejobpage(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.FrameLayout, fragment);
            transaction.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.Notification){
            goto_notifactionpage();
        }
        return true;
    }

    private void goto_notifactionpage(){
        Intent intent = new Intent(getApplicationContext(), MyNotifaction.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(MyJobDashBoard.this);
    }
}