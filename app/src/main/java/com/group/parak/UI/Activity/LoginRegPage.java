package com.group.parak.UI.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.group.parak.R;
import com.group.parak.Response.PostResponse;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.LoginregBinding;


public class LoginRegPage extends AppCompatActivity {



    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 100;
    private FirebaseAuth Mauth;
    private ViewModel viewModel;
    private LoginregBinding binding;
    private IOSDialog iosDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.loginreg);

        creating_request();
        set_statusbarcolor();
        init_view();
        Mauth = FirebaseAuth.getInstance();
    }

    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(LoginRegPage.this)
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Login your Account")
                .build();

        iosDialog.show();
    }


    private void set_statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_green_400));
        }
        else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_green_400));
        }
    }

    private void init_view(){
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        binding.ProvidingButton.setOnClickListener(v -> goto_policypage());
        binding.CheckBoxID.setOnClickListener(v -> setbutton_action());

        disable_button();
    }

    private void goto_policypage(){
        Intent intent = new Intent(getApplicationContext(), PrivacyPolicy.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(LoginRegPage.this);
    }

    private void setbutton_action(){
        if(binding.CheckBoxID.isChecked()){
            enable_button();
            binding.PhoneSignInButton.setOnClickListener(v -> goto_phoneloginpage());
            binding.GoogleButtonID.setOnClickListener(view -> signIn());
        }
        else {
            disable_button();
        }
    }

    private void disable_button(){
        binding.PhoneSignInButton.setBackground(getResources().getDrawable(R.drawable.phone_otpbuttonbg_disable));
        binding.GoogleButtonID.setBackground(getResources().getDrawable(R.drawable.googlesignin_buttonbg_disable));
        binding.FacebookButtonID.setBackground(getResources().getDrawable(R.drawable.facebookbuttonbg_disable));

        binding.PhoneSignInButton.setEnabled(false);
        binding.GoogleButtonID.setEnabled(false);
        binding.FacebookButtonID.setEnabled(false);
    }

    private void enable_button(){
        binding.PhoneSignInButton.setBackground(getResources().getDrawable(R.drawable.phone_otpbuttonbg));
        binding.GoogleButtonID.setBackground(getResources().getDrawable(R.drawable.googlesignin_buttonbg));
        binding.FacebookButtonID.setBackground(getResources().getDrawable(R.drawable.facebookbuttonbg));

        binding.PhoneSignInButton.setEnabled(true);
        binding.GoogleButtonID.setEnabled(true);
        binding.FacebookButtonID.setEnabled(true);
    }
    


    private void goto_phoneloginpage(){
        Intent intent = new Intent(getApplicationContext(), PhoneSignin.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(LoginRegPage.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSwipeRight(LoginRegPage.this);
    }


    private void creating_request(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {

                GoogleSignInAccount account = task.getResult(ApiException.class);

                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {

                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {
        progressdialoag();
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        Mauth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            String email = task.getResult().getUser().getEmail();
                            String name = task.getResult().getUser().getDisplayName();
                            String phonenumber = task.getResult().getUser().getPhoneNumber();
                            Uri photouri = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl();
                            setup_profile(email, name, phonenumber, photouri);


                        } else {

                            Toast.makeText(LoginRegPage.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }

    private void setup_profile(String email, String name, String number, Uri photo){
        viewModel.getemailuser_info(email, name, number, photo)
                .observe( this, new Observer<PostResponse>() {
                    @Override
                    public void onChanged(PostResponse postResponse) {
                        if(postResponse.isJobpost()){
                            iosDialog.dismiss();
                            finish();
                            Animatoo.animateSlideRight(LoginRegPage.this);
                        }else {
                            iosDialog.dismiss();
                        }
                    }
                });
    }
}