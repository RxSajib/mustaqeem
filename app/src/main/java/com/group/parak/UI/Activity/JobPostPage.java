package com.group.parak.UI.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.group.parak.Adapter.SpinnerAdapter;
import com.group.parak.CashMemory.SalaryPref;
import com.group.parak.Data.DataManager;
import com.group.parak.Data.JobCurrencySpinnerData;
import com.group.parak.Model.ResponseCode;
import com.group.parak.R;
import com.group.parak.ViewModel.JobPostViewModel;
import com.group.parak.databinding.ErrorDialogBinding;
import com.group.parak.databinding.JobPostPageBinding;
import com.group.parak.databinding.SuccessDialoagBinding;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class JobPostPage extends AppCompatActivity {


    private String StartSalaryData, EndSalaryData;
    private SalaryPref salaryPref;

    private String JobWorkingStatus = "";
    private String IndustryStatus = "";


    private FirebaseAuth Mauth;
    private FirebaseFirestore MuserFireStore;
    private FirebaseFirestore MPostFireStore;
    private EditText rewardsInput;
    private EditText locationInput;
    private JobPostViewModel jobPostViewModel;
    private JobPostPageBinding binding;
    private String Industry = "";
    private String Currency = null;

    private SpinnerAdapter spinnerAdapter;
    private IOSDialog iosDialog;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_post_page);
        binding = DataBindingUtil.setContentView(this, R.layout.job_post_page);



        getCountryDialCode();
        initview();

        setCurrencySpinner();
    }

    private void setCurrencySpinner() {
        spinnerAdapter = new SpinnerAdapter(getApplicationContext(), JobCurrencySpinnerData.models());
        binding.SpinnerID.setAdapter(spinnerAdapter);

        binding.SpinnerID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int spinnerposition = (int) parent.getItemAtPosition(position);
                if (spinnerposition == 0) {
                    Currency = null;
                }
                if (spinnerposition == 1) {
                    Currency = DataManager.Pakistan_Rupee;
                }
                if (spinnerposition == 2) {
                    Currency = DataManager.India_Rupee;
                }
                if (spinnerposition == 3) {
                    Currency = DataManager.AED;
                }
                if (spinnerposition == 4) {
                    Currency = DataManager.BDT;
                }
                if (spinnerposition == 5) {
                    Currency = DataManager.Saudi_Riyal;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String getCountryDialCode() {
        String contryId = null;
        String contryDialCode = null;

        TelephonyManager telephonyMngr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        contryId = telephonyMngr.getSimCountryIso().toUpperCase();
        String[] arrContryCode = this.getResources().getStringArray(R.array.DialingCountryCode);
        for (int i = 0; i < arrContryCode.length; i++) {
            String[] arrDial = arrContryCode[i].split(",");
            if (arrDial[1].trim().equals(contryId.trim())) {
                contryDialCode = arrDial[0];
                break;
            }
        }
        return contryDialCode;
    }

    private void clearcheckindustry() {

        IndustryStatus = "";
        binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
    }

    private void initview() {
        jobPostViewModel = new ViewModelProvider(this).get(JobPostViewModel.class);
        locationInput = findViewById(R.id.Location);
        MPostFireStore = FirebaseFirestore.getInstance();
        rewardsInput = findViewById(R.id.RewardInputID);
        Mauth = FirebaseAuth.getInstance();
        MuserFireStore = FirebaseFirestore.getInstance();

        binding.IndustryCheckBox.setOnClickListener(v -> {
            if (binding.IndustryCheckBox.isChecked()) {
                binding.IndustryInput.setVisibility(View.VISIBLE);
                clearcheckindustry();
            } else {
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
            }
        });

        binding.ITSoftwareID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);

                IndustryStatus = DataManager.IT_Software;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.BpoCallCenterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.BpoCallCenter;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.BankingID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Banking;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.ConsultancyID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Consultancy;


                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.MarktingID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Marketing;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.HumanResourceID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Human_Resource;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.RealEsateID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Real_Estate;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.InsureanceID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Insurance;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.WriterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Writer;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.WebDesignerID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.WebDesigner;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.WaiterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Waiters;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.VetID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Vet;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TvPresenterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.TvPresenter;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TvCaeramanID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.TvCameraman;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TruckerID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Trucker;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TravelAgentID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.TravelAgent;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.TranslatorID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.IndustryCheckBox.setChecked(false);
                binding.IndustryInput.setVisibility(View.GONE);
                binding.IndustryInput.setText(null);
                IndustryStatus = DataManager.Translator;

                binding.ITSoftwareID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BpoCallCenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BankingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ConsultancyID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MarktingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.HumanResourceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.RealEsateID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InsureanceID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TruckerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TravelAgentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_button_click_event);
            }
        });
        //todo industry -----------

        //todo job type buttons

        binding.FullTimeButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Fulltime;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.FreshersButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Freshers;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.WorkAtHomeButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Work_At_home;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.FreelanceButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Freelance;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.IntershipButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Intership;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.PartimeButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Partime;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_type_unselected);
            }
        });

        binding.ContractButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobWorkingStatus = DataManager.Contract;
                binding.FullTimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreshersButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WorkAtHomeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FreelanceButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.IntershipButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.PartimeButtonID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.ContractButtonID.setBackgroundResource(R.drawable.job_button_click_event);
            }
        });
        //todo job type buttons


        salaryPref = new SalaryPref(getApplicationContext());


        binding.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Animatoo.animateSlideRight(JobPostPage.this);
            }
        });


        binding.JobPostButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploading_data();
            }
        });
    }



    private void uploading_data() {

        String Start_salary = binding.StartSalaryID.getText().toString();
        String End_salary = binding.EndSalaryID.getText().toString();
        String Experience = binding.JobDetailsID.getText().toString().trim();
        String Reward = rewardsInput.getText().toString().trim();
        String Location = locationInput.getText().toString().trim();
        String Contact = binding.ContactInformactionID.getText().toString().trim();


        if (!binding.IndustryInput.getText().toString().trim().isEmpty()) {
            Industry = binding.IndustryInput.getText().toString().trim();
        } else {
            Industry = IndustryStatus;
        }

        if (JobWorkingStatus == "") {
            Toast.makeText(JobPostPage.this, "Job type require", Toast.LENGTH_SHORT).show();
        } else if (Currency == null) {
            Toast.makeText(JobPostPage.this, "Select your currency", Toast.LENGTH_SHORT).show();
        } else if (Start_salary.isEmpty()) {
            Toast.makeText(JobPostPage.this, "Start salary require", Toast.LENGTH_SHORT).show();
        } else if (End_salary.isEmpty()) {
            Toast.makeText(JobPostPage.this, "End salary require", Toast.LENGTH_SHORT).show();
        } else if (Integer.valueOf(Start_salary) > Integer.valueOf(End_salary)) {
            Toast.makeText(JobPostPage.this, "End salary must bigger then start salary", Toast.LENGTH_SHORT).show();
        } else if (Experience.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Experience request", Toast.LENGTH_SHORT).show();
        } else if (Reward.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Reward require", Toast.LENGTH_SHORT).show();
        } else if (Location.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Location require", Toast.LENGTH_SHORT).show();
        } else if (Contact.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Location require", Toast.LENGTH_SHORT).show();
        } else if (Industry == "") {
            Toast.makeText(getApplicationContext(), "Industry require", Toast.LENGTH_SHORT).show();
        } else {
            progressdialoag();
            jobPostViewModel.postjob(JobWorkingStatus, Start_salary, End_salary, Experience, Reward, Location, Contact, Industry, Currency)
                    .observe(this, new Observer<ResponseCode>() {
                        @Override
                        public void onChanged(ResponseCode responseCode) {
                            if(responseCode.getCode() == DataManager.SuccessCode){
                                responseCode.setCode(DataManager.DefaultCode);
                                iosDialog.dismiss();
                                successdialoag();
                            }
                            if(responseCode.getCode() == DataManager.ErrorCode){
                                iosDialog.dismiss();
                                responseCode.setCode(DataManager.DefaultCode);
                                errordialog();
                            }
                        }
                    });
        }


    }

    private void errordialog(){
        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(JobPostPage.this, R.raw.alert);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(JobPostPage.this, R.style.PauseDialog);
        ErrorDialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.error_dialog, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        binding.CloseButton.setOnClickListener(view -> {
            alertDialog.dismiss();
        });

        binding.FeedbackButton.setOnClickListener(view -> {
            alertDialog.dismiss();
            sendfeedback();
        });
    }

    private void sendfeedback(){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"+DataManager.FeedbackEmail));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Animatoo.animateSlideRight(JobPostPage.this);
    }

    private void successdialoag() {

        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(JobPostPage.this, R.raw.success);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }


        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(JobPostPage.this, R.style.PauseDialog);
        SuccessDialoagBinding binding = DataBindingUtil.inflate(LayoutInflater.from(JobPostPage.this), R.layout.success_dialoag, null, false);
        Mbuilder.setView(binding.getRoot());

        androidx.appcompat.app.AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        binding.OkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(JobPostPage.this);
            }
        });

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
                Animatoo.animateSlideRight(JobPostPage.this);
            }
        });
    }


    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(JobPostPage.this)
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Sending your request")
                .build();

        iosDialog.show();
    }
}