package com.group.parak.UI.TabFragement;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.group.parak.Adapter.HeirAdapter;
import com.group.parak.Model.HeirModel;
import com.group.parak.Model.Profile;
import com.group.parak.R;
import com.group.parak.Response.PostResponse;
import com.group.parak.UI.Activity.HeirPostPage;
import com.group.parak.UI.Activity.LoginRegPage;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.DownloaddialoagBinding;
import com.group.parak.databinding.HairBinding;
import com.group.parak.databinding.IdcardnotactiveDialogBinding;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class Hair extends Fragment {

    private ViewModel viewModel;
    private HeirAdapter adapter;
    private List<HeirModel> list = new ArrayList<>();
    private int page = 20;
    private HairBinding binding;
    private MediaPlayer mediaPlayer;

    public Hair() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.hair, container, false);

        init_view();

        return binding.getRoot();
    }


    private void init_view() {
        binding.ShimmerView.setVisibility(View.VISIBLE);
        binding.RecylerViewID.setHasFixedSize(true);
        adapter = new HeirAdapter();
        binding.RecylerViewID.setAdapter(adapter);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        binding.AddButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewModel.getUserExists().observe(getViewLifecycleOwner(), new Observer<PostResponse>() {
                    @Override
                    public void onChanged(PostResponse postResponse) {
                        if (postResponse.isJobpost()) {
                            viewModel.getprofile().observe(getViewLifecycleOwner(), new Observer<Profile>() {
                                @Override
                                public void onChanged(Profile profile) {
                                    if (profile != null) {
                                        if (profile.isCardActiveStatus()) {
                                            goto_heirpostpage();
                                        } else {
                                            openfeedbackdialog();
                                        }
                                    }
                                }
                            });

                        } else {
                            goto_loginpage();
                        }
                    }
                });

            }
        });

        binding.SwipRefreshLayoutID.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getdata_server(page);
            }
        });

        binding.RecylerViewID.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {

                if (!recyclerView.canScrollVertically(1)) {

                    page = page + 20;
                    getdata_server(page);

                }

                super.onScrolled(recyclerView, dx, dy);
            }
        });

        getdata_server(page);
    }

    private void getdata_server(int page) {


        if (20 < page) {
            binding.ProgressbarID.setVisibility(View.VISIBLE);
            binding.SwipRefreshLayoutID.setRefreshing(true);
            viewModel.getheirdata(page).observe(getViewLifecycleOwner(), new Observer<List<HeirModel>>() {
                @Override
                public void onChanged(List<HeirModel> heirModels) {

                    if (heirModels != null) {
                        adapter.setHeirModelList(heirModels);
                        adapter.notifyDataSetChanged();
                        binding.ShimmerView.setVisibility(View.GONE);
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.ProgressbarID.setVisibility(View.GONE);

                        binding.MessageIcon.setVisibility(View.GONE);
                        binding.MessageText.setVisibility(View.GONE);

                        adapter.SetOnclickLisiner(new HeirAdapter.OnClick() {
                            @Override
                            public void Click(String JobTitle, String Location, String FileUri, String CVDownloadURl, String UID) {

                                getuserinfo(JobTitle, Location, FileUri, CVDownloadURl);

                            }
                        });
                    } else {
                        adapter.setHeirModelList(heirModels);
                        adapter.notifyDataSetChanged();
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.ProgressbarID.setVisibility(View.GONE);
                        binding.ShimmerView.setVisibility(View.GONE);

                        binding.MessageText.setVisibility(View.VISIBLE);
                        binding.MessageIcon.setVisibility(View.VISIBLE);
                    }


                }
            });
        } else {
            viewModel.getheirdata(page).observe(getViewLifecycleOwner(), new Observer<List<HeirModel>>() {
                @Override
                public void onChanged(List<HeirModel> heirModels) {

                    if (heirModels != null) {
                        adapter.setHeirModelList(heirModels);
                        adapter.notifyDataSetChanged();
                        binding.ShimmerView.setVisibility(View.GONE);
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.ProgressbarID.setVisibility(View.GONE);

                        adapter.SetOnclickLisiner(new HeirAdapter.OnClick() {
                            @Override
                            public void Click(String JobTitle, String Location, String FileUri, String CVDownloadURL, String UID) {

                                getuserinfo(JobTitle, Location, FileUri, CVDownloadURL);


                            }
                        });
                        binding.MessageText.setVisibility(View.GONE);
                        binding.MessageIcon.setVisibility(View.GONE);
                    } else {
                        adapter.setHeirModelList(heirModels);
                        adapter.notifyDataSetChanged();
                        binding.SwipRefreshLayoutID.setRefreshing(false);
                        binding.ProgressbarID.setVisibility(View.GONE);
                        binding.ShimmerView.setVisibility(View.GONE);

                        binding.MessageText.setVisibility(View.VISIBLE);
                        binding.MessageIcon.setVisibility(View.VISIBLE);
                    }


                }
            });
        }

    }

    private void goto_heirpostpage() {
        Intent intent = new Intent(getActivity(), HeirPostPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void goto_loginpage() {
        Intent intent = new Intent(getActivity(), LoginRegPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void getuserinfo(String JobTitle, String Location, String FileUri, String CVDownloadURl) {

        BottomSheetDialog dialog = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
        DownloaddialoagBinding view = DownloaddialoagBinding.inflate(getLayoutInflater(), null, false);
        dialog.setContentView(view.getRoot());
        dialog.show();

        view.CloseButtonID.setOnClickListener(v -> {
            dialog.dismiss();
        });

        view.Location.setText(Location);
        view.Work.setText(JobTitle);

        if (FileUri != null) {
            view.CVLinkButtonID.setVisibility(View.VISIBLE);
            view.CVLinkButtonID.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(FileUri));
                startActivity(intent);
            });
        }

        if (CVDownloadURl != null) {
            view.DownloadCVButtonID.setVisibility(View.VISIBLE);
            view.DownloadCVButtonID.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(CVDownloadURl));
                startActivity(intent);
            });
        }


    }

    private void openfeedbackdialog() {
        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(getActivity(), R.raw.notification);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }

        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity(), R.style.PauseDialog);
        IdcardnotactiveDialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.idcardnotactive_dialog, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        binding.CloseButtonID.setOnClickListener(v -> {
            alertDialog.dismiss();
        });

        binding.FeedbackButton.setOnClickListener(v -> {
            emailfeedback();
            alertDialog.dismiss();
        });
    }

    private void emailfeedback() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:rakpak2020@gmail.com"));
        startActivity(Intent.createChooser(emailIntent, "Send feedback"));
    }
}