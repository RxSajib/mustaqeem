package com.group.parak.UI.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.group.parak.Data.DataManager;
import com.group.parak.Model.Data;
import com.group.parak.Model.ResponseCode;
import com.group.parak.Response.NotifactionResponse;
import com.group.parak.Response.UploadAssetsResponse;
import com.group.parak.R;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.ErrorDialogBinding;
import com.group.parak.databinding.HeirPostPageBinding;
import com.group.parak.databinding.SuccessDialoagBinding;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dmax.dialog.SpotsDialog;

public class HeirPostPage extends AppCompatActivity {

    private static final int PDFCODE = 1;
    private ViewModel viewModel;
    private String PdfDownloadUri;
    private String JobType = "";
    private HeirPostPageBinding binding;
    private boolean isjobvalid = false;
    private String ValidLink;
    private String CvLink;
    private IOSDialog iosDialog;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.heir_post_page);


        init_view();
    }

    private void init_view() {

        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        binding.SalesID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Sales;
                binding.SalesID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.BusinessDevelopmentID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Business_Development;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.AccountingID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Accounting;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.JobsWantedID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Jobs_Wanted;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.MediaID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Media;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.LegalServicesID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Legal_Services;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.InternetID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Internet;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.GraphicDesignID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.GraphicDesign;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.WriterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Waiters;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.WebDesignerID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.WebDesigner;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.WaiterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Writer;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.VetID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Vet;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.TvPresenterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.TvPresenter;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.TvCaeramanID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.TvCameraman;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.FoodBeveragesID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Food_Beverages;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.EngineeringID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Engineering;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_button_click_event);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);

                clearcheackbox();
            }
        });

        binding.TranslatorID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobType = DataManager.Translator;
                binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
                binding.TranslatorID.setBackgroundResource(R.drawable.job_button_click_event);

                clearcheackbox();
            }
        });

        binding.UploadCvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                startActivityForResult(intent, PDFCODE);
            }
        });

        binding.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Animatoo.animateSlideRight(HeirPostPage.this);
            }
        });

        binding.CheckBoxID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.CheckBoxID.isChecked()) {
                    binding.MyQulactionID.setVisibility(View.VISIBLE);
                    JobType = "";
                    clerwork();
                } else {
                    binding.MyQulactionID.setVisibility(View.GONE);
                    Log.d("status", "unchecked");
                    JobType = "";
                    binding.MyQulactionID.setText(null);
                    clerwork();
                }
            }
        });


        binding.JobPostButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload_data();

            }
        });


    }

    private void clearcheackbox(){
        binding.MyQulactionID.setText(null);
        binding.CheckBoxID.setChecked(false);
        binding.MyQulactionID.setVisibility(View.GONE);
    }

    private void upload_data() {

        String Name = binding.Name.getText().toString().trim();
        String Location = binding.Location.getText().toString().trim();
        String PhoneNumber = binding.Phone.getText().toString().trim();
        String Email = binding.Email.getText().toString().trim();
        CvLink = binding.CVLInk.getText().toString().trim();
        String InputJob = binding.MyQulactionID.getText().toString().trim();


        if (Name.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Name require", Toast.LENGTH_SHORT).show();
        }
        else if (Location.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Location require", Toast.LENGTH_SHORT).show();
        }
        else if (PhoneNumber.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Location require", Toast.LENGTH_SHORT).show();
        }
        else if (PhoneNumber.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Phone require", Toast.LENGTH_SHORT).show();
        }
        else if (Email.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Email require", Toast.LENGTH_SHORT).show();
        }

        else if (JobType == "" && InputJob.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Job type require", Toast.LENGTH_SHORT).show();
        }
        else {
            if (JobType != "") {
                uploadpost(PdfDownloadUri, Name, Location, PhoneNumber, Email, JobType);
            } else if (!InputJob.isEmpty()) {
                uploadpost(PdfDownloadUri, Name, Location, PhoneNumber, Email, InputJob);
            }
        }


    }

    private void uploadpost(String Pdfuri, String Name, String Location, String Phone, String Email, String Work){
        progressdialoag();
        if(!CvLink.isEmpty()){
            if(!isValidUrl(CvLink)){
                Toast.makeText(getApplicationContext(), "Cv link not valid", Toast.LENGTH_SHORT).show();
                iosDialog.dismiss();
            }else {
                viewModel.uploadhairdata(Name, Location, Phone, Email, Work, CvLink, Pdfuri).observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if(aBoolean){
                            iosDialog.dismiss();
                            send_notifaction();
                            successuploaddialoag();
                        }else {
                            iosDialog.dismiss();
                            errordialog();
                        }
                    }
                });

            }
        }else {
            viewModel.uploadhairdata(Name, Location, Phone, Email, Work, CvLink, Pdfuri).observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if(aBoolean){
                        send_notifaction();
                        iosDialog.dismiss();
                        successuploaddialoag();
                    }else {
                        iosDialog.dismiss();
                        errordialog();
                    }
                }
            });
        }
    }

    private void errordialog(){
        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(HeirPostPage.this, R.raw.alert);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(HeirPostPage.this, R.style.PauseDialog);
        ErrorDialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.error_dialog, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        binding.CloseButton.setOnClickListener(view -> {
            alertDialog.dismiss();
        });

        binding.FeedbackButton.setOnClickListener(view -> {
            alertDialog.dismiss();
            sendfeedback();
        });
    }
    private void sendfeedback(){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"+DataManager.FeedbackEmail));
        startActivity(intent);
    }

    private void send_notifaction(){

        Data data = new Data("send hair post", "hair ...", "https://www.learndash.com/wp-content/uploads/Notification-Add-on.png", "");
        NotifactionResponse response = new NotifactionResponse(data, "ccddIBD9T9efuU-GGNF5um:APA91bGwCheYFJafrIU8dTfkQAjxtTbufeLq6KP0pIQ1fxGM1qu8GPZl65fpT-oEjXXsArQzgNA98Jjo_jjyW_sBcySSCZEmSBolJprS5uOYyJmGcwM1p1MlqjZ-6V0prwTCKtE9sclL", DataManager.High);

        viewModel.send_notifaction(response).observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean responseCode) {
                if(responseCode){
                }else {
                }
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PDFCODE && resultCode == RESULT_OK) {
            progressdialoag();
            viewModel.upload_hairCV(data.getData()).observe(this, new Observer<UploadAssetsResponse>() {
                @Override
                public void onChanged(UploadAssetsResponse uploadAssetsResponse) {
                    if (uploadAssetsResponse.getCode() == DataManager.SuccessCode) {
                        uploadAssetsResponse.setCode(DataManager.DefaultCode);
                        PdfDownloadUri = uploadAssetsResponse.getPosterpath();
                        iosDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Upload success", Toast.LENGTH_SHORT).show();
                        binding.PdfUploadImage.setVisibility(View.VISIBLE);
                        binding.PdfUploadText.setText("upload success");
                        binding.PdfUploadText.setTextColor(getResources().getColor(R.color.carbon_black_54));

                    } else {
                        uploadAssetsResponse.setCode(DataManager.DefaultCode);
                        Toast.makeText(getApplicationContext(), "Error upload document", Toast.LENGTH_SHORT).show();
                        iosDialog.dismiss();
                        binding.PdfUploadImage.setVisibility(View.GONE);
                        binding.PdfUploadText.setText("Error upload");
                        binding.PdfUploadText.setTextColor(getResources().getColor(R.color.carbon_red_400));
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSlideRight(HeirPostPage.this);
    }

    private void clerwork() {
        binding.SalesID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.BusinessDevelopmentID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.AccountingID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.JobsWantedID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.MediaID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.LegalServicesID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.InternetID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.GraphicDesignID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WriterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WebDesignerID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.WaiterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.VetID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TvPresenterID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TvCaeramanID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.FoodBeveragesID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.EngineeringID.setBackgroundResource(R.drawable.job_type_unselected);
        binding.TranslatorID.setBackgroundResource(R.drawable.job_type_unselected);
    }

    private boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }

    private void send_notifactionhistory(){
        viewModel.send_notifactionhistory("Send a hair post request", "hair")
                .observe(this, new Observer<ResponseCode>() {
                    @Override
                    public void onChanged(ResponseCode responseCode) {
                        if(responseCode.getCode() == DataManager.SuccessCode){
                            responseCode.setCode(DataManager.DefaultCode);
                            successuploaddialoag();
                            iosDialog.dismiss();
                        }else if(responseCode.getCode() == DataManager.ErrorCode){
                            responseCode.setCode(DataManager.DefaultCode);
                            iosDialog.dismiss();
                        }
                    }
                });
    }

    private void successuploaddialoag(){
        if (mediaPlayer == null){
            mediaPlayer =  MediaPlayer.create(HeirPostPage.this, R.raw.success);
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
        }
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(HeirPostPage.this, R.style.PauseDialog);
        SuccessDialoagBinding binding = DataBindingUtil.inflate(LayoutInflater.from(HeirPostPage.this), R.layout.success_dialoag, null, false);
        Mbuilder.setView(binding.getRoot());

        androidx.appcompat.app.AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
                Animatoo.animateSlideRight(HeirPostPage.this);
            }
        });

        binding.OkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(HeirPostPage.this);
            }
        });
    }


    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(HeirPostPage.this)
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Uploading")
                .build();

        iosDialog.show();
    }
}