package com.group.parak.UI.JobDashboardFragement;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.group.parak.Adapter.PendingAdapter;
import com.group.parak.Data.DataManager;
import com.group.parak.Data.TimesSinceAgo;
import com.group.parak.Model.JobModel;
import com.group.parak.Model.ResponseCode;
import com.group.parak.R;
import com.group.parak.UI.Activity.UpdateJob;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.DashboardOptionLayoutBinding;
import com.group.parak.databinding.DashboardShowDialoagBinding;
import com.group.parak.databinding.DeleteDashboardLayoutBinding;
import com.group.parak.databinding.FragmentPendingJobBinding;

import java.util.List;

import de.mateware.snacky.Snacky;


public class PendingJob extends Fragment {

    private FragmentPendingJobBinding binding;
    private ViewModel viewModel;
    private PendingAdapter pendingAdapter;
    private IOSDialog iosDialog;

    public PendingJob() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pending_job, container, false);

        SuccessDeleteMessage();
        init_view();
        return binding.getRoot();
    }

    private void init_view() {
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        pendingAdapter = new PendingAdapter();
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        binding.RecyclerView.setAdapter(pendingAdapter);
        getdata_server();
    }


    private void getdata_server() {

        viewModel.myjobpending(10)
                .observe(getViewLifecycleOwner(), new Observer<List<JobModel>>() {
                    @Override
                    public void onChanged(List<JobModel> jobModels) {
                        if (jobModels != null) {
                            binding.ShimmerView.setVisibility(View.GONE);
                            pendingAdapter.setJobModelList(jobModels);
                            pendingAdapter.notifyDataSetChanged();

                            binding.MessageIcon.setVisibility(View.GONE);
                            binding.MessageText.setVisibility(View.GONE);
                            pendingAdapter.SetOnclickLisiner(new PendingAdapter.SetOnclick() {
                                @Override
                                public void Click(long Key, int adapterposition) {
                                    open_dialoag(Key, adapterposition, jobModels);
                                }
                            });

                        } else {
                            binding.ShimmerView.setVisibility(View.GONE);
                            pendingAdapter.setJobModelList(jobModels);
                            pendingAdapter.notifyDataSetChanged();
                            binding.MessageIcon.setVisibility(View.VISIBLE);
                            binding.MessageText.setVisibility(View.VISIBLE);
                            binding.ShimmerView.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void open_dialoag(long Key, int adapterposition, List<JobModel> jobModelList) {
        BottomSheetDialog Mdialoag = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
        DashboardOptionLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.dashboard_option_layout, null, false);
        Mdialoag.setContentView(binding.getRoot());

        Mdialoag.show();
        binding.ViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mdialoag.dismiss();
                open_show_dialoag(Key);
            }
        });

        binding.RemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mdialoag.dismiss();
                remove_post(Key, adapterposition, jobModelList);
            }
        });

        binding.UpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mdialoag.dismiss();
                Intent intent = new Intent(getActivity(), UpdateJob.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(DataManager.UID, Key);
                startActivity(intent);
                Animatoo.animateSlideLeft(getActivity());
            }
        });
    }



    private void open_show_dialoag(long Key) {


        BottomSheetDialog Mdialoag = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
        DashboardShowDialoagBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.dashboard_show_dialoag, null, false);
        Mdialoag.setContentView(binding.getRoot());
        Mdialoag.show();


        binding.CloseButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Mdialoag.dismiss();
            }
        });


        viewModel.getPendingJobdetails(Key).observe(this, new Observer<JobModel>() {
            @Override
            public void onChanged(JobModel jobModel) {
                if (jobModel != null) {
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    binding.ShimmerView.setVisibility(View.GONE);
                    binding.MainviewID.setVisibility(View.VISIBLE);
                    binding.Experiences.setText(jobModel.getExperience());
                    binding.Reward.setText(jobModel.getRewards());
                    binding.Industry.setText(jobModel.getIndustry());
                    binding.Price.setText(jobModel.getCurrency() + " " + jobModel.getStartSalary() + " - " + jobModel.getEndSalary());
                    binding.Location.setText(jobModel.getLocation());
                    binding.JobType.setText(jobModel.getJobType());

                    TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
                    String Time = timesSinceAgo.getTimeAgo(jobModel.getTimestamp(), getActivity());
                    binding.Times.setText(Time);
                } else {

                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);

                    binding.ShimmerView.setVisibility(View.GONE);
                    binding.MainviewID.setVisibility(View.GONE);
                }
            }
        });


    }

    private void remove_post(long Key, int adapterposition, List<JobModel> jobModelList) {
        BottomSheetDialog Mdialoag = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
        DeleteDashboardLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.delete_dashboard_layout, null, false);

        Mdialoag.setContentView(binding.getRoot());
        Mdialoag.show();

        binding.CancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mdialoag.dismiss();
            }
        });

        binding.DeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mdialoag.dismiss();
                progressdialoag();
                viewModel.delete_requestjob(Key).observe(getViewLifecycleOwner(), new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if(aBoolean){
                            iosDialog.dismiss();
                        }else {
                            iosDialog.dismiss();
                        }
                    }
                });
            }
        });
    }

    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(getActivity())
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Removing")
                .build();

        iosDialog.show();
    }

    private void SuccessDeleteMessage(){
       /* Snacky.builder()
                .setView(binding.View)
                .setText("Your item delete success")

                .setDuration(Snacky.LENGTH_SHORT)
                .setTextColor(getResources().getColor(R.color.white))
                .build()
                .show();*/


    }
}