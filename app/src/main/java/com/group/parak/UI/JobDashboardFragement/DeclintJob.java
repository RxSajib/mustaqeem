package com.group.parak.UI.JobDashboardFragement;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.group.parak.Adapter.PendingAdapter;
import com.group.parak.Data.DataManager;
import com.group.parak.Data.TimesSinceAgo;
import com.group.parak.Model.JobModel;
import com.group.parak.R;
import com.group.parak.ViewModel.ViewModel;
import com.group.parak.databinding.DashboardShowDialoagBinding;
import com.group.parak.databinding.DeclineDashboardLayoutBinding;
import com.group.parak.databinding.DeleteDashboardLayoutBinding;
import com.group.parak.databinding.FragmentDeclintJobBinding;

import java.util.List;

public class DeclintJob extends Fragment {

    private FragmentDeclintJobBinding binding;
    private ViewModel viewModel;
    private PendingAdapter pendingAdapter;
    private IOSDialog iosDialog;

    public DeclintJob() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_declint_job, container, false);

        init_view();
        return binding.getRoot();
    }

    private void init_view() {
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        pendingAdapter = new PendingAdapter();
        binding.RecyclerView.setAdapter(pendingAdapter);

        getdata_from_server();
    }

    private void getdata_from_server() {
        viewModel.getRejectedJob(10).observe(getViewLifecycleOwner(), new Observer<List<JobModel>>() {
            @Override
            public void onChanged(List<JobModel> jobModels) {
                if (jobModels != null) {
                    binding.ShimmerView.setVisibility(View.GONE);
                    pendingAdapter.setJobModelList(jobModels);
                    pendingAdapter.notifyDataSetChanged();
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    pendingAdapter.SetOnclickLisiner(new PendingAdapter.SetOnclick() {
                        @Override
                        public void Click(long Key, int adapterposition) {
                            open_dialoag(Key);
                        }
                    });

                } else {
                    pendingAdapter.setJobModelList(jobModels);
                    pendingAdapter.notifyDataSetChanged();
                    binding.ShimmerView.setVisibility(View.GONE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void open_dialoag(long Key) {
        BottomSheetDialog Mdialoag = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
        DeclineDashboardLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.decline_dashboard_layout, null, false);
        Mdialoag.setContentView(binding.getRoot());

        Mdialoag.show();
        binding.ViewPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mdialoag.dismiss();
                open_show_dialoag(Key);
            }
        });

        binding.DeleteButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mdialoag.dismiss();
                remove_post(Key);
            }
        });
    }

    private void open_show_dialoag(long Key) {
        BottomSheetDialog Mdialoag = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
        DashboardShowDialoagBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.dashboard_show_dialoag, null, false);
        Mdialoag.setContentView(binding.getRoot());
        Mdialoag.show();


        binding.CloseButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Mdialoag.dismiss();
            }
        });



        viewModel.getjobdashboard_rejected_iteamdetails(Key).observe(this, new Observer<JobModel>() {
            @Override
            public void onChanged(JobModel jobModel) {
                if (jobModel != null) {
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    binding.ShimmerView.setVisibility(View.GONE);
                    binding.MainviewID.setVisibility(View.VISIBLE);
                    binding.Experiences.setText(jobModel.getExperience());
                    binding.Reward.setText(jobModel.getRewards());
                    binding.Industry.setText(jobModel.getIndustry());
                    binding.Price.setText(jobModel.getCurrency() + " " + jobModel.getStartSalary() + " - " + jobModel.getEndSalary());
                    binding.Location.setText(jobModel.getLocation());
                    binding.JobType.setText(jobModel.getJobType());

                    TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
                    String Time = timesSinceAgo.getTimeAgo(jobModel.getTimestamp(), getActivity());
                    binding.Times.setText(Time);
                } else {
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);

                    binding.ShimmerView.setVisibility(View.GONE);
                    binding.MainviewID.setVisibility(View.GONE);
                }
            }
        });


    }

    private void remove_post(long Key){
        BottomSheetDialog Mdialoag = new BottomSheetDialog(getActivity(), R.style.CustomBottomSheetDialogTheme);
        DeleteDashboardLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.delete_dashboard_layout, null, false);

        Mdialoag.setContentView(binding.getRoot());
        Mdialoag.show();

        binding.DeleteButton.setOnClickListener(view -> {
            Mdialoag.dismiss();
            progressdialoag();
            viewModel.JobDeclineRemove(Key).observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if(aBoolean){
                        iosDialog.dismiss();
                    }else {
                        iosDialog.dismiss();
                    }
                }
            });

        });

        binding.CancelButton.setOnClickListener(view -> Mdialoag.dismiss());
    }

    private void progressdialoag() {
        iosDialog = new IOSDialog.Builder(getActivity())
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent("Removing")
                .build();

        iosDialog.show();
    }
}